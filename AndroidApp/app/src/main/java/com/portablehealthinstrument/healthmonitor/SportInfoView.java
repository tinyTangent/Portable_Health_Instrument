package com.portablehealthinstrument.healthmonitor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by tansinan on 2016/2/28.
 */
public class SportInfoView extends RelativeLayout {
    protected TimeBarGraph diagram;
    protected ArcNumberView arcNumberView;
    protected AlertDialog datePickerDialog;
    protected Button datePickerButton;
    protected Button buttonShare;

    public void updateData(){
        GregorianCalendar today = new GregorianCalendar();
        int total = 0;
        ArrayList<Integer> stat = new ArrayList<>();
        int timeSlots = 24 * 3;
        for(int i = 0; i < timeSlots; i++)
        {
            stat.add(0);
        }
        for(HealthDataManager.SportData i: HealthDataManager.getInstance().getSportsData()){
            if (i.dateTime.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
            i.dateTime.get(Calendar.MONTH) == today.get(Calendar.MONTH) &&
            i.dateTime.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
                total++;
                int secondOfData = i.dateTime.get(Calendar.HOUR_OF_DAY) * 60 +
                        i.dateTime.get(Calendar.MINUTE);
                int slotAt = secondOfData / ((24 * 60) / timeSlots);
                stat.set(slotAt, stat.get(slotAt) + 1);
            }
        }
        setWillNotDraw(false);
        arcNumberView.setValue(total);
        diagram.setBarGraphData(stat);
    }
    public SportInfoView(Context context) {
        super(context);
    }

    public SportInfoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void onFinishInflate() {
        setupColorPickerDialog();
        super.onFinishInflate();
        Controller.getInstance().viewSportInfo = this;
        diagram = (TimeBarGraph)this.findViewById(R.id.diagram);
        arcNumberView = (ArcNumberView)this.findViewById(R.id.view2);
        datePickerButton = (Button)findViewById(R.id.button_set_date);
        datePickerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GregorianCalendar c1 = new GregorianCalendar();
                GregorianCalendar c2 = new GregorianCalendar();
                c1.set(2015, 1, 1, 1, 1, 1);
                c1.set(Calendar.MILLISECOND, 15);
                c2.set(2015, 1, 1, 1, 1, 1);
                c2.set(Calendar.MILLISECOND, 15);
                Toast.makeText(getContext(), c1.hashCode() + " " + c2.hashCode() +
                        c1.equals(c2), Toast.LENGTH_SHORT).show();
                DatePickerDialog dialog;
                Calendar newCalendar = new GregorianCalendar();
                dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });
        buttonShare = (Button)findViewById(R.id.button_share);
        buttonShare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonShare();
            }
        });
        updateData();
    }
    protected void setupColorPickerDialog() {
        if(isInEditMode()) return;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

        // set title
        alertDialogBuilder.setTitle("Your Title");
        // set dialog message
        alertDialogBuilder
                //.setView(new DatePicker())
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        //dialog.cancel();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        datePickerDialog = alertDialogBuilder.create();
    }

    protected void onButtonShare() {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "__temp_image.png");
        try {
            Bitmap bitmap = Bitmap.createBitmap(600, 300, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.WHITE);
            Canvas c = new Canvas(bitmap);
            Paint p = new Paint();
            p.setColor(Color.RED);
            HealthDataManager h = HealthDataManager.getInstance();
            c.drawText("今天运动了" + h.getSportsData().size(), 100, 100, p);
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
        } catch (Exception e) {}
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            ((Activity)getContext()).startActivityForResult(
                    takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
        /*Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        getContext().startActivity(
                Intent.createChooser(sharingIntent, "Share image using"));*/
    }
    static final int REQUEST_IMAGE_CAPTURE = 101;
}
