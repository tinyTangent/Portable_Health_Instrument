package com.portablehealthinstrument.healthmonitor;

import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;

/**
 * HomeColumnar类实现了一个柱状图的显示数据。
 * TODO : 该类具有和HomeDiagram类完全相似的问题……
 * TODO : 该类尽管提供了图表显示的功能，但是问题在于该对象的长度会无限延伸
 * TODO : 因此需要在外面包装一个ScrollView才能使其真正正常工作。
 * TODO : 需要提供能够Set/Get Data的函数
 * TODO : 还有个问题是这个View会试图假定自己的Parent为RelativeLayout然后为自己设置LayoutParam，
 * TODO : 因此不合适的Parent View类型将导致崩溃。
 */
public class HomeColumnar extends View {

	private List<Score> score;
	private float tb;
	private float interval_left_right;
	private Paint paint_date, paint_rectf_gray, paint_rectf_blue;

	private int fineLineColor = 0x5faaaaaa; // 灰色
	private int blueLineColor = 0xffff3f00; // 蓝色

	public HomeColumnar(Context context) {
		super(context);
		initPaint();
	}

	public HomeColumnar(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		initPaint();
	}

	public boolean setColumnarData(List<Score> score) {
		if (null == score || score.size() == 0)
			return false;
		this.score = score;
		setLayoutParams(new RelativeLayout.LayoutParams(
				(int) (this.score.size() * interval_left_right),
				RelativeLayout.LayoutParams.MATCH_PARENT));
		invalidate();
		return true;
	}

	protected void initPaint()
	{
		Resources res = getResources();
		tb = res.getDimension(R.dimen.historyscore_tb);
		interval_left_right = tb * 5.0f;

		paint_date = new Paint();
		paint_date.setStrokeWidth(tb * 0.1f);
		paint_date.setTextSize(tb * 1.2f);
		paint_date.setColor(fineLineColor);
		paint_date.setTextAlign(Align.CENTER);

		paint_rectf_gray = new Paint();
		paint_rectf_gray.setStrokeWidth(tb * 0.1f);
		paint_rectf_gray.setColor(fineLineColor);
		paint_rectf_gray.setStyle(Style.FILL);
		paint_rectf_gray.setAntiAlias(true);

		paint_rectf_blue = new Paint();
		paint_rectf_blue.setStrokeWidth(tb * 0.1f);
		paint_rectf_blue.setColor(blueLineColor);
		paint_rectf_blue.setStyle(Style.FILL);
		paint_rectf_blue.setAntiAlias(true);
	}

	protected void onDraw(Canvas c) {
		if (null == score || score.size() == 0)
			return;
		drawDate(c);
		drawRectf(c);
	}

	/**
	 * 绘制矩形
	 * 
	 * @param c
	 */
	public void drawRectf(Canvas c) {
		for (int i = 0; i < score.size(); i++) {

			RectF f = new RectF();
			f.set(tb * 0.2f + interval_left_right * i,
					getHeight() - tb * 11.0f, tb * 3.2f + interval_left_right
							* i, getHeight() - tb * 2.0f);
			c.drawRoundRect(f, tb * 0.3f, tb * 0.3f, paint_rectf_gray);

			float base = score.get(i).score * (tb * 10.0f / 100);
			RectF f1 = new RectF();
			f1.set(tb * 0.2f + interval_left_right * i, getHeight()
					- (base + tb * 1.5f), tb * 3.2f + interval_left_right * i,
					getHeight() - tb * 1.5f);
			c.drawRoundRect(f1, tb * 0.3f, tb * 0.3f, paint_rectf_blue);
		}
	}

	/**
	 * 绘制日期
	 * 
	 * @param c
	 */
	public void drawDate(Canvas c) {
		for (int i = 0; i < score.size(); i++) {
			String date = score.get(i).date;
			String date_1 = date
					.substring(date.indexOf("-") + 1, date.length());
			c.drawText(date_1, tb * 1.7f + interval_left_right * i,
					getHeight(), paint_date);

		}
	}
}
