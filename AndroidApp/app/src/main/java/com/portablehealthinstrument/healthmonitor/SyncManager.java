package com.portablehealthinstrument.healthmonitor;

import android.annotation.TargetApi;
import android.bluetooth.*;
import android.content.Context;
import android.os.*;
import java.io.*;
import java.util.UUID;

/**
 * Created by tansinan on 2014/11/4.
 */
public class SyncManager
{
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    public static final int MESSAGE_CONNECTION_STATE_CHANGED = 100;
    public static final int MESSAGE_DATA_RECEIVED = 101;

    //input cache
    public byte[] tempBuffer = new byte[1024];
    public int tempBufferBytes = 0;
    public byte[] buffer = new byte[102400];
    int bufferSize = 102400;
    public int bufferBytes = 0;

    private static final UUID bluetoothSerialPortServiceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //Memebers for bluetooth communication
    protected BluetoothAdapter bluetoothAdapter;
    protected BluetoothDevice bluetoothDevice;
    protected BluetoothSocket bluetoothSocket;

    //Members for communicating with other class
    protected Context context;
    //protected DataCollectorView dataCollectorView;
    protected Handler handler;

    protected  ConnectThread connectThread;
    protected DataTransmissionThread dataTransmissionThread;
    protected int state;

    SyncManager(BluetoothAdapter adapter, Context _context, Handler _handler/*DataCollectorView view*/)
    {
        bluetoothAdapter = adapter;
        context = _context;
        handler = _handler;
        //dataCollectorView = view;
        dataTransmissionThread = new DataTransmissionThread();
    }

    public void setState(int newState)
    {
        state = newState;
        Message msg = new Message();
        msg.what = MESSAGE_CONNECTION_STATE_CHANGED;
        handler.sendMessage(msg);
    }

    public int getState()
    {
        return state;
    }

    public synchronized void connect(BluetoothDevice device) {
        bluetoothDevice = device;
        if (state == STATE_CONNECTING) {
            if (connectThread != null) {
                connectThread.cancel(); connectThread = null;}
        }

        // Cancel any thread currently running a connection
        //TODO: recover this code later
        //if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}

        // Start the thread to connect with the given device
        connectThread = new ConnectThread(device);
        connectThread.start();
        setState(STATE_CONNECTING);
    }

    public synchronized void disconnect() {
        if (connectThread != null) {
            connectThread.cancel();
            connectThread = null;
        }

        if (dataTransmissionThread != null) {
            dataTransmissionThread.cancel();
            dataTransmissionThread = null;
        }

        setState(STATE_NONE);
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket bluetoothSocket;
        private final BluetoothDevice bluetoothDevice;

        @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
        public ConnectThread(BluetoothDevice device)
        {
            bluetoothDevice = device;
            BluetoothSocket temp = null;
            try
            {
                temp = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(bluetoothSerialPortServiceUUID);
            }
            catch (Exception e)
            {
            }
            bluetoothSocket = temp;
        }

        public void run()
        {
            setName("ConnectThread");
            bluetoothAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                bluetoothSocket.connect();
            } catch (IOException e) {
                onConnectFailed();
                // Close the socket
                try
                {
                    bluetoothSocket.close();
                }
                catch (IOException e2) {}
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (SyncManager.this) {
                connectThread = null;
            }

            // Start the onConnectSuccess thread
            onConnectSuccess(bluetoothSocket);
        }

        public void cancel()
        {
            try
            {
                bluetoothSocket.close();
            }
            catch (IOException e) {}
        }
    }

    private class DataTransmissionThread extends Thread
    {
        protected InputStream inputStream;
        protected OutputStream outputStream;
        DataTransmissionThread()
        {
            inputStream = null;
            outputStream = null;
            if(bluetoothSocket==null)
            {
                return;
            }
            try
            {
                inputStream = bluetoothSocket.getInputStream();
                outputStream = bluetoothSocket.getOutputStream();
            }
            catch (IOException e)
            {
                onConnectFailed();
                return;
            }
        }
        public void run()
        {
            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    // Read from the InputStream
                    tempBufferBytes = inputStream.read(tempBuffer);
                    onDataReceived();
                    //mEmulatorView.write(buffer, bytes);
                    // Send the obtained bytes to the UI Activity
                    //mHandler.obtainMessage(BlueTerm.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    onConnectFailed();
                    break;
                }
            }
        }

        public void cancel()
        {
            try {
                inputStream.close();
                outputStream.close();
            }
            catch(IOException e){};
        }
    }

    protected void onConnectFailed() {
        setState(STATE_NONE);
    }

    protected void onConnectSuccess(BluetoothSocket socket)
    {
        setState(STATE_CONNECTED);
        bluetoothSocket = socket;
    }

    protected void onDataReceived()
    {
        for(int i=0;i<tempBufferBytes;i++)
        {
            buffer[bufferBytes+i] = tempBuffer[i];
        }
        bufferBytes += tempBufferBytes;
        Message msg = new Message();
        msg.what = MESSAGE_DATA_RECEIVED;
        handler.sendMessage(msg);
    }

    public void clearBuffer()
    {
        bufferBytes = 0;
    }

    public void beginDataCollection()
    {
        dataTransmissionThread = new DataTransmissionThread();
        dataTransmissionThread.start();
    }
}
