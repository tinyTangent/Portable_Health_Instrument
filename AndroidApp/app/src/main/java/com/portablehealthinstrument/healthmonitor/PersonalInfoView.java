package com.portablehealthinstrument.healthmonitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

/**
 * Created by tansinan on 16-4-6.
 */
public class PersonalInfoView extends LinearLayout {

    protected String userName;
    protected boolean userIsFemale;
    protected String userBirthday;
    protected int userSportTarget;

    Button buttonSave;
    Button buttonCancel;

    EditText editUserName;
    EditText editSportTarget;
    EditText editBirthday;
    RadioButton radioButtonFemale;
    RadioButton radioButtonMale;

    protected SharedPreferences userPreferences;

    public PersonalInfoView(Context context) {
        super(context);
        initUserInfo();
    }

    public PersonalInfoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initUserInfo();
    }

    protected void initUserInfo() {
        userPreferences = getContext().getSharedPreferences("user_info", 0);
        userBirthday = userPreferences.getString("birthday", "1990-01-01");
        userSportTarget = userPreferences.getInt("sport_target", 5000);
        userIsFemale = userPreferences.getBoolean("is_female", true);
        userName = userPreferences.getString("name", "测试用户");
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        editUserName = (EditText)findViewById(R.id.edit_user_name);
        editUserName.setText(userName);

        editSportTarget = (EditText)findViewById(R.id.edit_sport_target);
        editSportTarget.setText("" + userSportTarget);

        editBirthday = (EditText)findViewById(R.id.edit_birthday);
        editBirthday.setText(userBirthday);

        radioButtonFemale = (RadioButton)findViewById(R.id.radio_button_female);
        radioButtonFemale.setChecked(userIsFemale);

        radioButtonMale = (RadioButton)findViewById(R.id.radio_button_male);
        radioButtonMale.setChecked(!userIsFemale);

        buttonSave = (Button)findViewById(R.id.button_save);
        buttonSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonSave();
            }
        });

        buttonCancel = (Button) findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonCancel();
            }
        });
    }

    protected void onButtonSave() {
        SharedPreferences.Editor editor = userPreferences.edit();
        userName = editUserName.getText().toString();
        editor.putString("name", userName);

        userBirthday = editBirthday.getText().toString();
        editor.putString("birthday", userBirthday);

        userSportTarget = Integer.parseInt(editSportTarget.getText().toString());
        editor.putInt("sport_target", userSportTarget);

        userIsFemale = radioButtonFemale.isChecked();
        editor.putBoolean("is_female", userIsFemale);

        editor.apply();
    }

    protected void onButtonCancel() {
        editUserName.setText(userName);
        editSportTarget.setText("" + userSportTarget);
        editBirthday.setText(userBirthday);
        radioButtonFemale.setChecked(userIsFemale);
        radioButtonMale.setChecked(!userIsFemale);
    }
}
