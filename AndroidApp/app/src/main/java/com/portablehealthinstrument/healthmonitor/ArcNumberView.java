package com.portablehealthinstrument.healthmonitor;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;

/**
 * HomeArc类实现一个环形的分数显示器，该数值被一个圆弧包围，这个控件具有动画效果
 * TODO: 需要一个score的Get/Set函数！！
 */
public class ArcNumberView extends View {

    private Paint paint_black, paint_white;
    private RectF rectf;
    private float tb;
    private int blackColor = 0x70000000; // 底黑色
    private int whiteColor = 0xddffffff; // 白色
    protected int score;
    private float arc_y = 0f;
    private int score_text;

    public ArcNumberView(Context context)
    {
        super(context);
        init(90);
    }
    public ArcNumberView(Context context, int score) {
        super(context);
        init(score);
    }

    public ArcNumberView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(90);
    }

    public void setValue(int data)
    {
        score = data;
        score_text = data;
        invalidate();
    }

    public void init(int score) {
        //TODO: I don't know why init is called many many times.
        //this.score = score;
        //this.score_text = score;
        Resources res = getResources();
        tb = res.getDimension(R.dimen.historyscore_tb);

        paint_black = new Paint();
        paint_black.setAntiAlias(true);
        paint_black.setColor(whiteColor);
        paint_black.setStrokeWidth(tb * 0.4f);
        paint_black.setStyle(Style.STROKE);

        paint_white = new Paint();
        paint_white.setAntiAlias(true);
        paint_white.setColor(whiteColor);
        paint_white.setTextSize(tb*6.0f);
        paint_white.setStrokeWidth(tb * 0.2f);
        paint_white.setTextAlign(Align.CENTER);
        paint_white.setStyle(Style.STROKE);

        rectf = new RectF();
        rectf.set(tb * 0.5f, tb * 0.5f, tb * 17.5f, tb * 17.5f);

        setLayoutParams(new LayoutParams((int) (tb * 18f), (int) (tb * 18f)));

        /*this.getViewTreeObserver().addOnPreDrawListener(
                new OnPreDrawListener() {
                    public boolean onPreDraw() {
                        new thread();
                        getViewTreeObserver().removeOnPreDrawListener(this);
                        return false;
                    }
                });*/
    }

    protected void onDraw(Canvas c) {
        super.onDraw(c);
        c.drawArc(rectf, -90, 360, false, paint_black);
        c.drawArc(rectf, -90, arc_y, false, paint_white);
        c.drawText("" + score_text, tb * 9.7f, tb * 11.0f, paint_white);
    }

    /*class thread implements Runnable {
        private Thread thread;
        private int statek;
        int count;

        public thread() {
            thread = new Thread(this);
            thread.start();
        }

        public void run() {
            while (true) {
                switch (statek) {
                    case 0:
                        try {
                            Thread.sleep(200);
                            statek = 1;
                        } catch (InterruptedException e) {
                        }
                        break;
                    case 1:
                        try {
                            Thread.sleep(15);
                            arc_y += 3.6f;
                            score_text++;
                            count++;
                            postInvalidate();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                }
                if (count >= score)
                    break;
            }
        }
    }*/

}
