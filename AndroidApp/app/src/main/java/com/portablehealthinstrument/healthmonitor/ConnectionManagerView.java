package com.portablehealthinstrument.healthmonitor;

import android.bluetooth.*;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.*;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.portablehealthinstrument.healthmonitor.Model.DevicePair;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

import io.palaima.smoothbluetooth.Device;
import io.palaima.smoothbluetooth.SmoothBluetooth;

/**
 * Created by tansinan on 2015/5/30.
 */
public class ConnectionManagerView extends LinearLayout implements View.OnClickListener, Handler.Callback{

    private SmoothBluetooth.Listener mListener = new SmoothBluetooth.Listener() {
        @Override
        public void onBluetoothNotSupported() {
            //device does not support bluetooth
        }

        @Override
        public void onBluetoothNotEnabled() {}

        @Override
        public void onConnecting(Device device) {}

        @Override
        public void onConnected(Device device) {
            Calendar calendar = new GregorianCalendar();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);
            bluetoothManager.send("set_time ");
            bluetoothManager.send(year + " " + month +
                    " " + day + " " + hour + " " + minute + " " + second + "\r");
        }

        @Override public void onDisconnected() {}

        @Override
        public void onConnectionFailed(Device device) {
            Toast.makeText(getContext(),
                    "Connection Failed!!!",Toast.LENGTH_SHORT).show();
        }

        @Override public void onDiscoveryStarted() {}

        @Override public void onDiscoveryFinished() {}

        @Override public void onNoDevicesFound() {}

        @Override public void onDevicesFound(List<Device> deviceList, SmoothBluetooth.ConnectionCallback connectionCallback) {}

        @Override
        public void onDataReceived(int data) {
            char ch = (char)data;
            dataBuffer.append(ch);
            if(ch == '\n')
            {
                String dataString = dataBuffer.toString();
                InputStream stream = new ByteArrayInputStream(dataString.getBytes());
                Scanner scanner = new Scanner(new InputStreamReader(stream));
                if(scanner.hasNext()) {
                    String command = scanner.next();
                    List<Integer> arrSport = new ArrayList<>();
                    if(command.equals("ans_set_time")) {
                        bluetoothManager.send("dump\r");
                    }
                    else if(command.equals("ans_dump")) {
                        List<Integer> arrPollution = new ArrayList<>();
                        Integer time = scanner.nextInt();
                        int dataCount = scanner.nextInt();
                        arrSport.add(time);
                        arrSport.clear();
                        while (scanner.hasNext()) {
                            Integer value = scanner.nextInt();
                            arrSport.add(value);
                            value = scanner.nextInt();
                            arrPollution.add(value);
                        }
                        bluetoothManager.send("get_pollution\r");
                    }
                    else if(command.equals("ans_pollution")) {
                        int pm25 = scanner.nextInt();
                        int pm10 = scanner.nextInt();
                        Toast.makeText(getContext(), pm25 + " " + pm10, Toast.LENGTH_SHORT).show();
                        HealthDataManager healthDataManager = HealthDataManager.getInstance();
                        healthDataManager.addPollutionData(pm25, pm10);
                        if(bluetoothManager.isConnected()) {
                            bluetoothManager.disconnect();
                        }
                    }
                }
                dataBuffer.setLength(0);
            }
            //We need to debug
            //bluetoothManager.
        }
    };

    StringBuffer dataBuffer = new StringBuffer();

    DevicePair devicePair = new DevicePair();
    Controller controller;
    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice healthDevice;
    TextView textViewBluetoothStatus;

    Button buttonEnableBluetooth;
    Button buttonSearchDevices;
    Button buttonConnect;
    Button buttonSyncData;
    Button buttonTestBluetoothSync;
    Button buttonTestNetworkSync;

    ListView listBluetoothDevice;
    Handler handler;

    SmoothBluetooth bluetoothManager = new SmoothBluetooth(getContext(), SmoothBluetooth.ConnectionTo.OTHER_DEVICE,
            SmoothBluetooth.Connection.INSECURE, mListener);

    public ConnectionManagerView(Context context) {
        super(context);
    }

    public ConnectionManagerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        controller = Controller.getInstance();
        controller.viewConnectionManager = this;
        if(this.isInEditMode()) return;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        textViewBluetoothStatus = (TextView)findViewById(R.id.text_view_connection_status);

        buttonEnableBluetooth = (Button)findViewById(R.id.button_enable_bluetooth);
        buttonEnableBluetooth.setOnClickListener(new View.OnClickListener(){
            @Override public void onClick(View v) { ConnectionManagerView.this.onButtonEnableBluetooth(); }
        });

        buttonSearchDevices = (Button)findViewById(R.id.button_search_devices);
        buttonSearchDevices.setOnClickListener(new View.OnClickListener(){
            @Override public void onClick(View v) { ConnectionManagerView.this.onButtonSearchDevices(); }
        });

        buttonConnect = (Button)findViewById(R.id.button_connect);
        buttonConnect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManagerView.this.onButtonConnect();
            }
        });

        buttonSyncData = (Button)findViewById(R.id.button_sync_data);
        buttonSyncData.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionManagerView.this.onButtonSyncData();
            }
        });

        buttonTestBluetoothSync = (Button)findViewById(R.id.button_test_bluetooth_sync);
        buttonTestBluetoothSync.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onDebugButtonTestBluetoothSync();
            }
        });

        buttonTestNetworkSync = (Button)findViewById(R.id.button_test_network_sync);
        buttonTestNetworkSync.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onDebugButtonTestNetworkSync();
            }
        });

        listBluetoothDevice = (ListView)findViewById(R.id.list_bluetooth_devices);
        listBluetoothDevice.setAdapter(new ArrayAdapter<BluetoothDevice>(getContext(), -1) {
            public View getView(int position, View convertView, ViewGroup parent) {
                final BluetoothDevice device = getItem(position);
                View view;
                if(convertView != null) {
                    view = convertView;
                }
                else {
                    LayoutInflater inflater = LayoutInflater.from(ConnectionManagerView.this.getContext());
                    view = inflater.inflate(R.layout.item_bluetooth_device, parent, false);
                }
                ((TextView)view.findViewById(R.id.text_device_name)).setText(device.getName());
                ((TextView)view.findViewById(R.id.text_device_address)).setText(device.getAddress());
                view.findViewById(R.id.button_pair).setOnClickListener(new OnClickListener() {
                    @Override public void onClick(View v) {
                        ConnectionManagerView.this.onButtonPairDevice(device);
                        ConnectionManagerView.this.updateLayout();
                    }
                });
                return view;
            }
        });

        handler = new Handler(this);
        updateLayout();
    }

    protected void onButtonSyncData() {
        if(bluetoothManager.isConnected()) return;
        bluetoothManager.tryConnectionToDevice("HC-06", devicePair.getPairedDeivceUUID());
    }

    protected void onButtonConnect() {
        /*Toast.makeText(getContext(), "connecting", Toast.LENGTH_SHORT).show();
        bluetoothManager.tryConnectionToDevice("HC-06", devicePair.getPairedDeivceUUID());*/
    }

    @Override
    protected void onVisibilityChanged (@NonNull View view, int visibility) {
        if(visibility == VISIBLE) {
            updateLayout();
        }
    }

    /**
     * Update the layout according to Bluetooth connection status.
     */
    void updateLayout()
    {
        if(bluetoothAdapter == null) return;
        buttonEnableBluetooth.setVisibility(GONE);
        buttonSearchDevices.setVisibility(GONE);
        if(!bluetoothAdapter.isEnabled())
        {
            textViewBluetoothStatus.setText("Bluetooth not enabled.");
            buttonEnableBluetooth.setVisibility(VISIBLE);
        }
        else if(devicePair.getPairedDeivceUUID().isEmpty())
        {
            buttonSearchDevices.setVisibility(VISIBLE);
        }
        else
        {
            textViewBluetoothStatus.setText("Paired with " + devicePair.getPairedDeivceUUID());
        }
    }

    @Override
    public void onClick(View view)
    {
        if(bluetoothAdapter == null)
        {
            Toast.makeText(getContext(),
                    "Sorry, this device doesn't have a bluetooth adapter.",Toast.LENGTH_SHORT).show();
            return;
        }
        if(view == buttonEnableBluetooth)
        {
            onButtonEnableBluetooth();
            return;
        }
        bluetoothManager.tryConnection();
    }

    void onButtonEnableBluetooth() {
        if(bluetoothAdapter == null) {
            Toast.makeText(getContext(),
                    "Sorry, this device doesn't have a bluetooth adapter.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if(!bluetoothAdapter.isEnabled()) {
            controller.tryEnableBluetooth();
        }
        updateLayout();
    }

    void onButtonSearchDevices()
    {
        ((ArrayAdapter<BluetoothDevice>)listBluetoothDevice.getAdapter()).clear();
        controller.beginBluetoothDiscovery();
    }

    void onButtonPairDevice(BluetoothDevice device)
    {
        devicePair.setPairedDeviceUUID(device.getAddress());
    }

    void onDeviceDiscovered(BluetoothDevice device) {
        Toast.makeText(getContext(),"Found UUID:" + device.getAddress(),Toast.LENGTH_LONG).show();
        ((ArrayAdapter<BluetoothDevice>)listBluetoothDevice.getAdapter()).add(device);
        updateLayout();
    }

    void onDebugButtonTestBluetoothSync() {
        HealthDataManager healthDataManager = HealthDataManager.getInstance();
        healthDataManager.madeUpData();
    }

    void onDebugButtonTestNetworkSync() {
        HealthDataManager healthDataManager = HealthDataManager.getInstance();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url ="http://www.tinytangent.com:8000/api/report_data/";

        JSONArray requestData = new JSONArray();
        try {
            for (HealthDataManager.PollutionData pd : healthDataManager.getPollutionData()) {
                JSONObject pollutionData = new JSONObject();
                pollutionData.put("x", pd.location.getLatitude());
                pollutionData.put("y", pd.location.getLongitude());
                pollutionData.put("pm25Value", pd.pm25Value);
                pollutionData.put("pm10Value", pd.pm10Value);
                pollutionData.put("uploadDevice", pd.deviceMACAddress);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                pollutionData.put("uploadTime",format.format(pd.dateTime.getTime()));
                requestData.put(pollutionData);
            }
        }
        catch(Exception e) {
            return;
        }
        final String requestString = requestData.toString();
        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(getContext(),  response, Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public byte[] getBody() {
                return requestString.getBytes();
            }
        };

        queue.add(jsonRequest);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case SyncManager.MESSAGE_CONNECTION_STATE_CHANGED:
                break;
            case SyncManager.MESSAGE_DATA_RECEIVED:
                break;
        }
        return true;
    }
}
