package com.portablehealthinstrument.healthmonitor;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.portablehealthinstrument.healthmonitor.SampleFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
    protected String titles[] ;
    protected SampleFragment fragments[];

    @Override
    public void onPageScrolled(int a,float b,int c) {}

    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void onPageSelected(int page)
    {
        fragments[page].updateData();
    }

    public ViewPagerAdapter(FragmentManager fm, String[] _titles) {
        super(fm);
        fragments = new SampleFragment[_titles.length];
        titles=_titles;
    }

    @Override
    public Fragment getItem(int position) {
        if(position < titles.length)
        {
            fragments[position] = SampleFragment.newInstance(position);
            return fragments[position];
        }
        else return null;
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }

}