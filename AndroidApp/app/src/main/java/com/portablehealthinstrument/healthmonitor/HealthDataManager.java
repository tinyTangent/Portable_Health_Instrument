package com.portablehealthinstrument.healthmonitor;

import android.location.Location;

import com.portablehealthinstrument.healthmonitor.Model.DevicePair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by tansinan on 2015/5/16.
 */
public class HealthDataManager {

    //Inner class to descripe a footstep/pollution event.
    class PollutionData {
        double pm25Value;
        double pm10Value;
        Location location;
        GregorianCalendar dateTime;
        String deviceMACAddress;
    }

    class SportData {
        GregorianCalendar dateTime;
        double pm25Value;
        double pm10Value;
        String deviceMACAddress;
    }

    //Make HealthDataManager a singleton class
    protected static HealthDataManager instance;
    protected HealthDataManager() {}
    static HealthDataManager getInstance() {
        if(instance == null) {
            instance = new HealthDataManager();
        }
        return instance;
    }

    protected List<SportData> sportsData = new ArrayList<>();
    protected List<PollutionData> pollutionData = new ArrayList<>();
    protected Location recentLocation = null;
    public List<SportData> getSportsData()
    {
        return sportsData;
    }
    public List<PollutionData> getPollutionData()
    {
        return pollutionData;
    }

    public void updateRecentLocation(Location location) {
        this.recentLocation = location;
    }

    public Location getRecentLocation() {
        return this.recentLocation;
    }

    public boolean addSportData(double pm25Value, double pm10Value, GregorianCalendar dateTime)
    {
        SportData sportData = new SportData();
        sportData.pm25Value = pm25Value;
        sportData.pm10Value = pm10Value;
        sportData.dateTime = dateTime;
        sportData.deviceMACAddress = "FE:FF:FF:FF:FF:FF";
        this.sportsData.add(sportData);
        return true;
    }

    public boolean addPollutionData(double pm25Value, double pm10Value) {
        if(recentLocation == null) {
            //TODO: Add a toast to notify?
            return false;
        }
        PollutionData pollutionData = new PollutionData();
        pollutionData.pm10Value = pm10Value;
        pollutionData.pm25Value = pm25Value;
        pollutionData.location = recentLocation;
        pollutionData.dateTime = new GregorianCalendar();
        DevicePair devicePair = new DevicePair();
        pollutionData.deviceMACAddress = devicePair.getPairedDeivceUUID();
        this.pollutionData.add(pollutionData);

        return true;
    }

    //Made up some data... This function is only intended for debugging!
    //TODO: Remove this method.
    public void madeUpData() {
        for(int i = 0; i < 1033; i++) {
            double pm25Value = 200 * Math.random();
            double pm10Value = 200 * Math.random();
            GregorianCalendar dateTime = new GregorianCalendar();
            dateTime.set(Calendar.HOUR_OF_DAY, (int)(Math.random() * 8 + 8));
            dateTime.set(Calendar.MINUTE, (int)(Math.random() * 60));
            dateTime.set(Calendar.SECOND, (int)(Math.random() * 60));
            addSportData(pm25Value, pm10Value, dateTime);
        }
    }
}
