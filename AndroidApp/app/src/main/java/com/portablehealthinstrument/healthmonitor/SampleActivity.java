package com.portablehealthinstrument.healthmonitor;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.Button;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class SampleActivity extends ActionBarActivity {

    Controller controller;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    protected View testButton;
    private ListView mDrawerList;
    ViewPager pager;
    final static protected String titles[] =
            new String[]{"健康指数", "锻炼信息", "污染记录", "个人信息"};
    private Toolbar toolbar;

    final static int REQUEST_ENABLE_BLUETOOTH = 100;

    SlidingTabLayout slidingTabLayout;

    /**
     * Try to enable bluetooth adapter via android intent.
     * @return if the attempt succeeded. (As long as the device have a bluetooth adapter, this will
     * always return true).
     */
    public boolean tryEnableBluetooth()
    {
        //Get the bluetooth adapter object.
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //Check if the device do have an bluetooth adapter.
        if (bluetoothAdapter == null) {
            return false;
        }

        //Otherwise, use the android built-in intent to notify the user to enable bluetooth.
        if(!bluetoothAdapter.isEnabled())
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH);
        }

        //As long as the device have bluetooth adapter, this method always return true
        //(Because android intent is asynchronous)
        return true;
    }

    /**
     * This is a overloaded method to handle all possible result from called intent.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            //Handles the result of the bluetooth intent
            case REQUEST_ENABLE_BLUETOOTH:
                if(resultCode == RESULT_OK) {
                    Controller.getInstance().onTryEnableBluetoothResult(true);
                }
                else {
                    Controller.getInstance().onTryEnableBluetoothResult(false);
                }
                break;
            case 101:
                if(resultCode == RESULT_OK) {
                    File file = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS), "__temp_image.png");
                    File file2 = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS), "__temp_image2.png");
                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inMutable = true;
                        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                        Canvas c = new Canvas(bitmap);
                        Paint p = new Paint();
                        float fontSize = Math.min(bitmap.getHeight(), bitmap.getWidth()) / 16.0f;
                        p.setTextSize(fontSize);
                        p.setColor(Color.RED);
                        HealthDataManager h = HealthDataManager.getInstance();
                        c.drawText("今天运动步数" + 1356, fontSize, fontSize, p);
                        c.drawText("PM2.5 = 354", fontSize, fontSize * 2.5f, p);
                        c.drawText("PM10 = 214", fontSize, fontSize * 4.0f, p);
                        Calendar calendar = new GregorianCalendar();
                        String dateString = "" + calendar.get(Calendar.YEAR) + "-" +
                                (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) +
                                " " + calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                            (calendar.get(Calendar.MINUTE) >= 10 ? "" : "0")+ calendar.get(Calendar.MINUTE);
                        c.drawText(dateString, fontSize, fontSize * 5.5f, p);
                        file.getParentFile().mkdirs();
                        FileOutputStream out = new FileOutputStream(file2);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                        out.close();
                    } catch (Exception e) {
                        Toast.makeText(this, "ERROR!", Toast.LENGTH_LONG).show();
                    }
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("image/*");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file2));
                    startActivity(Intent.createChooser(sharingIntent, "Share image using"));
                }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Register location service
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                String positionStr = location.getLatitude() + " " + location.getLongitude();
                HealthDataManager.getInstance().updateRecentLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        //Update the activity stored in the controller.
        controller = Controller.getInstance();
        controller.activity = this;

        setContentView(R.layout.activity_sample);
        testButton = new Button(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.navdrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_ab_drawer);
        }
        pager = (ViewPager) findViewById(R.id.viewpager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), titles);
        pager.setAdapter(viewPagerAdapter);

        slidingTabLayout.setViewPager(pager);
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return Color.WHITE;
            }
        });
        slidingTabLayout.setOnPageChangeListener(viewPagerAdapter);
        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(drawerToggle);
        String[] values = new String[]{
                "DEFAULT", "RED", "BLUE", "MATERIAL GREY"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        mDrawerList.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_500));
                        toolbar.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_500));
                        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_500));
                        slidingTabLayout.removeAllViews();
                        //slidingTabLayout.addView(testButton);
                        mDrawerLayout.closeDrawer(Gravity.START);
                        break;
                    case 1:
                        mDrawerList.setBackgroundColor(getResources().getColor(R.color.red));
                        toolbar.setBackgroundColor(getResources().getColor(R.color.red));
                        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.red));
                        mDrawerLayout.closeDrawer(Gravity.START);

                        break;
                    case 2:
                        mDrawerList.setBackgroundColor(getResources().getColor(R.color.blue));
                        toolbar.setBackgroundColor(getResources().getColor(R.color.blue));
                        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                        mDrawerLayout.closeDrawer(Gravity.START);

                        break;
                    case 3:
                        mDrawerList.setBackgroundColor(getResources().getColor(R.color.material_blue_grey_800));
                        toolbar.setBackgroundColor(getResources().getColor(R.color.material_blue_grey_800));
                        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.material_blue_grey_800));
                        mDrawerLayout.closeDrawer(Gravity.START);

                        break;
                }
            }
        });
    }

    //@Override
    public boolean handleMessage(Message message) {
        switch(message.what)
        {
            case SyncManager.MESSAGE_CONNECTION_STATE_CHANGED:
                break;
            case SyncManager.MESSAGE_DATA_RECEIVED:
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(Gravity.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }



    public void cancelBluetoothDiscovery()
    {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if(adapter == null) return;
        if(!adapter.isEnabled()) return;
        adapter.cancelDiscovery();
        try {
            unregisterReceiver(receiverBluetooth);
        }
        catch(Exception e){}
        Toast.makeText(this, "Bluetooth discovery cancelled", Toast.LENGTH_LONG).show();
    }

    final BroadcastReceiver receiverBluetooth = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Controller.getInstance().onBluetoothDeviceDiscovered(device);
            }
        }
    };

    public boolean beginBluetoothDiscovery()
    {
        Toast.makeText(this, "Bluetooth discovery begin", Toast.LENGTH_LONG).show();
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if(adapter == null) return false;
        if(!adapter.isEnabled()) return false;
        adapter.startDiscovery();
        registerReceiver(receiverBluetooth, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        new CountDownTimer(10000, 10000) {
            @Override
            public void onTick(long millisUntilFinished) {}

            @Override
            public void onFinish() {
                SampleActivity.this.cancelBluetoothDiscovery();
            }
        }.start();
        return true;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        cancelBluetoothDiscovery();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        cancelBluetoothDiscovery();
    }
}
