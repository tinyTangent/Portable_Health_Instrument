package com.portablehealthinstrument.healthmonitor.Model;

import android.content.SharedPreferences;

import com.portablehealthinstrument.healthmonitor.Controller;

/**
 * Created by tansinan on 2015/10/19.
 */
public class DevicePair {

    static final String STORAGE_NAME = "DevicePair";
    static final String STORAGE_KEY_UUID = "DeviceUUID";

    /**
     * Get the UUID of the device that is currently paired with the App.
     * @return The UUID, or empty string if no device is paired.
     */
    public String getPairedDeivceUUID()
    {
        SharedPreferences preferences = Controller.getInstance().getSharedPreferences(STORAGE_NAME);
        return preferences.getString(STORAGE_KEY_UUID, "");
    }

    /**
     * Get the UUID of the device that is currently paired with the App.
     * @param UUID The UUID of the device
     * @return If the opeartion succeeded. Currently always true.
     */
    public boolean setPairedDeviceUUID(String UUID)
    {
        SharedPreferences preferences = Controller.getInstance().getSharedPreferences(STORAGE_NAME);
        preferences.edit().putString(STORAGE_KEY_UUID, UUID).apply();
        return true;
    }
}
