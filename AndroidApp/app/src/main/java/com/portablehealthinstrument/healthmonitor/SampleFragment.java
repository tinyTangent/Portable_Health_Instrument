package com.portablehealthinstrument.healthmonitor;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class SampleFragment extends Fragment {

    private static final String ARG_POSITION = "position";

    protected View managedView;

    private int position;

    public static SampleFragment newInstance(int position) {
        SampleFragment f = new SampleFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        position = getArguments().getInt(ARG_POSITION);
        if(position == 0)
        {
            View rootView = inflater.inflate(R.layout.connection_manager_view, container, false);
            rootView.setBackgroundColor(Color.DKGRAY);
            managedView = rootView;
            return rootView;
        }
        else if(position == 1)
        {
            SportInfoView rootView = (SportInfoView)inflater.inflate(R.layout.sport_info_view, container, false);
            rootView.setBackgroundColor(Color.DKGRAY);
            rootView.updateData();
            managedView = rootView;
            return rootView;
        }
        else if (position == 2)
        {
            PollutionInfoView rootView =
                    (PollutionInfoView)inflater.inflate(R.layout.pollution_info_page, container, false);
            rootView.updateData(new ArrayList<Integer>());
            rootView.setBackgroundColor(Color.DKGRAY);
            HomeColumnar diagram = (HomeColumnar)rootView.findViewById(R.id.diagram);
            ArrayList<Score> list = new ArrayList<Score>();
            for(int i=0;i<30;i++)
            {
                Score score = new Score();
                score.date = "aaaa";
                score.score = ((int)(Math.random()*100));
                score.total = ((int)(Math.random()*100));
                list.add(score);
            }
            diagram.setColumnarData(list);
            managedView = rootView;
            return rootView;
        }
        else if (position == 3)
        {
            View rootView = inflater.inflate(R.layout.personal_info_view, container, false);
            rootView.setBackgroundColor(Color.DKGRAY);
            managedView = rootView;
            return rootView;
        }
        View rootView = inflater.inflate(R.layout.page, container, false);

        ProgressBarCircular progressBarCircular = (ProgressBarCircular) rootView.findViewById(R.id.progress);
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fabButton);
        fab.setDrawableIcon(getResources().getDrawable(R.drawable.plus));
        switch (position) {
            case 0:
                fab.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_500));
                progressBarCircular.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_500));
                break;
            case 1:
                fab.setBackgroundColor(getResources().getColor(R.color.red));
                progressBarCircular.setBackgroundColor(getResources().getColor(R.color.red));

                break;
            case 2:
                progressBarCircular.setBackgroundColor(getResources().getColor(R.color.blue));
                fab.setBackgroundColor(getResources().getColor(R.color.blue));

                break;
            case 3:
                fab.setBackgroundColor(getResources().getColor(R.color.material_blue_grey_800));
                progressBarCircular.setBackgroundColor(getResources().getColor(R.color.material_blue_grey_800));

                break;
        }

        managedView = rootView;
        return rootView;
    }

    public void updateData()
    {
        position = getArguments().getInt(ARG_POSITION);
        if(position==1) {
            ((SportInfoView)managedView).updateData();
        }
        else if (position == 2) {
            ((PollutionInfoView)managedView).updateData(new ArrayList<Integer>());
        }
    }
}