package com.portablehealthinstrument.healthmonitor;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.List;

/**
 * Created by tansinan on 2016/2/28.
 */
public class PollutionInfoView extends RelativeLayout {
    protected TimeBarGraph diagram;
    protected ArcNumberView arcNumberView;
    protected Button datePickerButton;
    public void updateData(List<Integer> d){
        double averagePM25 = 0.0;
        HealthDataManager healthDataManager = HealthDataManager.getInstance();
        for(HealthDataManager.PollutionData data: healthDataManager.getPollutionData()) {
            averagePM25 += data.pm25Value;
        }
        averagePM25 /= healthDataManager.getPollutionData().size();
        arcNumberView.setValue((int)averagePM25);
    }
    public PollutionInfoView(Context context) {
        super(context);
    }

    public PollutionInfoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Controller.getInstance().pollutionInfoView = this;
        arcNumberView = (ArcNumberView)this.findViewById(R.id.view2);
        arcNumberView.setValue(121);
        datePickerButton = (Button)findViewById(R.id.button_set_date);
        datePickerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonSetDate();
            }
        });
    }
    
    protected void onButtonSetDate() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url ="http://www.baidu.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getContext(), "Response is: " + response.substring(0, 50), Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error T^T", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
        sendIntent.setType("text/plain");
        getContext().startActivity(sendIntent);
        /*Uri path = Uri.parse("android.resource://com.portablehealthinstrument.healthmonitor/" + R.drawable.ic_launcher);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, path);
        shareIntent.setType("image/jpeg");
        getContext().startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.app_name)));*/
    }
}
