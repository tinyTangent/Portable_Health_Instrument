package com.portablehealthinstrument.healthmonitor;

import android.bluetooth.*;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.*;

import java.util.List;

import io.palaima.smoothbluetooth.Device;
import io.palaima.smoothbluetooth.SmoothBluetooth;

/**
 * Created by tansinan on 2015/9/30.
 * Controller is the controller class for Helath Monitor, It receives various actions
 * from UI component, interacting with models/activities, and then report the result.
 *
 * This class should be an singleton. Only one instance should appear in on application.
 */
public class Controller {

    static Controller instance;

    private Controller()
    {
    }

    public static Controller getInstance()
    {
        if(instance != null) return instance;
        instance = new Controller();
        return instance;
    }

    protected SampleActivity activity;
    ConnectionManagerView viewConnectionManager;
    SportInfoView viewSportInfo;
    PollutionInfoView pollutionInfoView;

    /**
     * The tryEnableBluetooth method calls corresponding method in th Activity.
     * The reason for using this is that only an activity can create intent, and receive the result
     * code from the intent.
     */
    public void tryEnableBluetooth()
    {
        activity.tryEnableBluetooth();
    }

    /**
     * This method is intented to be called by SampleActivity.onActivityResult to report whether
     * the user give permission to enable bluetooth.
     * @param result whether the user give permission to enable bluetooth
     */
    public void onTryEnableBluetoothResult(boolean result)
    {
        if(result == false)
        {
            Toast.makeText(activity,"Cannot activate bluetooth.",Toast.LENGTH_SHORT).show();
        }
        else
        {
            viewConnectionManager.updateLayout();
        }
    }

    /**
     * The beginBluetoothDiscovery method calls corresponding method in th Activity.
     * It's mainly implemented in the activity, for We need context to do something.
     */
    public void beginBluetoothDiscovery()
    {
        activity.beginBluetoothDiscovery();
    }

    /**
     * The cancelBluetoothDiscovery method calls corresponding method in th Activity.
     * It's mainly implemented in the activity, for We need context to do something.
     */
    public void cancelBluetoothDiscovery()
    {
        activity.cancelBluetoothDiscovery();
    }

    public void onBluetoothDeviceDiscovered(BluetoothDevice device)
    {
        viewConnectionManager.onDeviceDiscovered(device);
    }

    public SharedPreferences getSharedPreferences (String name)
    {
        return activity.getSharedPreferences(name, Context.MODE_PRIVATE);
    }
}
