from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import *
from datetime import datetime
import json

# Create your views here.


def index(request):
    return render(request, 'index.html')


def ajax_fetch_data(request):
    ret_data = []
    allPollutionData = PollutionData.objects.all()
    for pollutionData in allPollutionData:
        arr = {
            "pm25Value": pollutionData.pm25Value,
            "pm10Value": pollutionData.pm10Value,
            "uploadDevice": pollutionData.uploadDevice,
            "uploadTime": str(pollutionData.uploadTime),
            "x": pollutionData.location.x,
            "y": pollutionData.location.y
        }
        ret_data.append(arr)
    return HttpResponse(json.dumps(ret_data))


@csrf_exempt
def ajax_report_data(request):
    count = 0
    request_data = json.loads(request.body.decode("utf-8"))
    for data in request_data:
        if "uploadTime" not in data:
            data["uploadTime"] = datetime.now()

        if "uploadDevice" not in data:
            data["uploadDevice"] = "FE:00:00:00:00:01"

        if PollutionData.objects.filter( \
        location="POINT(" + str(data["x"]) + " " + str(data["y"]) + ")", \
        uploadTime=data["uploadTime"], \
        uploadDevice=data["uploadDevice"]):
            continue
        pollution_data = PollutionData()
        pollution_data.pm25Value = data["pm25Value"]
        pollution_data.pm10Value = data["pm10Value"]
        pollution_data.location = \
            "POINT(" + str(data["x"]) + " " + str(data["y"]) + ")"
        if "uploadTime" in data:
            pollution_data.uploadTime = data["uploadTime"]
        else:
            pollution_data.uploadTime = datetime.now()

        if "uploadDevice" in data:
            pollution_data.uploadDevice = data["uploadDevice"]
        else:
            pollution_data.uploadDevice = "FE:00:00:00:00:01"
        pollution_data.save() #TODO : use batch save
        count += 1
    return HttpResponse('{"count": ' + str(count) + '}')
