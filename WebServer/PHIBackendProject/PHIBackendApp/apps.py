from django.apps import AppConfig


class PhibackendappConfig(AppConfig):
    name = 'PHIBackendApp'
