from django.contrib.gis.db import models


class PollutionData(models.Model):
    pm25Value = models.FloatField()
    pm10Value = models.FloatField()
    uploadDevice = models.CharField(max_length=20)
    # TODO: uploadUser = models.ForeignKey blabla
    uploadTime = models.DateTimeField()
    location = models.PointField()

    # Returns the string representation of the model.
    def __str__(self):
        return "Pollution data record:" + \
            "pm2.5:" + str(self.pm25Value) + ";pm10:" + str(self.pm10Value) + \
            "@" + \
            str(self.location.x) + "," + str(self.location.y) + "@" + \
            str(self.uploadTime)

# Create your models here.
