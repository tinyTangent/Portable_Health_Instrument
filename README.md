# Portable Health Instrument
This project aims at creating a wearable device that can measure both sports and air pollution, and sync them with an mobile device.
There are 3 applications inside this repository:
* Firmware : The firmware intended for a KL25Z128 device, with a number of sensors on it.
* AndroidApp : The android application to monitor the embedded device.
* Web Server : A Django application used to provide internet sync and data analyze service.

## Team members:
* Tan Si Nan
* Ma Yu Gao