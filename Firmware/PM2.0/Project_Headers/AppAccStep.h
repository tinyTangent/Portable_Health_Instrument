/*
 *  AppAccStep.h
 *
 *  Created on: Jun 18, 2015
 *      Author: leo
 */

#ifndef APPACCSTEP_H_
#define APPACCSTEP_H_

void AppAccStep_drawHandler();
void AppAccStep_onKeyPressed(char key);
void AppAccStep_init();
App* AppAccStep_getApp();


#endif /* APPACCSTEP_H_ */
