/*
 * AppAlarmClock.h
 *
 *  Created on: Jun 18, 2015
 *      Author: leo
 */

#ifndef APPALARMCLOCK_H_
#define APPALARMCLOCK_H_

void AppAlarmClock_drawHandler();

void AppAlarmClock_onKeyPressed(char key);

void AppAlarmClock_init();

App* AppAlarmClock_getApp();

#endif /* APPALARMCLOCK_H_ */
