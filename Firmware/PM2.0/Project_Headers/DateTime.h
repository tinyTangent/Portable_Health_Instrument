/*
 * OneWire.h
 *
 *  Created on: Oct 26, 2015
 *      Author: tansinan
 */

#ifndef UTIL_DATETIME_H_
#define UTIL_DATETIME_H_

#include "Includes.h"

typedef struct {
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t msec; // in 0.1s
} DateTime;

DateTime* DateTime_getCurrent();
void DateTime_tick();

#endif
