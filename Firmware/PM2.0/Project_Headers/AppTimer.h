/*
 * AppTimer.h
 *
 *  Created on: Jun 1, 2015
 *      Author: leo
 */

#ifndef APPTIMER_H_
#define APPTIMER_H_

void AppTimer_drawHandler();

void AppTimer_onKeyPressed(char key);

void AppStpeSensor_init();

App* AppTimer_getApp();

#endif /* APPTIMER_H_ */
