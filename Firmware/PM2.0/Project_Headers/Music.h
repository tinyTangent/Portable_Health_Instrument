/*
 * Music.h
 *
 *  Created on: Apr 16, 2015
 *      Author: leo
 */

#ifndef MUSIC_H_
#define MUSIC_H_

void MusicPlay(int music); 
void MusicStop();
void PianoSet(int note);
void PianoClrAll();
void PianoDelete();
void PianoPlay(); 
void ConfirmNote ();



#endif /* MUSIC_H_ */
