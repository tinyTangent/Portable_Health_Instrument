/*
 * Extern.h
 *
 *  Created on: May 6, 2015
 *      Author: leo
 */

#ifndef EXTERN_H_
#define EXTERN_H_

#include "AppFramework.h"

typedef signed short int   int16;

extern unsigned int number;
extern unsigned int flag_100ms;
extern unsigned char show[6];
extern unsigned char MMA8451_data[6];
extern unsigned char lED1_light_flag;
extern unsigned char lED2_light_flag;
extern unsigned char lED3_light_flag;
extern unsigned char lED4_light_flag;
extern int acc_0;
extern unsigned int ADC_Result;


//MMA8451Q.c
extern int  acc_x;
extern int acc_y;
extern int acc_z;

//main.c
extern char str[20];

//functon.c
extern int handleChangeFlag;
extern int buzzFlag;
/*just for test*/
//from:MMA8451Q.c
extern int acc_0;
extern int _acc;

//Music.h
extern int musicChoice; 
extern int noteChoice;
extern char noteBuffer[40];
extern int noteCount;


//Applications
extern App OLED_MENU_app;
extern App AppSensorTest_app;
extern App JavaGame_app;
extern App AppStepSensor_app;
extern App AppMusic_app;
extern App AppTimer_app;
extern App AppReactTimer_app;
extern App AppPiano_app;
extern int step;
extern int accStep;
extern App AppAlarmClock_app;
extern App AppAccStep_app;

extern App AppSnakeGame_app;
extern struct Snake snake;
extern int gameFlag;
extern App AppMusicTutor_app;


extern int snake_x;
extern int snake_y;
//SMG.c
extern int enableTimer;
extern int minute;
extern int second;
extern int second_1;
extern int second_2;

extern int enableTimer_react;
extern int minute_react;
extern int second_react;
extern int second_1_react;
extern int second_2_react;

extern int AlarmClockEidt;
extern int AlarmClockEditChoice;
extern int minuteSet;
extern int secondSet;
extern int sencond_1_set;
extern int sencond_2_set;

extern unsigned int ADC_LightIntensity;
extern unsigned int ADC_Temperature;
extern unsigned int ADC_potentiometer;

extern int power;

extern int receiveNumber;
#endif /* EXTERN_H_ */
