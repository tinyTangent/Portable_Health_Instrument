/*
 * OneWire.h
 *
 *  Created on: Oct 26, 2015
 *      Author: tansinan
 */

#ifndef UTIL_STORAGE_H_
#define UTIL_STORAGE_H_

#include <stdint.h>
#include "DateTime.h"

typedef struct {
	DateTime dateTime;
	int pm25Value;
	int pm10Value;
} HealthData;

void recordSport();
void recordPollution(int val);
uint16_t getAvailableDataCount();
uint32_t getRecordDuration();
uint16_t* getPollutionData();
uint16_t* getSportData();

#endif
