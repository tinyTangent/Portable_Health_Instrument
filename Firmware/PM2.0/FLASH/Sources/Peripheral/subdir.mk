################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Peripheral/ADC.c" \
"../Sources/Peripheral/Function.c" \
"../Sources/Peripheral/MMA8451Q.c" \
"../Sources/Peripheral/OLED.c" \
"../Sources/Peripheral/OLEDFB.c" \
"../Sources/Peripheral/OLEDFONT.c" \
"../Sources/Peripheral/SMG.c" \
"../Sources/Peripheral/UART.c" \
"../Sources/Peripheral/sa_mtb.c" \
"../Sources/Peripheral/sht21.c" \

C_SRCS += \
../Sources/Peripheral/ADC.c \
../Sources/Peripheral/Function.c \
../Sources/Peripheral/MMA8451Q.c \
../Sources/Peripheral/OLED.c \
../Sources/Peripheral/OLEDFB.c \
../Sources/Peripheral/OLEDFONT.c \
../Sources/Peripheral/SMG.c \
../Sources/Peripheral/UART.c \
../Sources/Peripheral/sa_mtb.c \
../Sources/Peripheral/sht21.c \

OBJS += \
./Sources/Peripheral/ADC.o \
./Sources/Peripheral/Function.o \
./Sources/Peripheral/MMA8451Q.o \
./Sources/Peripheral/OLED.o \
./Sources/Peripheral/OLEDFB.o \
./Sources/Peripheral/OLEDFONT.o \
./Sources/Peripheral/SMG.o \
./Sources/Peripheral/UART.o \
./Sources/Peripheral/sa_mtb.o \
./Sources/Peripheral/sht21.o \

C_DEPS += \
./Sources/Peripheral/ADC.d \
./Sources/Peripheral/Function.d \
./Sources/Peripheral/MMA8451Q.d \
./Sources/Peripheral/OLED.d \
./Sources/Peripheral/OLEDFB.d \
./Sources/Peripheral/OLEDFONT.d \
./Sources/Peripheral/SMG.d \
./Sources/Peripheral/UART.d \
./Sources/Peripheral/sa_mtb.d \
./Sources/Peripheral/sht21.d \

OBJS_QUOTED += \
"./Sources/Peripheral/ADC.o" \
"./Sources/Peripheral/Function.o" \
"./Sources/Peripheral/MMA8451Q.o" \
"./Sources/Peripheral/OLED.o" \
"./Sources/Peripheral/OLEDFB.o" \
"./Sources/Peripheral/OLEDFONT.o" \
"./Sources/Peripheral/SMG.o" \
"./Sources/Peripheral/UART.o" \
"./Sources/Peripheral/sa_mtb.o" \
"./Sources/Peripheral/sht21.o" \

C_DEPS_QUOTED += \
"./Sources/Peripheral/ADC.d" \
"./Sources/Peripheral/Function.d" \
"./Sources/Peripheral/MMA8451Q.d" \
"./Sources/Peripheral/OLED.d" \
"./Sources/Peripheral/OLEDFB.d" \
"./Sources/Peripheral/OLEDFONT.d" \
"./Sources/Peripheral/SMG.d" \
"./Sources/Peripheral/UART.d" \
"./Sources/Peripheral/sa_mtb.d" \
"./Sources/Peripheral/sht21.d" \

OBJS_OS_FORMAT += \
./Sources/Peripheral/ADC.o \
./Sources/Peripheral/Function.o \
./Sources/Peripheral/MMA8451Q.o \
./Sources/Peripheral/OLED.o \
./Sources/Peripheral/OLEDFB.o \
./Sources/Peripheral/OLEDFONT.o \
./Sources/Peripheral/SMG.o \
./Sources/Peripheral/UART.o \
./Sources/Peripheral/sa_mtb.o \
./Sources/Peripheral/sht21.o \


# Each subdirectory must supply rules for building sources it contributes
Sources/Peripheral/ADC.o: ../Sources/Peripheral/ADC.c
	@echo 'Building file: $<'
	@echo 'Executing target #11 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/ADC.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/ADC.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/Function.o: ../Sources/Peripheral/Function.c
	@echo 'Building file: $<'
	@echo 'Executing target #12 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/Function.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/Function.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/MMA8451Q.o: ../Sources/Peripheral/MMA8451Q.c
	@echo 'Building file: $<'
	@echo 'Executing target #13 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/MMA8451Q.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/MMA8451Q.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/OLED.o: ../Sources/Peripheral/OLED.c
	@echo 'Building file: $<'
	@echo 'Executing target #14 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/OLED.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/OLED.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/OLEDFB.o: ../Sources/Peripheral/OLEDFB.c
	@echo 'Building file: $<'
	@echo 'Executing target #15 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/OLEDFB.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/OLEDFB.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/OLEDFONT.o: ../Sources/Peripheral/OLEDFONT.c
	@echo 'Building file: $<'
	@echo 'Executing target #16 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/OLEDFONT.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/OLEDFONT.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/SMG.o: ../Sources/Peripheral/SMG.c
	@echo 'Building file: $<'
	@echo 'Executing target #17 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/SMG.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/SMG.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/UART.o: ../Sources/Peripheral/UART.c
	@echo 'Building file: $<'
	@echo 'Executing target #18 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/UART.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/UART.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/sa_mtb.o: ../Sources/Peripheral/sa_mtb.c
	@echo 'Building file: $<'
	@echo 'Executing target #19 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/sa_mtb.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/sa_mtb.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Peripheral/sht21.o: ../Sources/Peripheral/sht21.c
	@echo 'Building file: $<'
	@echo 'Executing target #20 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Peripheral/sht21.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Peripheral/sht21.o"
	@echo 'Finished building: $<'
	@echo ' '


