################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Service/BackgroundPM25.c" \
"../Sources/Service/BackgroundUART.c" \

C_SRCS += \
../Sources/Service/BackgroundPM25.c \
../Sources/Service/BackgroundUART.c \

OBJS += \
./Sources/Service/BackgroundPM25.o \
./Sources/Service/BackgroundUART.o \

C_DEPS += \
./Sources/Service/BackgroundPM25.d \
./Sources/Service/BackgroundUART.d \

OBJS_QUOTED += \
"./Sources/Service/BackgroundPM25.o" \
"./Sources/Service/BackgroundUART.o" \

C_DEPS_QUOTED += \
"./Sources/Service/BackgroundPM25.d" \
"./Sources/Service/BackgroundUART.d" \

OBJS_OS_FORMAT += \
./Sources/Service/BackgroundPM25.o \
./Sources/Service/BackgroundUART.o \


# Each subdirectory must supply rules for building sources it contributes
Sources/Service/BackgroundPM25.o: ../Sources/Service/BackgroundPM25.c
	@echo 'Building file: $<'
	@echo 'Executing target #9 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Service/BackgroundPM25.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Service/BackgroundPM25.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Service/BackgroundUART.o: ../Sources/Service/BackgroundUART.c
	@echo 'Building file: $<'
	@echo 'Executing target #10 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Service/BackgroundUART.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Service/BackgroundUART.o"
	@echo 'Finished building: $<'
	@echo ' '


