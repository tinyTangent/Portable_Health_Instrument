################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Apps/AppAccStep.c" \
"../Sources/Apps/AppFramework.c" \
"../Sources/Apps/AppPM25.c" \
"../Sources/Apps/AppSensorTest.c" \
"../Sources/Apps/AppStepSensor.c" \

C_SRCS += \
../Sources/Apps/AppAccStep.c \
../Sources/Apps/AppFramework.c \
../Sources/Apps/AppPM25.c \
../Sources/Apps/AppSensorTest.c \
../Sources/Apps/AppStepSensor.c \

OBJS += \
./Sources/Apps/AppAccStep.o \
./Sources/Apps/AppFramework.o \
./Sources/Apps/AppPM25.o \
./Sources/Apps/AppSensorTest.o \
./Sources/Apps/AppStepSensor.o \

C_DEPS += \
./Sources/Apps/AppAccStep.d \
./Sources/Apps/AppFramework.d \
./Sources/Apps/AppPM25.d \
./Sources/Apps/AppSensorTest.d \
./Sources/Apps/AppStepSensor.d \

OBJS_QUOTED += \
"./Sources/Apps/AppAccStep.o" \
"./Sources/Apps/AppFramework.o" \
"./Sources/Apps/AppPM25.o" \
"./Sources/Apps/AppSensorTest.o" \
"./Sources/Apps/AppStepSensor.o" \

C_DEPS_QUOTED += \
"./Sources/Apps/AppAccStep.d" \
"./Sources/Apps/AppFramework.d" \
"./Sources/Apps/AppPM25.d" \
"./Sources/Apps/AppSensorTest.d" \
"./Sources/Apps/AppStepSensor.d" \

OBJS_OS_FORMAT += \
./Sources/Apps/AppAccStep.o \
./Sources/Apps/AppFramework.o \
./Sources/Apps/AppPM25.o \
./Sources/Apps/AppSensorTest.o \
./Sources/Apps/AppStepSensor.o \


# Each subdirectory must supply rules for building sources it contributes
Sources/Apps/AppAccStep.o: ../Sources/Apps/AppAccStep.c
	@echo 'Building file: $<'
	@echo 'Executing target #22 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Apps/AppAccStep.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Apps/AppAccStep.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Apps/AppFramework.o: ../Sources/Apps/AppFramework.c
	@echo 'Building file: $<'
	@echo 'Executing target #23 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Apps/AppFramework.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Apps/AppFramework.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Apps/AppPM25.o: ../Sources/Apps/AppPM25.c
	@echo 'Building file: $<'
	@echo 'Executing target #24 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Apps/AppPM25.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Apps/AppPM25.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Apps/AppSensorTest.o: ../Sources/Apps/AppSensorTest.c
	@echo 'Building file: $<'
	@echo 'Executing target #25 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Apps/AppSensorTest.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Apps/AppSensorTest.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Apps/AppStepSensor.o: ../Sources/Apps/AppStepSensor.c
	@echo 'Building file: $<'
	@echo 'Executing target #26 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Apps/AppStepSensor.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Apps/AppStepSensor.o"
	@echo 'Finished building: $<'
	@echo ' '


