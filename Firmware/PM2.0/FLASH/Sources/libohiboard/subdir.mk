################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/libohiboard/clock.c" \
"../Sources/libohiboard/i2c_KL25Z4.c" \
"../Sources/libohiboard/utility.c" \

C_SRCS += \
../Sources/libohiboard/clock.c \
../Sources/libohiboard/i2c_KL25Z4.c \
../Sources/libohiboard/utility.c \

OBJS += \
./Sources/libohiboard/clock.o \
./Sources/libohiboard/i2c_KL25Z4.o \
./Sources/libohiboard/utility.o \

C_DEPS += \
./Sources/libohiboard/clock.d \
./Sources/libohiboard/i2c_KL25Z4.d \
./Sources/libohiboard/utility.d \

OBJS_QUOTED += \
"./Sources/libohiboard/clock.o" \
"./Sources/libohiboard/i2c_KL25Z4.o" \
"./Sources/libohiboard/utility.o" \

C_DEPS_QUOTED += \
"./Sources/libohiboard/clock.d" \
"./Sources/libohiboard/i2c_KL25Z4.d" \
"./Sources/libohiboard/utility.d" \

OBJS_OS_FORMAT += \
./Sources/libohiboard/clock.o \
./Sources/libohiboard/i2c_KL25Z4.o \
./Sources/libohiboard/utility.o \


# Each subdirectory must supply rules for building sources it contributes
Sources/libohiboard/clock.o: ../Sources/libohiboard/clock.c
	@echo 'Building file: $<'
	@echo 'Executing target #4 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/libohiboard/clock.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/libohiboard/clock.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/libohiboard/i2c_KL25Z4.o: ../Sources/libohiboard/i2c_KL25Z4.c
	@echo 'Building file: $<'
	@echo 'Executing target #5 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/libohiboard/i2c_KL25Z4.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/libohiboard/i2c_KL25Z4.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/libohiboard/utility.o: ../Sources/libohiboard/utility.c
	@echo 'Building file: $<'
	@echo 'Executing target #6 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/libohiboard/utility.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/libohiboard/utility.o"
	@echo 'Finished building: $<'
	@echo ' '


