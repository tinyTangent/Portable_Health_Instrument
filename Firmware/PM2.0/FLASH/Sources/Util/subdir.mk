################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Util/DateTime.c" \
"../Sources/Util/Storage.c" \

C_SRCS += \
../Sources/Util/DateTime.c \
../Sources/Util/Storage.c \

OBJS += \
./Sources/Util/DateTime.o \
./Sources/Util/Storage.o \

C_DEPS += \
./Sources/Util/DateTime.d \
./Sources/Util/Storage.d \

OBJS_QUOTED += \
"./Sources/Util/DateTime.o" \
"./Sources/Util/Storage.o" \

C_DEPS_QUOTED += \
"./Sources/Util/DateTime.d" \
"./Sources/Util/Storage.d" \

OBJS_OS_FORMAT += \
./Sources/Util/DateTime.o \
./Sources/Util/Storage.o \


# Each subdirectory must supply rules for building sources it contributes
Sources/Util/DateTime.o: ../Sources/Util/DateTime.c
	@echo 'Building file: $<'
	@echo 'Executing target #7 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Util/DateTime.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Util/DateTime.o"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Util/Storage.o: ../Sources/Util/Storage.c
	@echo 'Building file: $<'
	@echo 'Executing target #8 $<'
	@echo 'Invoking: ARM Ltd Windows GCC C Compiler'
	"$(ARMSourceryDirEnv)/arm-none-eabi-gcc" "$<" @"Sources/Util/Storage.args" -MMD -MP -MF"$(@:%.o=%.d)" -o"Sources/Util/Storage.o"
	@echo 'Finished building: $<'
	@echo ' '


