#include "Storage.h"
#include "Timer.h"

#define MAX_STORAGE_HOURS 48

static int currentHealthDataCursor = 0;
static HealthData healthData[100];

static uint16_t sportData[MAX_STORAGE_HOURS] = {0};
static uint16_t pollutionData[MAX_STORAGE_HOURS] = {0};
static int recordBeginHour = 0;

uint32_t getRecordDuration()
{
	return Timer_getSecSinceBoot() - recordBeginHour * 3600;
}

uint16_t* getPollutionData()
{
	return pollutionData;
}

uint16_t* getSportData()
{
	return sportData;
}

uint16_t getAvailableDataCount()
{
	int ret = MAX_STORAGE_HOURS - 1;
	while(sportData[ret] == 0 && pollutionData[ret] == 0 && ret >= 0)
		ret--;
	ret++;
	return ret;
}

void Storage_init() {
	for(int i = 0; i < 100; i++) {
		healthData[i].dateTime.year = 0;
	}
}

HealthData* getSportHistoryAt(int pos) {
	pos = (currentHealthDataCursor - 1 - pos) % 100;
	return pos;
}

void recordSport() {
	healthData[currentHealthDataCursor].dateTime = *(DateTime_getCurrent());
	getPollutionPM25AndPM10(
		&(healthData[currentHealthDataCursor].pm25Value),
		&(healthData[currentHealthDataCursor].pm10Value)
	);
	currentHealthDataCursor++;
	////////////////////////////////////////////////
	uint32_t time = Timer_getSecSinceBoot();
	int storageUnit = time / 3600;
	if (storageUnit - recordBeginHour < MAX_STORAGE_HOURS)
	{
		sportData[storageUnit - recordBeginHour]++;
	}
	else
	{
		int newRecordBeginHour = storageUnit - MAX_STORAGE_HOURS + 1;
		for (int i = newRecordBeginHour - recordBeginHour; i< MAX_STORAGE_HOURS; i++)
		{
			sportData[i - (newRecordBeginHour - recordBeginHour)] = sportData[i];
		}
		recordBeginHour = newRecordBeginHour;
		sportData[storageUnit - recordBeginHour]++;
	}
	recordPollution(rand()%100 + 100);
}

void recordPollution(int val)
{
	uint32_t time = Timer_getSecSinceBoot();
	int storageUnit = time / 3600;
	if (storageUnit - recordBeginHour < MAX_STORAGE_HOURS)
	{
		pollutionData[storageUnit - recordBeginHour] = val;
	}
	else
	{
		int newRecordBeginHour = storageUnit - MAX_STORAGE_HOURS + 1;
		for (int i = newRecordBeginHour - recordBeginHour; i< MAX_STORAGE_HOURS; i++)
		{
			pollutionData[i - (newRecordBeginHour - recordBeginHour)] = sportData[i];
		}
		recordBeginHour = newRecordBeginHour;
		pollutionData[storageUnit - recordBeginHour] = val;
	}
}
