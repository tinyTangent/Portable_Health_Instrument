/*
 * DateTime.c
 *
 *  Created on: Apr 9, 2016
 *      Author: tansinan
 */

#include <DateTime.h>

static DateTime currentDateTime;

DateTime* DateTime_getCurrent() {
	return &currentDateTime;
}

void DateTime_tick() {
	currentDateTime.msec++;
	if(currentDateTime.msec == 10) {
		currentDateTime.msec = 0;
		currentDateTime.second++;
	}
	else return;
	if(currentDateTime.second == 60) {
		currentDateTime.second = 0;
		currentDateTime.minute++;
	}
	else return;
	if(currentDateTime.minute == 60) {
		currentDateTime.minute = 0;
		currentDateTime.hour++;
	}
	else return;
	if(currentDateTime.hour == 60) {
		currentDateTime.hour = 0;
		currentDateTime.day++;
	}
	//TODO�� day/month/year progress.
}
