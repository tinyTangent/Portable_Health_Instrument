/*
 * AppStepSensor.c
 *
 *  Created on: May 30, 2015
 *      Author: leo
 */

#include "Includes.h"

App AppStepSensor_app;

int step=0;


void AppStepSensor_drawHandler()
{
	unsigned char Str[3];
	OLEDFB_clear();
	OLEDFB_drawText(1,0,"hELLO");
	OLEDFB_drawText(1,1,"Step:");
	Str[0]=(step/10)|0x30;//ʮλ
	Str[1]=(step%10)|0x30;//��λ
	Str[2]=(step%100)|0x30;
	OLEDFB_drawText(108,50,Str);
	OLEDFB_flush();
}


void AppStepSensor_onKeyPressed(char key)
{
	if(key == KEY_LEFT)
	{
		//App_switchTo(OLED_MENU_getApp());		
	}
}
void AppStepSensor_init()
{
	App_create(&AppStepSensor_app, AppStepSensor_drawHandler, AppStepSensor_onKeyPressed);
}

App* AppStepSensor_getApp()
{
	return &AppStepSensor_app;
}
