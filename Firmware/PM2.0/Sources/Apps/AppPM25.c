/*
 * AppPiano.c
 *
 *  Created on: Jun 1, 2015
 *      Author: leo
 */

#include "Includes.h"
#include "DateTime.h"

App AppPM25_app;


const int LED_R = 1;
const int LED_G = 2;
const int LED_B = 4;

static int pm25 = 0;
static int pm10 = 0;
static char buffer[10];
static int currentMode = 0;
static int ledOn = 1;
static int screenOn = 1;

static void drawTopBar()
{
	DateTime* dateTime = DateTime_getCurrent();
	//Draw left border
	OLEDFB_drawText(0, 0, currentMode==0?"Env":"Run");
	
	//Draw time
	sprintf(buffer ,"%02d:%02d", dateTime->minute, dateTime->second);
	
	//Draw date
	OLEDFB_drawText(44, 0, buffer);
	OLEDFB_drawLine(100, 0, 120, 0);
	OLEDFB_drawLine(100, 6, 120, 6);
	OLEDFB_drawLine(100, 0, 100, 6);
	OLEDFB_drawLine(120, 0, 120, 1);
	OLEDFB_drawLine(120, 5, 120, 6);
	OLEDFB_drawLine(120, 1, 123, 1);
	OLEDFB_drawLine(120, 5, 123, 5);
	OLEDFB_drawLine(123, 1, 123, 5);
	OLEDFB_drawRect(100, 0, 111, 6, OLEDFB_WHITE);
}

void AppPM25_drawSportData()
{
	OLEDFB_clear();
	drawTopBar();
	
	//Draw lines
	OLEDFB_drawLine(0, 9, 127, 9);
	OLEDFB_drawLine(64, 9, 64, 63);
	
	//Draw ranking:
	OLEDFB_drawText(0, 14, "RANK");
	buffer[0] = 'A' + (pm25 + pm10) / 100;
	buffer[1] = '\0';
	OLEDFB_drawTextEx(64 - 32, 16, 26, 26, buffer);
	sprintf(buffer, "pm25:%d", pm25);
	OLEDFB_drawText(0, 48, buffer);
	sprintf(buffer, "pm10:%d", pm10);
	OLEDFB_drawText(0, 56, buffer);
	
	//Draw step & target:
	sprintf(buffer, "%04d", step);
	OLEDFB_drawTextEx(65, 12, 12, 12, buffer);
	
	//TODO int target = 10000
	sprintf(buffer, "/%d", 10000);
	OLEDFB_drawText(80, 26, buffer);
	sprintf(buffer, "FIN:%2d%", step * 100 / 10000);
	OLEDFB_drawText(65, 36, buffer);
	sprintf(buffer, "Cal:");
	OLEDFB_drawText(65, 46, buffer);
	sprintf(buffer, "  %05d", step/22);
	OLEDFB_drawText(65, 56, buffer);
	
	
	OLEDFB_flush();
}

void AppPM25_drawHandler()
{
	getPollutionPM25AndPM10(&pm25, &pm10);
	
	int pollutionLevel = (pm10 + pm25) / 100;
	GPIOE_PDOR |= 0xFFF;
	if(ledOn)
	{
		if(pollutionLevel == 0) GPIOE_PDOR &= ~LED_G;
		else if(pollutionLevel == 1) GPIOE_PDOR &= ~LED_B;
		else if(pollutionLevel == 2) GPIOE_PDOR &= (~LED_R) & (~LED_G);
		else GPIOE_PDOR &= ~LED_R;
	}
	if(!screenOn)
	{
		OLEDFB_clear();
		OLEDFB_flush();
		return;
	}
	
	if(currentMode != 0)
	{
		AppPM25_drawSportData();
		return;
	}
	OLEDFB_clear();
	drawTopBar();
	
	//Draw lines
	OLEDFB_drawLine(0, 9, 127, 9);
	OLEDFB_drawLine(64, 9, 64, 63);
	
	//Draw pm25 and pm10 tags:
	OLEDFB_drawTextEx(0, 10, 8, 8, "PM2.5:");
	OLEDFB_drawTextEx(0, 38, 8, 8, "PM10:");
	
	//Draw pm25 and pm10 value.
	//TODO: pm25 Value >= 1000?
	sprintf(buffer, "%d", pm25);
	OLEDFB_drawTextEx(10 + 12 * (3 - strlen(buffer)), 22, 12, 12, buffer);
	sprintf(buffer, "%d", pm10);
	OLEDFB_drawTextEx(10 + 12 * (3 - strlen(buffer)), 50, 12, 12, buffer);
	
	//Draw temperature and humidity
	sprintf(buffer, "TEM:%dC", 16);
	OLEDFB_drawText(65, 11, buffer);
	sprintf(buffer, "HUM:%d%", 30);
	OLEDFB_drawText(65, 22, buffer);
	
	//Draw ranking:
	OLEDFB_drawText(65, 33, "RANK");
	buffer[0] = 'A' + (pm25 + pm10) / 100;
	buffer[1] = '\0';
	OLEDFB_drawTextEx(128 - 32 + 3, 64 - 32 + 3, 26, 26, buffer);
	
	OLEDFB_flush();
}

void AppPM25_onKeyPressed(char key)
{
	if(key == KEY_UP)
	{
		ledOn = !ledOn;
	}
	if(key == KEY_DOWN)
	{
		screenOn = !screenOn;
	}
	else
	{
		currentMode++;
		if(currentMode > 1)
			currentMode = 0;
	}
}

void AppPM25_init()
{
	App_create(&AppPM25_app, AppPM25_drawHandler, AppPM25_onKeyPressed);
}

App* AppPM25_getApp()
{
	return &AppPM25_app;
}
