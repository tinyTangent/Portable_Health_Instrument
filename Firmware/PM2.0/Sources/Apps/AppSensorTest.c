/*
 * AppSensorTest.c
 *
 *  Created on: May 23, 2015
 *      Author: tansinan
 */

#ifndef APPSENSORTEST_H_
#define APPSENSORTEST_H_

#include "Includes.h"

App AppSensorTest_app;

void AppSensorTest_drawHandler()
{
	static int counter  = 0;
	
	static unsigned int _ADC_LightIntensity=0;
	static unsigned int _ADC_Temperature=0;
	static unsigned int _ADC_potentiometer=0;
	
	ADC_channel_potentiometer();
	_ADC_potentiometer=ADC0_TR_DATA();
	
	ADC_channel_temperature();
	_ADC_Temperature=ADC0_TR_DATA(); //ADC之间会互相干扰
	
	ADC_channel_photosensor();
	_ADC_LightIntensity=ADC0_TR_DATA();
	OLEDFB_clear();
	/*
		counter++;
		if(counter>=10) {
			counter=0;
			Demo_accMeasure();
		}
	
		sprintf(str,"   X:     ");
		if (acc_x<0) { 
			str[6]='-';
			acc_x=-acc_x;
		} else {
			str[6]=' '; 
		}
					
		str[9]=acc_x%10+'0';
		str[8]=(acc_x/10)%10+'0';
		OLED_W_Str(2,1,str);
		
		sprintf(str,"   Y:     ");
		if (acc_y<0) { 
			str[6]='-';
			acc_y=-acc_y;
		} else {
			str[6]=' '; 
		}
					
		str[9]=acc_y%10+'0';
		str[8]=(acc_y/10)%10+'0';
		OLED_W_Str(4,1,str);
		
		sprintf(str,"   Z:     ");
		if (acc_z<0) { 
			str[6]='-';
			acc_z=-acc_z;
		} else {
			str[6]=' '; 
		}
					
		str[9]=acc_z%10+'0';
		str[8]=(acc_z/10)%10+'0';
		OLED_W_Str(6,1,str);
		*/
	    sprintf(str,"Poten:    m ");
		str[11]=_ADC_potentiometer%10+'0';
		str[10]=(_ADC_potentiometer/10)%10+'0';
		str[9]=(_ADC_potentiometer/100)%10+'0';
		str[8]=(_ADC_potentiometer/1000)%10+'0';
		str[7]=(_ADC_potentiometer/10000)%10+'0';
		OLEDFB_drawText(1,15,str);
	
		OLEDFB_flush();

	
}

void AppSensorTest_onKeyPressed(char key)
{
	if(key == KEY_LEFT)
	{
		//App_switchTo(OLED_MENU_getApp());		
	}
}
void AppSensorTest_init()
{
	App_create(&AppSensorTest_app, AppSensorTest_drawHandler, AppSensorTest_onKeyPressed);
}

App* AppSensorTest_getApp()
{
	return &AppSensorTest_app;
}

#endif /* APPSENSORTTEST_H_ */
