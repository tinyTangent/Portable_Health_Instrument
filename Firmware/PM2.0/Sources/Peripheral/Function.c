/*
 * Function.c
 *
 *  Created on: May 6, 2015
 *      Author: leo
 */

#include "Includes.h"


int power=1;
char str[20];

unsigned char lED1_light_flag;
unsigned char lED2_light_flag;
unsigned char lED3_light_flag;
unsigned char lED4_light_flag;

int handleChangeFlag=0;

int buzzFlag=0;

/*enable interrupt*/
void enable_irq(int irq) {

    //确定irq号为有效的irq号
    if (irq > 32)	irq=32;
    
    NVIC_ICPR |= (1<<(irq%32));
    NVIC_ISER |= (1<<(irq%32));   

}

/*enable initial*/
void EN_Init() {
	enable_irq(13);  //UART0 interrupt
	enable_irq(22);  //PIT interrupt
}


void ButtonBuzz() {
	static int count=0;
	static int delay_n=100;
	
	count++;
	if(delay_n<=0) {
		delay_n=100;
		buzzFlag=0;
	}
	
	if( (count>=0) && (delay_n >0) && (buzzFlag==1) ) {
		GPIOB_PTOR |=BIT18; 
		count=0;
		delay_n--;
	}
}

//give some delay~~
void delay()
{
   unsigned short i,j;
   for(i=0;i<3000;i++)
	{
   		for(j=0;j<100;j++)
      		asm("nop");
	}
}

/*delay function*/
void delayms(unsigned int number) {
  unsigned short i,j;
  for(i=0;i<number;i++)
  {
	  for(j=0;j<100;j++)
		  asm("nop");
  }
}

//按键
/*KEY initial*/
void KEY_Init()
{
	/*enable PORTB,PORTC,PORTE  clock*/
	SIM_SCGC5|=SIM_SCGC5_PORTB_MASK+SIM_SCGC5_PORTE_MASK+SIM_SCGC5_PORTD_MASK+SIM_SCGC5_PORTA_MASK;
	
	/*portB  set to GPIO,raising edge interrupt,no pull enable,passive filter enable*/	
	PORTB_PCR8=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
	PORTB_PCR9=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;			
	PORTB_PCR16=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
	PORTA_PCR17=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
	
	
	//for ButtonBuzz
	PORTB_PCR18=PORT_PCR_MUX(0X1);
	GPIOB_PDDR|=BIT18;
	
	/*portE  set to GPIO,raising edge interrupt,no pull enable,passive filter enable*/
	PORTE_PCR2=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
	PORTE_PCR3=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
	PORTE_PCR4=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
	PORTE_PCR5=PORT_PCR_MUX(0X1)+PORT_PCR_IRQC(0X0A)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;
	
	//for step sensor _ query 
	PORTD_PCR4=PORT_PCR_MUX(0X1)+PORT_PCR_PE_MASK+PORT_PCR_PS_MASK;	
}

static char keyRecord_D=0;
static char keyRecord_upEdge_D=0;

void step_query() {
	if((GPIOD_PDIR&BIT4)!=BIT4) {
		if((keyRecord_D & BIT0)==0)
		{
			//uart0_putchar('a');
			step++;
		}
			keyRecord_D |= BIT0;
			keyRecord_upEdge_D &=~BIT0;
		}
	else {
		keyRecord_D &= ~BIT0;
		if((keyRecord_upEdge_D & BIT0)==0) {
			//uart0_putchar('u');		
		}
		keyRecord_upEdge_D |=BIT0;
	}
}

static char keyRecord = 0;
static char keyRecord_upEdge=0;


void PORTB_Handler()
{
	static int Key_timer_up=0;
	static int Key_timer_down=0;
	static int Key_timer_right=0;
	static int Key_timer_left=0;
	
	App* currentApp = App_getCurrentApp();
	if(currentApp == NULL) return;
	//A KEY handler  all show 0
	if((GPIOB_PDIR&BIT8)!=BIT8)
	{
		if((keyRecord & BIT0)==0) {
			currentApp->onKeyEvent(KEY_RIGHT);
			buzzFlag=1;
		}
		keyRecord |= BIT0;
		Key_timer_right++;
		if(Key_timer_right>=60) {
			currentApp->onKeyEvent(KEY_RIGHT_LONG);
			Key_timer_right=0;
		}
	}
	else
	{
		keyRecord &= ~BIT0;
	}
	//B KEY handler  all show 1
	if((GPIOB_PDIR&BIT9)!=BIT9)
	{
		if((keyRecord & BIT1)==0) {
			currentApp->onKeyEvent(KEY_DOWN);
			buzzFlag=1;
		}
		keyRecord |= (BIT1);
		Key_timer_down++;
		if(Key_timer_down>=60) {
			currentApp->onKeyEvent(KEY_DOWN_LONG);
			Key_timer_down=0;
		}
	}
	else
	{
		keyRecord &= ~BIT1;
	}
	//C KEY handler  all show 2
	if((GPIOA_PDIR&BIT17)!=BIT17)
	{
		if((keyRecord & BIT2)==0) {
			currentApp->onKeyEvent(KEY_LEFT);
			buzzFlag=1;
		}
		keyRecord |= BIT2;
		Key_timer_left++;
		if(Key_timer_left>=60) {
			currentApp->onKeyEvent(KEY_LEFT_LONG);
			Key_timer_left=0;
		}
		//LED_light(0x03);	
	}
	else
	{
		keyRecord &= ~BIT2;
	}
	//D KEY handler  all close
	if((GPIOB_PDIR&BIT16)!=BIT16)
	{
		if((keyRecord & BIT3)==0) {
			currentApp->onKeyEvent(KEY_UP);
			buzzFlag=1;
		}
		keyRecord |= BIT3;
		Key_timer_up++;
		if(Key_timer_up>=60) {
			currentApp->onKeyEvent(KEY_UP_LONG);
			Key_timer_up=0;
		}
	}
	else
	{
		keyRecord &= ~BIT3;
	}
}

void PORTE_Handler()
{
	static int Key_timer;
	App* currentApp = App_getCurrentApp();
	if(currentApp == NULL) return;
		//L KEY handle
		if((GPIOE_PDIR&BIT2)!=BIT2){
			if((keyRecord & BIT4)==0) {
				currentApp->onKeyEvent(KEY_A);
			}
			keyRecord |= BIT4;
			keyRecord_upEdge &=~BIT4;
		} else {
			keyRecord &= ~BIT4;
			if((keyRecord_upEdge & BIT4)==0) {
				
			}
			keyRecord_upEdge |=BIT4;
		}
		//DOWN KEY handler
		if((GPIOE_PDIR&BIT3)!=BIT3) {
			if((keyRecord & BIT5)==0) {
				
			}
			keyRecord |= BIT5;
			keyRecord_upEdge &=~BIT5;
		} else {
			keyRecord &= ~BIT5;
			if((keyRecord_upEdge & BIT5)==0) {
				
			}
			keyRecord_upEdge |=BIT5;
		}
			
		//Key handle	
		if((GPIOE_PDIR&BIT4)!=BIT4) {
			if((keyRecord & BIT6)==0) {
				
			}
			keyRecord |= BIT6;
			keyRecord_upEdge &=~BIT6;
		} else {
			keyRecord &= ~BIT6;
			if((keyRecord_upEdge & BIT6)==0) {
				
			}
			keyRecord_upEdge |=BIT6;
		}
		
		//key handle	
	    if((GPIOE_PDIR&BIT5)!=BIT5) {
	    	if((keyRecord & BIT7)==0) {
	    		currentApp->onKeyEvent(KEY_D);
	    	
	    	}
			Key_timer++;
			if(power) {
				if(Key_timer>=1000) {		
					Key_timer=0;
					power=0;
					OLEDFB_clear();
					OLEDFB_flush();
					OLED_Clr();
				}
			} else {
				if(Key_timer>=60000) {
					power=1;
					Key_timer=0;
				}
			}
			
	    	keyRecord |= BIT7;
	    	keyRecord_upEdge &=~BIT7;
	    } else {
			keyRecord &= ~BIT7;
			if((keyRecord_upEdge & BIT7)==0) {
				
			}
			keyRecord_upEdge |=BIT7;
		}
}


void PORTB_Handler_2()
{
	static int Key_timer_up=0;
	static int Key_timer_down=0;
	static int Key_timer_right=0;
	static int Key_timer_left=0;
	
	App* currentApp = App_getCurrentApp();
	if(currentApp == NULL) return;
	//A KEY handler  all show 0
	if((GPIOB_PDIR&BIT8)!=BIT8)
	{
		if((keyRecord & BIT0)==0) {
			currentApp->onKeyEvent(KEY_RIGHT);
		}
		keyRecord |= BIT0;
		Key_timer_right++;
		if(Key_timer_right>=60) {
			currentApp->onKeyEvent(KEY_RIGHT_LONG);
			Key_timer_right=0;
		}
	}
	else
	{
		keyRecord &= ~BIT0;
	}
	//B KEY handler  all show 1
	if((GPIOB_PDIR&BIT9)!=BIT9)
	{
		if((keyRecord & BIT1)==0) {
			currentApp->onKeyEvent(KEY_DOWN);
		}
		keyRecord |= (BIT1);
		Key_timer_down++;
		if(Key_timer_down>=60) {
			currentApp->onKeyEvent(KEY_DOWN_LONG);
			Key_timer_down=0;
		}
	}
	else
	{
		keyRecord &= ~BIT1;
	}
	//C KEY handler  all show 2
	if((GPIOB_PDIR&BIT17)!=BIT17)
	{
		if((keyRecord & BIT2)==0) {
			currentApp->onKeyEvent(KEY_LEFT);
		}
		keyRecord |= BIT2;
		Key_timer_left++;
		if(Key_timer_left>=60) {
			currentApp->onKeyEvent(KEY_LEFT_LONG);
			Key_timer_left=0;
		}
		//LED_light(0x03);	
	}
	else
	{
		keyRecord &= ~BIT2;
	}
	//D KEY handler  all close
	if((GPIOB_PDIR&BIT16)!=BIT16)
	{
		if((keyRecord & BIT3)==0) {
			currentApp->onKeyEvent(KEY_UP);
		}
		keyRecord |= BIT3;
		Key_timer_up++;
		if(Key_timer_up>=60) {
			currentApp->onKeyEvent(KEY_UP_LONG);
			Key_timer_up=0;
		}
	}
	else
	{
		keyRecord &= ~BIT3;
	}
}

void PORTE_Handler_2()
{
	static int Key_tmer=0;
	//L KEY handler
	if((GPIOE_PDIR&BIT2)!=BIT2)   
	{
		//do someting
	}
	//DOWN KEY handler
	else if((GPIOE_PDIR&BIT3)!=BIT3)
	{
		//do something 
	}
	else if((GPIOE_PDIR&BIT4)!=BIT4)
	{
		//do something
	}
	else if((GPIOE_PDIR&BIT5)!=BIT5)
	{
		//do something
		//LED_light(0x04);
	}
}


#define FUNC 1
#define UP 2
#define DOWN 3
#define LEFT 4
#define RIGHT 5

/*LED inital*/
void LED_Init()
{
	/*enable PORTB and PORTD  clock*/
	SIM_SCGC5|=SIM_SCGC5_PORTC_MASK+SIM_SCGC5_PORTE_MASK;
	
	PORTE_PCR0=PORT_PCR_MUX(0X1);	
	PORTE_PCR1=PORT_PCR_MUX(0X1);
	PORTE_PCR2=PORT_PCR_MUX(0X1);	
	/*portC0---11 was set to GPIO*/	
//	PORTC_PCR0=PORT_PCR_MUX(0X1);	
//	PORTC_PCR1=PORT_PCR_MUX(0X1);
//	PORTC_PCR2=PORT_PCR_MUX(0X1);	
//	PORTC_PCR3=PORT_PCR_MUX(0X1);
//	PORTC_PCR4=PORT_PCR_MUX(0X1);	
//	PORTC_PCR5=PORT_PCR_MUX(0X1);
//	PORTC_PCR6=PORT_PCR_MUX(0X1);	
//	PORTC_PCR7=PORT_PCR_MUX(0X1);	
//	PORTC_PCR8=PORT_PCR_MUX(0X1);	
//	PORTC_PCR9=PORT_PCR_MUX(0X1);
//	PORTC_PCR10=PORT_PCR_MUX(0X1);	
//	PORTC_PCR11=PORT_PCR_MUX(0X1);
	    
	/*port set to out*/
	GPIOE_PDDR|=0XFFF;
	/*port out set to high*/
	GPIOE_PDOR=0XFFF;
	
}

//input parameter number:0x01--LED1;0x02----LED2;0x03----LED3;0x04---LED4
void LED_light(unsigned char number)
{
	switch(number)
	{
	     case 1:
	    		/*port out set to high*/
	    		GPIOE_PDOR=0XFFF;      //all dim
	    	    GPIOE_PDOR&=~(BIT0|BIT1|BIT2);   //GPIOC0--1--2  light
	    	    break;
	     case 2:
	    		/*port out set to high*/
	    		GPIOE_PDOR=0XFFF;      //all dim
	    	    GPIOE_PDOR&=~(BIT3|BIT4|BIT5);   //GPIOC0--3--5  light
	    	    break;
	     case 3:
	    		/*port out set to high*/
	    		GPIOE_PDOR=0XFFF;      //all dim
	    	    GPIOE_PDOR&=~(BIT6|BIT7|BIT8);   //GPIOC0--6--8  light
	    	    break;
	     case 4:
	    		/*port out set to high*/
	    		GPIOE_PDOR=0XFFF;      //all dim
	    	    GPIOE_PDOR&=~(BIT9|BIT10|BIT11);  //GPIOC0--9--11  light
	    	    break;
	     default: 
	    	    GPIOE_PDOR&=~0xFFF;              //GPIOC0--1--2  light
	    	    break;
	}
}




void LED_color (int color,int Led) {
	if(Led==0) {
		
	} else if(Led==1) {
		switch (color) {
			case BLACK:
				GPIOC_PDOR|=(BIT0+BIT1+BIT2); 
				break;	
			case WHITE:       
				GPIOC_PDOR&=~(BIT0|BIT1|BIT2);break; 
			case RED: 
				  GPIOC_PDOR|=(BIT0+BIT1+BIT2);      
				  GPIOC_PDOR&=~(BIT0); 
				break;
			case GREEN: 
				GPIOC_PDOR|=(BIT0+BIT1+BIT2);     
				GPIOC_PDOR&=~(BIT1);
				break;
			case BLUE: 
				GPIOC_PDOR|=(BIT0+BIT1+BIT2);     
				GPIOC_PDOR&=~(BIT2);
				break;
		}
	} else if(Led==2) {
		switch (color) {
			case BLACK:
				GPIOC_PDOR|=(BIT3+BIT4+BIT5); 
				break;	
			case WHITE: 
				GPIOC_PDOR&=~(BIT3|BIT4|BIT5);
				break; 
			case RED:
				GPIOC_PDOR|=(BIT3+BIT4+BIT5);
				GPIOC_PDOR&=~BIT3;
				break;
			case GREEN: 
				GPIOC_PDOR|=(BIT3+BIT4+BIT5);
				GPIOC_PDOR&=~BIT4;
				break;
			case BLUE:
				GPIOC_PDOR|=(BIT3+BIT4+BIT5);
				GPIOC_PDOR&=~BIT5;
				break;
		}
	} else if(Led==3) {
		switch (color) {
			case BLACK:
				GPIOC_PDOR|=(BIT6+BIT7+BIT8); 
				break;	
			case WHITE: 
				GPIOC_PDOR&=~(BIT6|BIT7|BIT8);
				break; 
			case RED: 
				GPIOC_PDOR|=(BIT6+BIT7+BIT8);
				GPIOC_PDOR&=~BIT6;
				break;
			case GREEN:
				GPIOC_PDOR|=(BIT6+BIT7+BIT8);
				GPIOC_PDOR&=~BIT7;
				break;
			case BLUE: 
				GPIOC_PDOR|=(BIT6+BIT7+BIT8);
				GPIOC_PDOR&=~BIT8;
				break;
		}
	} else if(Led==4) {
		switch (color) {
			case BLACK:
				GPIOC_PDOR|=(BIT9+BIT10+BIT11); 
				break;	
			case WHITE: 
				GPIOC_PDOR&=~(BIT9|BIT10|BIT11);
				break; 
			case RED:
				GPIOC_PDOR|=(BIT9+BIT10+BIT11);
				GPIOC_PDOR&=~BIT9;
				break;
			case GREEN:
				GPIOC_PDOR|=(BIT9+BIT10+BIT11);
				GPIOC_PDOR&=~BIT10;
				break;
			case BLUE:
				GPIOC_PDOR|=(BIT9+BIT10+BIT11);
				GPIOC_PDOR&=~BIT11;
				break;
		}
	}
		
}
