/*
 * UART.c
 *
 *  Created on: May 7, 2015
 *      Author: leo
 */
#include "Includes.h"
void set_irq_priority (int irq, int prio)
{   
    /*irq priority pointer*/
    uint8_t *prio_reg;
    uint8_t err = 0;
    uint8_t div = 0;
    
    /* Make sure that the IRQ is an allowable number. Right now up to 32 is 
     * used.
     *
     * NOTE: If you are using the interrupt definitions from the header
     * file, you MUST SUBTRACT 16!!!
     */
    if (irq > 32)
    {
        //printf("\nERR! Invalid IRQ value passed to priority irq function!\n");
        err = 1;
    }

    if (prio > 3)
    {
        //printf("\nERR! Invalid priority value passed to priority irq function!\n");
        err = 1;
    }
    
    if (err != 1)
    {
        /* Determine which of the NVICIPx corresponds to the irq */
        div = irq / 4;
        prio_reg = (uint8_t *)((uint32_t)&NVIC_IP(div));
        /* Assign priority to IRQ */
        *prio_reg = ( (prio&0x3) << (8 - 2) );             
    }
}
void UART0_init()
{
	SIM_SOPT2 |= SIM_SOPT2_UART0SRC(1);
	SIM_SOPT2 &= ~SIM_SOPT2_PLLFLLSEL_MASK;
	SIM_SCGC5 |= 0x0200;
	SIM_SCGC4 |= 0x0400;
	PORTA_PCR1 = PORT_PCR_ISF_MASK|PORT_PCR_MUX(0x2); //0x200
	PORTA_PCR2 = PORT_PCR_ISF_MASK|PORT_PCR_MUX(0x2);
	UART0_C2 &= ~ (UART0_C2_TE_MASK| UART0_C2_RE_MASK);
	NVIC_ISER |= 1 << 12;
	set_irq_priority(12,3);
	
	UART0_BDH = 0x00;
	UART0_BDL = 0x8F;
	////////
	UART0_C1 = 0x00;
	UART0_C3 = 0x00;
	UART0_MA1 = 0x00;
	UART0_MA1 = 0x00;
	UART0_S1 |= 0x1F;
	UART0_S2 |= 0xC0;
	//UART0_C2 |= UART0_C2_TE_MASK|UART0_C2_RE_MASK;
	UART0_C2 |= UART0_C2_RIE_MASK|UART0_C2_TE_MASK|UART0_C2_RE_MASK;
}

bool UART_isReadyRead()
{
	return UART0_S1 & 0x20;
}

bool UART_isReadyWrite()
{
	return UART0_S1 & UART_S1_TDRE_MASK;
}

char UART_directRead()
{
	return UART0_D;
}

void UART_directWrite(char data)
{
	UART0_D = data;
}

void UART_putChar(unsigned char data)
{
	while(!UART_isReadyWrite());
	UART0_D = data;
}

void UART_putString(unsigned char* str)
{
	for(int i=0;;i++)
	{
		if(str[i]!='\0')
		{
			UART_putChar(str[i]);
		}
		else return;
	}
}

unsigned char UART_getChar()
{
	while(!UART_isReadyRead());
	return UART0_D;
}



void UART1_Init (void)
{
	/* Calculate baud settings */
	unsigned short uartclk_khz = 10550;
	unsigned short baud = 9600;
	unsigned short sbr;
	
	SIM_SCGC4 |= SIM_SCGC4_UART1_MASK;// 使能UART1的时钟
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;// 使能PORTC的时钟
	PORTC_PCR3 |= PORT_PCR_MUX(0x3); //配置端口PTc3为UART1复用端口
	PORTC_PCR4 |= PORT_PCR_MUX(0x3); //配置端口PTc4为UART1复用端口
	
	sbr = (unsigned short)((uartclk_khz *1000)/(baud * 16));
	UART1_BDH = (unsigned char)((sbr & 0x1F00) >> 8);
	UART1_BDL = (unsigned char)(sbr & 0x00FF);

	UART1_C2 =BIT2+BIT3+BIT5; //打开发送和接收器
}

unsigned char uart1_getchar (void)
{
	/* Wait until character has been received */
	while (!(UART1_S1 & UART_S1_RDRF_MASK));
	/* Return the 8-bit data from the receiver */
	return UART1_D;
}

void uart1_putchar (unsigned char data)
{
	/* Wait until space is available in the FIFO */
	while(!(UART1_S1 & UART_S1_TDRE_MASK));
	/* Send the character */
	UART1_D = data;
}


/*单片机向上位机发送字符串*/
void urat1__TransmitArray (char *data)
{
	int index = 0;
	while (data[index]!='\0')    //将字符串(即字符数组)拆分成单个字符,逐一发送
        {
		   uart1_putchar (data[index]);
           index++;
        }
}

/*将int型转化为字符串输出*/
void urat1_TransmitNumeral (int numeral) 
{
  int reversed = 0, length = 0;char _cflag;
  if (numeral < 0) { urat1__TransmitArray("-"); numeral = -numeral; }   //先对符号进行判断,如有符号,输出"-"
  while (numeral > 0) {
    reversed = reversed * 10 + (numeral % 10); 
    numeral /= 10; length++; }          //对数据的长度进行测量
  while (length > 0) {                  //根据测量的结果,逐一发出数据
    _cflag='0'+reversed % 10;
    uart1_putchar(_cflag);
    reversed /= 10; length--;
  }
}
