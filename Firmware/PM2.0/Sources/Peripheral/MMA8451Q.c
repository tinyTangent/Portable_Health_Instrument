/*
 * MMA8451Q.c
 *
 *  Created on: May 7, 2015
 *      Author: leo
 */



#include "Includes.h"



unsigned char MMA8451_data[6];
unsigned char MasterTransmission;
unsigned char SlaveID;


int acc_x=0;
int acc_y=0;
int acc_z=0;

//just for test
int acc_0=0;
int _acc=0;


/*******************************************************************/
/*!
 * I2C Initialization
 * Set Baud Rate and turn on I2C1
 */
void MMA8451_init_I2C(void)
{
    SIM_SCGC4 |= SIM_SCGC4_I2C1_MASK; //Turn on clock to I2C1 module
    SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;//Turn on clock to PORTC module
    
    
    /* configure GPIO for I2C1 function */
    PORTC_PCR10 = PORT_PCR_MUX(2);
    PORTC_PCR11 = PORT_PCR_MUX(2);
    I2C1_F  = 0x14;       /* set MULT and ICR */
    I2C1_C1 = I2C_C1_IICEN_MASK;       /* enable IIC */
}

/*******************************************************************/
/*!
 * Start I2C Transmision
 * @param SlaveID is the 7 bit Slave Address
 * @param Mode sets Read or Write Mode
 */

void MMA8451_IIC_StartTransmission (unsigned char SlaveID, unsigned char Mode)
{
  if(Mode == MWSR)
  {
    /* set transmission mode */
    MasterTransmission = MWSR;
  }
  else
  {
    /* set transmission mode */
    MasterTransmission = MRSW;
  }

  /* shift ID in right possition */
  SlaveID = (unsigned char) MMA8451_I2C_ADDRESS << 1;

  /* Set R/W bit at end of Slave Address */
  SlaveID |= (unsigned char)MasterTransmission;

  /* send start signal */
  MMA8451_i2c_Start();

  /* send ID with W/R bit */
  MMA8451_i2c_write_byte(SlaveID);
}

/*******************************************************************/
/*!
 * Pause Routine
 */
void Pause(void){
    int n;
    for(n=1;n<50;n++) {
      asm("nop");
    }
}

/*******************************************************************/
/*!
 * Read a register from the MPR084
 * @param u8RegisterAddress is Register Address
 * @return Data stored in Register
 */
unsigned char MMA8451_ReadRegister(unsigned char u8RegisterAddress)
{
  unsigned char result;
  unsigned int j;

  /* Send Slave Address */
  MMA8451_IIC_StartTransmission(SlaveID,MWSR);
  MMA8451_i2c_Wait();

  /* Write Register Address */
  I2C1_D = u8RegisterAddress;
  MMA8451_i2c_Wait();

  /* Do a repeated start */
  I2C1_C1 |= I2C_C1_RSTA_MASK;

  /* Send Slave Address */
  I2C1_D = (MMA8451_I2C_ADDRESS << 1) | 0x01; //read address
  MMA8451_i2c_Wait();

  /* Put in Rx Mode */
  I2C1_C1 &= (~I2C_C1_TX_MASK);

  /* Turn off ACK */
  I2C1_C1 |= I2C_C1_TXAK_MASK;

  /* Dummy read */
  result = I2C1_D ;
  for (j=0; j<5000; j++){};
  MMA8451_i2c_Wait();

  /* Send stop */
  MMA8451_i2c_Stop();
  result = I2C1_D ;
  Pause();
  return result;
}

/*******************************************************************/
/*!
 * Write a byte of Data to specified register on MPR084
 * @param u8RegisterAddress is Register Address
 * @param u8Data is Data to write
 */
void MMA8451_WriteRegister(unsigned char u8RegisterAddress, unsigned char u8Data)
{
  /* send data to slave */
  MMA8451_IIC_StartTransmission(SlaveID,MWSR);
  MMA8451_i2c_Wait();

  I2C1_D = u8RegisterAddress;
  MMA8451_i2c_Wait();

  I2C1_D = u8Data;
  MMA8451_i2c_Wait();

  MMA8451_i2c_Stop();

  Pause();
}

//MMA8451 initial
void MMA8451_Init()
{
	MMA8451_init_I2C();
	MMA8451_i2c_Start();
	//I2C1_C1 |= I2C_C1_RSTA_MASK;
	//MMA8451_i2c_Wait();
	//I2C1_C1 |= I2C_C1_TX_MASK;
	MMA8451_i2c_write_byte(0x80);
	MMA8451_i2c_Wait();
	
	MMA8451_i2c_write_byte(0xF5);
	MMA8451_i2c_Wait();


	//MMA8451_i2c_Wait();
	/*
	 * I2C1_C1 |= I2C_C1_RSTA_MASK;

  I2C1_D = (MMA8451_I2C_ADDRESS << 1) | 0x01; //read address
  MMA8451_i2c_Wait();

  I2C1_C1 &= (~I2C_C1_TX_MASK);
	 * */
	I2C1_C1 |= I2C_C1_RSTA_MASK;
	MMA8451_i2c_write_byte(0x81);
	MMA8451_i2c_Wait();
	I2C1_C1 &= (~I2C_C1_TX_MASK);
	
	char d1 = I2C1_D;
	//MMA8451_i2c_Wait();
	char d2 = I2C1_D;
	char c = 0;
	//MMA8451_i2c_Wait();
	c = 1;
	//MMA8451_ID=MMA8451_ReadRegister(0x0D);  //read device ID
	//MMA8451_WriteRegister(0x2A, 0x01);	
}

void Demo_accMeasure() {
		/*char _temp_x=0;
		char _temp_y=0;
		char _temp_z=0;
	    MMA8451_data[0]=MMA8451_ReadRegister(0x01);  //x
    	MMA8451_data[1]=MMA8451_ReadRegister(0x02);
		
    	MMA8451_data[2]=MMA8451_ReadRegister(0x03);  //y
    	MMA8451_data[3]=MMA8451_ReadRegister(0x04);
		
    	MMA8451_data[4]=MMA8451_ReadRegister(0x05);  //z
    	MMA8451_data[5]=MMA8451_ReadRegister(0x06);

    	if (MMA8451_data[0]&0x80) { 
			_temp_x=(~MMA8451_data[0])+1;	//负数处理
		} else {
			_temp_x=MMA8451_data[0];
		}		    
		 acc_x= dtoa(_temp_x%10)-'0'  +  ((int)dtoa((_temp_x/10)%10)-'0')*10;
		if (MMA8451_data[0]&0x80) {
			acc_x=-acc_x;
		}
		
		if (MMA8451_data[2]&0x80) {
		_temp_y=(~MMA8451_data[2])+1;	//负数处理
		} else {
			_temp_y=MMA8451_data[2];
		}
		acc_y= dtoa(_temp_y%10)-'0'  +  ((int)dtoa((_temp_y/10)%10)-'0')*10;
		if (MMA8451_data[2]&0x80) {
					acc_y=-acc_y;
		}  
		
		if (MMA8451_data[4]&0x80) { 
			_temp_z=(~MMA8451_data[4])+1;	//负数处理
		} else {
			_temp_z=MMA8451_data[4];
		}		    
		 acc_z= dtoa(_temp_z%10)-'0'  +  ((int)dtoa((_temp_z/10)%10)-'0')*10;
		if (MMA8451_data[4]&0x80) {
			acc_z=-acc_z;
		}*/
}
