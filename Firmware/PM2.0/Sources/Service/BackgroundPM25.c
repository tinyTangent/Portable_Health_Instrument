/*
 * BackgroundPM25.c
 *
 *  Created on: Apr 9, 2016
 *      Author: tansinan
 */

#include "Includes.h"
#include "UART.h"

extern unsigned char BufferData[30];
extern int pm25BufferUsed;

static int pm25Value = 0;
static int pm10Value = 0;

void UART1_IRQHandler(void) {
	//ADC_TR_DATA();
	//static int counter=0;
	if (UART1_S1 & UART_S1_RDRF_MASK) {
		BufferData[pm25BufferUsed] = UART1_D;
		pm25BufferUsed++;
		if (pm25BufferUsed == 30) {
			pm25BufferUsed = 0;
			for (int i = 0; i < 30; i++) {
				//uart0_putchar(BufferData[i]);
			}
		}
	}
}

void getPollutionPM25AndPM10(int* pm25, int* pm10) {
	for(int i = 0; i < pm25BufferUsed - 1; i++)
	{
		if(BufferData[i] == 0xAA && BufferData[i+1] == 0xC0)
		{
			if(i + 9 < pm25BufferUsed && BufferData[i + 9] == 0xAB)
			{
				//TODO: We swapped pm2.5 and pm10 value, may be the manual is wrong?
				pm10Value = (BufferData[i + 3] * 256 + BufferData[i + 2]) / 10;
				pm25Value = (BufferData[i + 5] * 256 + BufferData[i + 4]) / 10;
				break;
			}
		}
	}
	*pm25 = pm25Value;
	*pm10 = pm10Value;
}
