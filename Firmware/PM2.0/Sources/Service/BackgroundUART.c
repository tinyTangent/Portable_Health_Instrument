/*
* UART.c

*
*  Created on: Oct 21, 2015
*      Author: tansinan
*/
#include <string.h>
#include <types.h>
#include <Storage.h>
#include "UART.h"
#include "DateTime.h"

#define UART_READ_BUFFER_SIZE 64

static char inputBuffer[UART_READ_BUFFER_SIZE];
static uint16_t inputBufferUsed = 0;
static void handleIncomingData(char* command);

void UART0_IRQHandler(void)
{
	asm("CPSID i");
	if (inputBufferUsed < UART_READ_BUFFER_SIZE)
	{
		bool haveZero = false;
		for (int i = 0; i<20; i++)
		{
			while (UART_isReadyRead())
			{
				char data = UART_directRead();
				//TODO: This will finally required to be removed.
				//UART_putChar(data);
				inputBuffer[inputBufferUsed] = data;
				if (data == '\n'||data == '\r') haveZero = true;
				inputBufferUsed++;
			}
		}
		if (haveZero)
		{
			char command[20];
			UART_readString(command);
			handleIncomingData(command);
		}
		//else App_sendMessage(EVENT_UART_READY_READ, 0);
	}
	asm("CPSIE i");
}

void UART_readString(char* str)
{
	int stringEndPos = 0;
	while ((inputBuffer[stringEndPos] != '\n' && inputBuffer[stringEndPos] != '\r')
			&& stringEndPos < inputBufferUsed)
		stringEndPos++;
	for (int i = 0; i < stringEndPos; i++)
	{
		str[i] = inputBuffer[i];
	}
	str[stringEndPos] = '\0';
	for (int i = stringEndPos + 1; i < inputBufferUsed; i++)
	{
		inputBuffer[i - stringEndPos - 1] = inputBuffer[i];
	}
	inputBufferUsed -= stringEndPos + 1;
}

void handleIncomingData(char* command)
{
	if(strcmp(command, "help") == 0)
	{
		UART_putString("\r\nans_help help sync dump sport pollution get_time set_time sync\r\n");
	}
	else if(strcmp(command, "get_time") == 0)
	{
		UART_putString("\r\nget_time_ans ");
		char buf[20];
		sprintf(buf, "%d \r\n", Timer_getSecSinceBoot());
		UART_putString(buf);
	}
	else if(memcmp(command, "set_time ", 9) == 0)
	{
		command += 9;
		int year, month, day, hour, minute, second;
		sscanf(command, "%d %d %d %d %d %d",
				&year, &month, &day, &hour, &minute, &second);
		DateTime* dateTime = DateTime_getCurrent();
		dateTime->year = year;
		dateTime->month = month;
		dateTime->day = day;
		dateTime->hour = hour;
		dateTime->minute = minute;
		dateTime->second = second;
		UART_putString("ans_set_time \r\n");
	}
	else if(strcmp(command, "dump") == 0)
	{
		char buf[20];
		UART_putString("ans_dump ");
		sprintf(buf, "%d ", getRecordDuration());
		UART_putString(buf);
		int dataCount = getAvailableDataCount();
		sprintf(buf, "%d ", dataCount);
		UART_putString(buf);
		for(int i = 0; i < dataCount; i++)
		{
			sprintf(buf, "%d ", getSportData()[i]);
			UART_putString(buf);
			sprintf(buf, "%d ", getPollutionData()[i]);
			UART_putString(buf);
		}
		UART_putString("\r\n");
		//UART_putString("ans_dump_p");
	}
	else if(strcmp(command, "get_pollution") == 0)
	{
		char buf[20];
		UART_putString("ans_pollution ");
		int pm25, pm10;
		getPollutionPM25AndPM10(&pm25, &pm10);
		sprintf(buf, "%d %d\r\n", pm25, pm10);
		UART_putString(buf);
	}
	else if(memcmp(command, "sport ", 6) == 0)
	{
		command += 6;
		int count = atoi(command);
		char buf[20];
		UART_putString("\r\nans_sport ");
		sprintf(buf, "add %d\r\n", count);
		UART_putString(buf);
		for(int i = 0; i < count; i++)
			recordSport();
	}
	else if(memcmp(command, "pollution ", 10) == 0)
	{
		command += 10;
		int count = atoi(command);
		char buf[20];
		UART_putString("\r\nans_pollution ");
		sprintf(buf, "add %d\r\n", count);
		UART_putString(buf);
		recordPollution(count);
	}
	else if(strcmp(command, "sync") == 0)
	{
		UART_putString("\r\nDumping all current data and delete local data\r\n");
		for(int i = 0; i < 2;i++)
		{
			UART_putString("\r\nA \r\n");
		}
	}
}
