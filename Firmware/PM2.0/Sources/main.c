/*
 * main implementation: use this 'C' sample to create your own application
 *
 */

#include "Includes.h"
#include "sht21.h"
#include "libohiboard/i2c.h"

unsigned char BufferData[30] = { };
int pm25BufferUsed = 0;
unsigned char Input, ch1, ch2;
unsigned char buffer[10];
int bufferUsed = 0;
int pm25, pm10;
char checksum = 0;

void PM_Measure() {
	if (UART1_S1 & UART_S1_RDRF_MASK) {
		Input = UART1_D;
		//uart0_putchar(Input);
		ch1 = ch2;
		ch2 = Input;
		if (bufferUsed == 0) {
			if (ch1 == 0xAA && ch2 == 0xC0) {
				bufferUsed = 2;
				buffer[0] = 0xAA;
				buffer[1] = 0xC0;
			}
		} else {
			buffer[bufferUsed] = Input;
			bufferUsed++;
			if (bufferUsed == 10) {
				bufferUsed = 0;
				checksum = 0;
				if (buffer[9] == 0xAB) {
					pm25 = (buffer[3] * 256 + buffer[2]) / 10;
					pm10 = (buffer[5] * 256 + buffer[4]) / 10;
					urat0__TransmitArray("PM2.5=");
					urat0_TransmitNumeral(pm25);
					urat0__TransmitArray("\r\n");
					urat0__TransmitArray("PM10=");
					urat0_TransmitNumeral(pm10);
					urat0__TransmitArray("\r\n");
				} else {
					//urat0__TransmitArray("\r\n");
				}
			}
		}
	}
}

int measure_flag = 0;
unsigned char measure_data[4];
int measureTime = 0;

int main(void) {
	Timer_init(1000);
	asm("CPSIE i");
	PIT_init(1);  //1ms interval interrupt
	ADC_Init();
	EN_Init();
	UART0_init();
	UART1_Init();

	KEY_Init();
	LED_Init();
	App* currentApp;
	Storage_init();
	//确认开启全局中断

	//MMA8451_Init();  //initial MMA8451 g sensor
	OLED_Init();
	OLED_Clr();
	//Initialise the "Sensor Test" Application
	AppSensorTest_init();
	AppStepSensor_init();
	AppPM25_init();
	AppStepSensor_init();
	AppAccStep_init();
	App_switchTo(AppPM25_getApp());

	for (;;) {
		//TODO : Add key polling to PIT/SysTick Interupt.

		//用了handleChangeFlag来切换两个键盘模式
		if (!handleChangeFlag) {
			PORTB_Handler();
			PORTE_Handler();
		}
		//ALL FUNCTION IN ONE部分
		if (power) {
			currentApp = App_getCurrentApp();
			if (currentApp != NULL) {
				currentApp->onDraw();
			}
		}
	}
	return 0;
}
