/*
 * LMusicLibrary.h
 *
 *  Created on: Sep 19, 2015
 *      Author: tansinan
 */

#ifndef LMUSICLIBRARY_H_
#define LMUSICLIBRARY_H_

/**
 * Define Frequencies.
 */
#define PWM0_CLK_FREQ 50000*3.28*2

#define M1 261
#define M2 293
#define M3 329
#define M4 349
#define M5 391
#define M6 440
#define M7 493


//�Ͱ˶�
#define L1 M1/2 
#define L2 M2/2
#define L3 M3/2
#define L4 M4/2
#define L5 M5/2
#define L6 M6/2
#define L7 M7/2

/*#define M1  (unsigned short)(PWM0_CLK_FREQ/523)
#define M2  (unsigned short)(PWM0_CLK_FREQ/587)
#define M3  (unsigned short)(PWM0_CLK_FREQ/659)
#define M4  (unsigned short)(PWM0_CLK_FREQ/698)
#define M5  (unsigned short)(PWM0_CLK_FREQ/784)
#define M6  (unsigned short)(PWM0_CLK_FREQ/880)
#define M7  (unsigned short)(PWM0_CLK_FREQ/988)*/

//�߰˶�
#define H1 M1*2 
#define H2 M2*2
#define H3 M3*2
#define H4 M4*2
#define H5 M5*2
#define H6 M6*2
#define H7 M7*2

//��ֹ��
#define NN 0 

extern const unsigned short note[];
extern const unsigned short duration[];
extern const unsigned short note_1[];
extern const unsigned short duration_1[];
extern const unsigned short note_2[];
extern const unsigned short duration_2[];
extern const unsigned short note_3[];
extern const unsigned short duration_3[];

#endif /* LMUSICLIBRARY_H_ */
