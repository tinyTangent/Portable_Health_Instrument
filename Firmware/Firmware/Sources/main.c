/*
 * main implementation: use this 'C' sample to create your own application
 *
 */
#include "derivative.h" /* include peripheral declarations */
#include "Core/Timer.h"
#include "Peripheral/OLEDFB.h"
#include "Peripheral/OLEDFB3D.h"
#include "Peripheral/Button.h"
#include "Peripheral/Buzzer.h"
#include "Peripheral/ADC.h"
#include "Peripheral/UART.h"
#include "Peripheral/OneWire.h"
#include "Peripheral/DallasTemperature.h"
#include "Music.h"
#include "Resource/TMusicLibrary.h"

#include "Core/AppFramework.h"
#include "Core/ServiceFramework.h"
#include "App.h"

extern const Service ServiceStepSensor_service;
extern const Service SERVICE_UART;

extern const App * const INSTALLED_APPS[];
extern const int INSTALLED_APPS_COUNT;
extern const Service * const INSTALLED_SERVICES[];
extern const int INSTALLED_SERVICES_COUNT;

const App * const INSTALLED_APPS[] = {
	&AppMusicGame_theApp,
	&AppAlarmClock_theApp,
	&AppReactionTime_theApp,
	&AppMenu_theApp,
	&AppAccStep_theApp,
	&AppStepSensor_theApp,
	&AppMusic_app,
	&APP_PM25,
	&APP_SNAKE_2D,
	&APP_SNAKE_3D,
};

const int INSTALLED_APPS_COUNT = 10;

const App* START_UP_APP = &AppMenu_theApp;

const Service * const INSTALLED_SERVICES[] = {
	&ServiceButton_service,
	&ServiceStepSensor_service,
	&SERVICE_UART
};

const int INSTALLED_SERVICES_COUNT = 3;

int main(void)
{
	Timer_init(10);
	OLED_Init();
	OLEDFB_init();
	Buzzer_init();
	MusicLibrary_init();
	//UART_init();
	ADC_init();
	MMA8451_Init();
	
	for(int i=0;i<INSTALLED_SERVICES_COUNT;i++)
	{
		INSTALLED_SERVICES[i]->initHandler();
	}
	
	for(int i=0;i<INSTALLED_APPS_COUNT;i++)
	{
		INSTALLED_APPS[i]->initHandler();
	}
	
	Vec3D cameraPosition = {0, 2.0, 2.8};
	Vec3D angle = {-25, 0, 0};
    OLEDFB3D_setCamera(cameraPosition, angle);
    
    App_switchTo(START_UP_APP);
    
	for(;;)
	{
		OLEDFB_clear();
		App_repaint();
	    OLEDFB_flush();
	}
}
