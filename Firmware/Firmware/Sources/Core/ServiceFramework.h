/*
 * ServiceFramework.h
 *
 *  Created on: Sep 15, 2015
 *      Author: tansinan
 */

#ifndef SERVICEFRAMEWORK_H_
#define SERVICEFRAMEWORK_H_

#include "derivative.h"
#include "Common/Types.h"

typedef struct
{
	bool (*initHandler)();
	void (*taskHandler)();
	uint16_t taskPerformDuration;
} Service;

#endif /* SERVICEFRAMEWORK_H_ */
