/*
 * Delay.c
 *
 *  Created on: Jul 24, 2015
 *      Author: tansinan
 */

#include "Delay.h"

void delayLoop(int count)
{
	int i = 0;
	while(i <= count) i++;
}

void delayMicroseconds(int duration)
{
	delayLoop((duration * 1725 + 500) / 1000);
}

void delay(int msDuration)
{
	delayLoop(msDuration * 1720);
}

void delayms(unsigned int number) {
  unsigned short i,j;
  for(i=0;i<number;i++)
  {
	  for(j=0;j<100;j++)
		  asm("nop");
  }
}

