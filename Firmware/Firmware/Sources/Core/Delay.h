/*
 * Delay.h
 *
 *  Created on: Jul 24, 2015
 *      Author: tansinan
 */

#ifndef DELAY_H_
#define DELAY_H_

void delayLoop(int count);
void delayMicroseconds(int duration);
void delay(int msDuration); // in millisecond
void delayms(unsigned int number);

#endif /* DELAY_H_ */
