/*
 * AppFramework.c
 *
 *  Created on: Jun 11, 2015
 *      Author: tansinan
 */

#include "Common.h"
#include "AppFramework.h"

App* currentApp = NULL;

void App_defaultInitHandler(){}

void App_defaultPaintHandler(){}

void App_defaultEventHandler(){}

void App_switchTo(const App* app)
{
	if(currentApp != NULL)
	{
		App_sendMessage(EVENT_APP_QUIT, 0);
	}
	currentApp = app;
	App_sendMessage(EVENT_APP_START, 0);
}

App* App_getCurrentApp()
{
	return currentApp;
}

void App_sendMessage(int event, int data)
{
	currentApp->eventHandler(event, data);
}

void App_repaint()
{
	currentApp->paintHandler();
}
