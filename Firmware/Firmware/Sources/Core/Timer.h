/*
 * Timer.h
 * Timer.h����SysTick����һ���̶�Ƶ�ʵ�ʱ�ӡ�֮�������������κ�ע���ص���������ϵͳʱ�ӵ�һ����Ƶ�ϡ�
 * SysTick��������Ŀ�ĺ��Ĺ��ܣ���Ϊ��ѯ�Ƿǳ����Ƽ��ģ����жϲ������ǿ��ã����˻���SysTick����ѯ�ƺ���
 * һ�����ԵĽ���������
 *
 *  Created on: May 28, 2015
 *      Author: tansinan
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

void Timer_init(uint16_t duration);
int8_t Timer_set(uint16_t frequency, void (*handler)());
void Timer_unset(int8_t timerId);
uint32_t Timer_getClockFromStartUp();
uint32_t Timer_getClockFromStartUp();
uint32_t Timer_getSecSinceBoot();
uint32_t Timer_setSecSinceBoot(int time);

#endif /* TIMER_H_ */
