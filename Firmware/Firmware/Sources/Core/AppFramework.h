/*
 * AppFramework.h
 *
 *  Created on: May 23, 2015
 *      Author: tansinan
 */

#ifndef APPFRAMEWORK_H_
#define APPFRAMEWORK_H_

#include "Common/Types.h"

typedef struct App
{
	void (*initHandler)();
	void (*paintHandler)();
	void (*eventHandler)(int event, int data);
} App;

void App_defaultInitHandler();

void App_defaultPaintHandler();

void App_defaultEventHandler();

void App_switchTo(const App* app);

App* App_getCurrentApp();

void App_sendMessage(int event, int data);

void App_repaint();

#endif /* APPFRAMEWORK_H_ */
