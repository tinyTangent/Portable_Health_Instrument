/*
 * menu.c
 *
 *  Created on: May 22, 2015
 *      Author: leo
 */

#include "App.h"
#include "Common.h"
#include "Core/AppFramework.h"
#include "Peripheral/Button.h"
#include "Peripheral/OLEDFB.h"
#include "AppMenu.h"
#include <time.h>

const char* s = "FUNC";

void UART_readString(char* str);

void initMenu();
void OLED_MENU_drawMenu();
void OLED_MENU_onKeyPressed(int event, int key);

static char title[20] = "HOME";

Menu menuRoot = {
	.children = (const Menu *[]){&menuFunction1, &menuFunction2, &menuFunction3},
	.childrenApplication = (const App *[]){NULL, NULL, NULL},
	.entries = (const char *[]){"HEALTH", "TOOL", "GAME", "OTHER"},
	.entryCount = 3,
	.parent = NULL,
	.selectedEntry = 0,
	.title = "HOME"	
};

Menu menuFunction1 = {
	.children = (const Menu *[]){NULL, NULL, NULL},
	.childrenApplication = (const App *[]){&AppAccStep_theApp, &AppStepSensor_theApp, &APP_PM25},
	.entries = (const char *[]){"Step (Acc)","Step (Sen)","PM2.5"},
	.entryCount = 3,
	.parent = &menuRoot,
	.selectedEntry = 0,
	.title = "HEALTH"
};

Menu menuFunction2 = {
	.children = (const Menu *[]){NULL, NULL, NULL},
	.childrenApplication = (const App *[]){&AppTimer_app, &AppAlarmClock_theApp, &AppMusic_app},
	.entries = (const char *[]){"Timer","Alarm Clock","MusicPlayer"},
	.entryCount = 3,
	.parent = &menuRoot,
	.selectedEntry = 0,
	.title = "TOOL"	
};

Menu menuFunction3 = {
	.children = (const Menu *[]){NULL, NULL},
	.childrenApplication = (const App *[]){&APP_SNAKE_2D, &APP_SNAKE_3D, &AppMusicGame_theApp},
	.entries =  (const char *[]){"Snake 2D","Snake 3D", "Music (incompat)"},
	.entryCount = 2,
	.parent = &menuRoot,
	.selectedEntry = 0,
	.title = "GAME"	
};

const App AppMenu_theApp = {
	.initHandler = initMenu,
	.paintHandler = OLED_MENU_drawMenu,
	.eventHandler = OLED_MENU_onKeyPressed
};

Menu* currentMenu = NULL;
App OLED_MENU_app;

void initMenu()
{
	OLED_MENU_setRoot(&menuRoot);
}

void OLED_MENU_setRoot(Menu* menu)
{
	currentMenu = menu;
}

void OLED_MENU_drawMenu()
{
	if(currentMenu == NULL) return;
	if(currentMenu->entryCount<4) {
		OLEDFB_drawText(40, 0, currentMenu->title);
	}
	if(currentMenu->entryCount<4)
	{
		for(int i = 0; i < currentMenu->entryCount; i++)
		{
			OLEDFB_drawText(20, (i*2 + 2)*8, currentMenu->entries[i]);
			if( i == currentMenu->selectedEntry)
			{
				OLEDFB_drawText(0, (i*2 + 2)*8, "*");
			}
		}
	}
	/*else {
		for(int i = 0; i < currentMenu->entryCount; i++)
		{	
			OLED_W_Str(i*2, 20, currentMenu->entries[i]);
			if( i == currentMenu->selectedEntry)
			{
				OLED_W_Str(i*2, 0, "*");
			}
			else
			{
				OLED_W_Str(i*2 , 0, " ");
			}
		}
	}*/
}

void OLED_MENU_onKeyPressed(int event, int key)
{
	if(currentMenu == NULL) return;
	char buf[20];
	if(event == EVENT_UART_READY_READ)
	{
		UART_readString(title);
		//return;
	}
	else if(event == EVENT_KEY_PRESSED)
	{
		switch(key)
		{
		case KEY_UP:
			if(currentMenu->selectedEntry > 0)
			{
				currentMenu->selectedEntry --;
			}
			break;
		case KEY_DOWN:
			if(currentMenu->selectedEntry < currentMenu->entryCount-1)
			{
				currentMenu->selectedEntry ++;
			}
			break;
		case KEY_RIGHT:
			if(currentMenu->children[currentMenu->selectedEntry] != NULL)
			{
				OLED_Clr();
				currentMenu = currentMenu->children[currentMenu->selectedEntry];
			}
			else if(currentMenu->childrenApplication[currentMenu->selectedEntry] != NULL)
			{
				OLED_Clr();
				App_switchTo(currentMenu->childrenApplication[currentMenu->selectedEntry]);
			}
			break;
		case KEY_LEFT:
			if(currentMenu->parent != NULL)
			{
				OLED_Clr();
				currentMenu = currentMenu->parent;
			}
			break;
		}
	}
}

