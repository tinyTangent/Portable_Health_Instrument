#include "Common.h"
#include "App.h"
#include "Core/Delay.h"
#include "Core/AppFramework.h"
#include "Peripheral/OLEDFB.h"
#include "Peripheral/ADC.h"
#include "Peripheral/OneWire.h"
#include "Peripheral/DallasTemperature.h"
#include "Peripheral/UART.h"

#define STATE_SET 0
#define STATE_RUNNING 2
#define STATE_PAUSE 3
#define STATE_TIMEOUT 4


static OneWire oneWire;
static DallasTemperature dallasTemp;

static const float pm25BaseVoltage = 1.24;
static const float pm25baseTemp = 25.0;

float getPm25BaseVoltage(float temp)
{
	if(temp < 40.0 && temp > -10.0)
	{
		return pm25BaseVoltage
				+ 0.006 * (temp - pm25baseTemp);
	}
	else
		return pm25BaseVoltage
				+ 0.006 * (40 - pm25baseTemp)
				- (temp - 40) * 0.0015;
}

static void initHandler();
static void paintHandler();
static void eventHandler(int event, int data);

const App APP_PM25 = {
	.initHandler = initHandler,
	.paintHandler = paintHandler,
	.eventHandler = eventHandler
};

static void initHandler()
{
	OneWire_init(&oneWire);
	DallasTemperature_init(&dallasTemp, &oneWire);
	DallasTemperature_begin(&dallasTemp);
	
	//Init PortC1
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;
	PORTC_PCR1 = 0x100;
	
	//Init PortB19
	SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;
	PORTB_PCR19 = 0x100;
	
	//Config PortB 19 as output
	DIRECT_MODE_OUTPUT((GPIO_MemMapPtr)PTB_BASE_PTR, 1<<19); 
}

/**
 * Warning : Interupts must be disabled so that communication protocol
 * time sequence works as expected.
 */
static void paintHandler()
{
	char str[20];
	//Get temperature
	DallasTemperature_requestTemperatures(&dallasTemp);
	
	asm("CPSID i");
	float temp = DallasTemperature_getTempCByIndex(&dallasTemp, 0);
	asm("CPSIE i");
	
	//Get PM2.5
	int origValue = 0;
	int dataCount = 100;
	for(int i=0;i<100;i++)
	{
		DIRECT_WRITE_HIGH((GPIO_MemMapPtr)PTB_BASE_PTR, 1<<19);
		delayMicroseconds(280);
		int adcVal = ADC_readData();
		float singlePM25Value = adcVal*3.3/4096;
		if(singlePM25Value < getPm25BaseVoltage(temp)) dataCount--;
		else origValue += adcVal;
		delayMicroseconds(40);
		DIRECT_WRITE_LOW((GPIO_MemMapPtr)PTB_BASE_PTR, 1<<19);
		delayMicroseconds(9680);
	}
	origValue /= dataCount;
	float pm25Voltage = origValue*3.3/4096;
	sprintf(str,"Temp : %d.%d ",(int)temp,(temp-(int)temp)*10);
	OLEDFB_drawText(0, 0, str);
	float pm25 = 600.0 * (pm25Voltage - getPm25BaseVoltage(temp));
	sprintf(str,"PM2.5 : %d.%d\r\n",(int)pm25,(temp-(int)pm25)*10);
	OLEDFB_drawText(0, 32, str);
	UART_putString(str);
	UART_putString("\r\n");
}

static void eventHandler(int event, int data)
{
	switch(event)
	{
	case EVENT_KEY_LONG_PRESSED:
		if(data == KEY_LEFT)
			App_switchTo(&AppMenu_theApp);
		break;
	}
}
