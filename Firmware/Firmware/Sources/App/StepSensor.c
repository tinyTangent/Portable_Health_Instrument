/*
 * AppStepSensor.c
 *
 *  Created on: May 30, 2015
 *      Author: leo
 */

#include <cstdio>
#include "Common.h"
#include "App.h"
#include "Core/AppFramework.h"
#include "Peripheral/StepSensor.h"
#include "Peripheral/OLEDFB.h"

App AppStepSensor_app;

static void AppStepSensor_drawHandler();
static void AppStepSensor_onKeyPressed(int event, int key);

const App AppStepSensor_theApp = {
	.initHandler = App_defaultInitHandler,
	.paintHandler = AppStepSensor_drawHandler,
	.eventHandler = AppStepSensor_onKeyPressed
};

static void AppStepSensor_drawHandler()
{
	char temp[20];
	OLEDFB_clear();
	//OLEDFB_drawText(1,0,"hELLO");
	sprintf(temp,"Step:%d",StepSensor_queryStep());
	OLEDFB_drawText(10,10,temp);
	OLEDFB_flush();
}


static void AppStepSensor_onKeyPressed(int event, int key)
{
	if(event == EVENT_KEY_LONG_PRESSED && key == KEY_LEFT)
	{
		App_switchTo(&AppMenu_theApp);		
	}
}
