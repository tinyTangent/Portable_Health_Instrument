/*
 * Menu.h
 *
 *  Created on: May 22, 2015
 *      Author: tansinan
 */

#include "Core/AppFramework.h"

#ifndef MENU_H_
#define MENU_H_

#include "Core/AppFramework.h"

typedef struct Menu
{
	const struct Menu* parent;
	const char* title;
	char entryCount;
	char selectedEntry;
	const char** entries;
	const struct Menu** children;
	const App** childrenApplication;
} Menu;

void OLED_MENU_setRoot(Menu* menu);

extern Menu menuRoot;
extern Menu menuFunction1;
extern Menu menuFunction2;
extern Menu menuFunction3;

#endif /* MENU_H_ */
