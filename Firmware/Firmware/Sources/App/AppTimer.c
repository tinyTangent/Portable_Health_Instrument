/*
 * AppTimer.c
 *
 *  Created on: May 31, 2015
 *      Author: leo
 */

#include "Common.h"
#include "App.h"
#include "Core/Timer.h"
#include "Core/AppFramework.h"

int enableTimer=0;
int minute=0;
int second=0;
int second_1=0;
int second_2=0;

static uint8_t timerId = 0xFF;
static void AppTimer_drawHandler();
static void AppTimer_onKeyPressed(int data, int key);

const App AppTimer_app = {
	.initHandler = App_defaultEventHandler,
	.eventHandler = AppTimer_onKeyPressed,
	.paintHandler = AppTimer_drawHandler,
};

static void onTimer(void)
{
	//do something
	//Clear the flag of channel0,PIT
	static timerCounter=0;
	
	//step_query();
	//ButtonBuzz();
		if(enableTimer) {
			timerCounter++;
			if(timerCounter>=5) {
				second_2++;
				timerCounter=0;
			}
			if(second_2>=10) {
				second_2=0;
				second_1++;
			}
			
			if(second_1>=10) {
				second_1=0;
				second++;
			}
			
			if(second>=60) {
				second=0;
				minute++;
			}
		}	
}

static int blink=1;

static void AppTimer_drawHandler()
{
	static int _count=0;
	char str[20];
	OLEDFB_clear();
	OLEDFB_drawText(1,8,"Timer:");
	if(!blink) {
		sprintf(str,"  :  :  ");
		str[1]=minute%10+'0';
		str[0]=(minute/10)%10+'0';
		str[4]=second%10+'0';
		str[3]=(second/10)%10+'0';
		str[7]=second_2%10+'0';
		str[6]=(second_1)%10+'0';
		OLEDFB_drawText(30,32,str);
	}  else {
		if(_count>=15) {
			if(_count>=40) {
				_count=0;
			}
			sprintf(str,"  :  :  ");
			str[1]=minute%10+'0';
			str[0]=(minute/10)%10+'0';
			str[4]=second%10+'0';
			str[3]=(second/10)%10+'0';
			str[7]=second_2%10+'0';
			str[6]=(second_1)%10+'0';
			OLEDFB_drawText(30,32,str);
		} else {
			sprintf(str,"  :  :  ");
			//OLED_W_Str(4,30,str);
			OLEDFB_drawText(30,32,str);
		}
	}
	_count++;
	OLEDFB_drawText(1,55,"Stp   zero   sta");
	OLEDFB_flush();
}

static void AppTimer_onKeyPressed(int event, int key)
{
	switch(event)
	{
	case EVENT_APP_START:
		timerId = Timer_set(1, onTimer);
		break;
	case EVENT_APP_QUIT:
		Timer_unset(timerId);
		break;
	case EVENT_KEY_LONG_PRESSED:
		if(key == KEY_LEFT)
			App_switchTo(&AppMenu_theApp);
		break;
	case EVENT_KEY_PRESSED:
		if(key == KEY_LEFT)
		{
			enableTimer=0;
			blink=1;
		}
		else if(key == KEY_RIGHT)
		{
			enableTimer=1;
			blink=0;
		}
		else if(key == KEY_UP)
		{
			enableTimer=0;
			minute=0;
			second=0;
			second_1=0;
			second_2=0;
			blink=1;
				
		}
		else if(key == KEY_DOWN)
		{
			App_switchTo(&AppMenu_theApp);
			enableTimer=0;
			minute=0;
			second=0;
			second_1=0;
			second_2=0;
			blink=1;
		}
		break;
	}
}
