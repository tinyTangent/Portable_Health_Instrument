/*
 * AppSnakeGame.c
 *
 *  Created on: Jun 3, 2015
 *      Author: leo
 */

#include "Common.h"
#include "App.h"
#include "Core/AppFramework.h"
#include "Peripheral/OLEDFB.h"
#define FUNC 1
#define UP 2
#define DOWN 3
#define LEFT 4
#define RIGHT 5
#define N 25

void AppSnakeGame_drawHandler();
void AppSnakeGame_eventHandler(int event, int key);

static char keyRecord = 0;
static char keyRecord_upEdge=0;

static int handleChangeFlag=0;
static int buzzFlag=0;

const App APP_SNAKE_2D = {
	.eventHandler = AppSnakeGame_eventHandler,
	.initHandler = App_defaultInitHandler,
	.paintHandler = AppSnakeGame_drawHandler
};

static int snake_x=0;
static int snake_y=0;

static int gameFlag=1;

static void delay(int duration)
{
	for(int j=0;j<40*duration;j++)
  		asm("nop");
}

static struct Food
{
	unsigned char x;
	unsigned char y;
	unsigned char yes;
} food;//食物结构体

static struct Snake
{
	unsigned char x[N];
	unsigned char y[N];
	unsigned char node;
	unsigned char direction;
	unsigned char life;
} snake;//蛇结构体

void AppSnakeGame_drawHandler()
{
	int i=0;
	gameFlag=1;
	handleChangeFlag=1;
	OLEDFB_clear();
	
	//画出边框
	DrawBoard();
	MusicPlay(4);
	GamePlay();//玩游戏
	GameOver();//游戏结束
	OLEDFB_clear();
	OLED_Clr();
	MusicStop();
}


void AppSnakeGame_eventHandler(int event, int key)
{
	switch(event)
	{
	case EVENT_KEY_LONG_PRESSED:
		if(key == KEY_LEFT)
		{
			App_switchTo(&AppMenu_theApp);
		}
		break;
	case EVENT_KEY_DOWN:
		switch(key)
		{
		case KEY_LEFT:
			snake.direction=LEFT;
			buzzFlag=1;
			break;
		case KEY_RIGHT:
			snake.direction=RIGHT;
			buzzFlag=1;
			break;
		case KEY_UP:
			snake.direction=UP;
			buzzFlag=1;
			break;
		case KEY_DOWN:
			snake.direction=DOWN;
			buzzFlag=1;
			break;
		}
	}
}
/**************************************************************/
static unsigned long Seed = 1;
#define A 48271L
#define M 2147483647L
#define Q (M / A)
#define R (M % A)
/************************************
伪随机数发生器
*************************************/
double Random(void)
{
	long TmpSeed;
	TmpSeed=A*(Seed%Q)-R*(Seed/Q);
	if(TmpSeed>=0)
		Seed=TmpSeed;
	else
		Seed=TmpSeed+M;
	return (double)Seed/M;
}

/**************************************
为伪随机数发生器播种
***************************************/
void InitRandom(unsigned long InitVal)
{
	Seed=rand();
}


unsigned char Flag=0;
unsigned char Score=0;
unsigned char Speed=5;
unsigned char KeyBuffer=0;

int snakeSpeed=3;
int level=1;
int PASSSCORE=5;//预定义过关成绩

/******************************
画墙壁，初始化界面
*******************************/
void DrawBoard()
{
	int i=0;
	OLEDFB_drawLine(0,0,0,63);
	OLEDFB_drawLine(101,0,101,63);
	OLEDFB_drawLine(0,0,101,0);
	OLEDFB_drawLine(0,63,101,63);
}


/***************************
打印成绩
****************************/
void PrintScore(void)
{
	unsigned char Str[3];
	Str[0]=(Score/10)|0x30;//十位
	Str[1]=(Score%10)|0x30;//个位
	Str[2]=0;
	OLEDFB_drawText(108,50,Str);
}

/********************************
打印速度级别
*********************************/
void PrintSpeed(void)
{
	unsigned char Str[2];
	OLEDFB_drawText(103,0,"Lv");
	Str[0]=(level/10)|0x30;//十位
	Str[1]=(level%10)|0x30;//个位
	OLEDFB_drawText(108, 20,Str);
}

/***********************************
游戏结束处理
************************************/
void GameOver()
{
	unsigned char n;
	OLEDFB_drawRect(food.x,food.y,food.x+2,food.y+2,OLEDFB_WHITE);//消隐出食物
	for(n=1;n<snake.node;n++)
	{
		OLEDFB_drawRect(snake.x[n],snake.y[n],snake.x[n]+2,snake.y[n]+2,OLEDFB_WHITE);//消隐食物，蛇头已到墙壁内，故不用消去		
	}
	if(Score<=(PASSSCORE-1)) {
		
	} else {
		OLED_W_Str(1,2,"Winner!!");
		PASSSCORE+=level*5;
		level++;
	}
	delayms(10000);
}

void SnakeDie()
{
	unsigned char n;
	OLEDFB_drawRect(food.x,food.y,food.x+2,food.y+2,OLEDFB_WHITE);//消隐出食物
	for(n=1;n<snake.node;n++)
	{
		OLEDFB_drawRect(snake.x[n],snake.y[n],snake.x[n]+2,snake.y[n]+2,OLEDFB_WHITE);//消隐食物，蛇头已到墙壁内，故不用消去		
	}

		OLED_W_Str(1,2,"Game Over");
		Score=0;

	delayms(10000);
}


/********************************
游戏的具体过程，也是贪吃蛇算法的关键部分
*********************************/
void GamePlay()
{
	unsigned char n;
	InitRandom(50);
	
	food.yes=1;//1表示需要出现新事物，0表示已经存在食物尚未吃掉
	snake.life=0;//表示蛇还活着
	snake.direction=DOWN;
	snake.x[0]=6;snake.y[0]=6;
	snake.x[1]=3;snake.y[1]=6;
	snake.node=2;
	while(gameFlag)
	{
		OLEDFB_clear();
		DrawBoard();
		PrintScore();
		PrintSpeed();
		if(food.yes==1)
		{
			while(1)
			{
				food.x=Random()*85+3;
				food.y=Random()*55+3;//获得随机数
	
				while(food.x%3!=0)
					food.x++;
				while(food.y%3!=0)
					food.y++;
			    for(n=0;n<snake.node;n++)//判断产生的食物坐标是否和蛇身重合
				{
					if((food.x==snake.x[n])&&(food.y==snake.y[n]))
						break;
				}
				if(n==snake.node)
				{
					food.yes=0;
					break;//产生有效的食物坐标
				}
			}
		}
		if(food.yes==0)
		{
			OLEDFB_drawRect(food.x,food.y,food.x+2,food.y+2,OLEDFB_WHITE);
		}	
		for(n=snake.node-1;n>0;n--)
		{
			snake.x[n]=snake.x[n-1];
			snake.y[n]=snake.y[n-1];
		}
		switch(snake.direction)
		{
			case DOWN:snake.y[0]+=3;break;
			case UP:snake.y[0]-=3;break;
			case RIGHT:snake.x[0]+=3;break;
			case LEFT:snake.x[0]-=3;break;
			default:break;
		}
		for(n=3;n<snake.node;n++)//从第三节开始判断蛇头是否咬到自己
		{
			if(snake.x[n]==snake.x[0]&&snake.y[n]==snake.y[0])
			{
				//GameOver();
				SnakeDie();
				snake.life=1;
				break;
			}
		}
		if(snake.x[0]<3||snake.x[0]>=100||snake.y[0]<3||snake.y[0]>=63)//判蛇头是否撞到墙壁
		{
			//GameOver();
			SnakeDie();
			gameFlag=0;
			snake.life=1;
		}
		if(snake.life==1)
			break;//蛇死，则跳出while(1)循环
		if(snake.x[0]==food.x&&snake.y[0]==food.y)//判蛇是否吃到食物
		{
			OLEDFB_drawRect(food.x,food.y,food.x+2,food.y+2,OLEDFB_WHITE);//消隐食物
			snake.x[snake.node]=200;
			snake.y[snake.node]=200;//产生蛇新的节坐标先放在看不见的位置
			snake.node++;//蛇节数加1
			food.yes=1;//食物标志置1
			
			if(++Score>=PASSSCORE)
			{
				PrintScore();
				GameOver();
				break;
			}
		}
		for(n=0;n<snake.node;n++)
		{
			OLEDFB_drawRect(snake.x[n],snake.y[n],snake.x[n]+2,snake.y[n]+2,OLEDFB_WHITE);
		}//根据蛇的节数画出蛇
		delay(Speed*1000);
		OLEDFB_drawRect(snake.x[snake.node-1],snake.y[snake.node-1],snake.x[snake.node-1]+2,snake.y[snake.node-1]+2,OLEDFB_WHITE);
		switch(KeyBuffer)
		{
			case FUNC:
					KeyBuffer=0;
					if(++Speed>=10)
						Speed=1;
				   	PrintSpeed();
					break;
			case DOWN:
					KeyBuffer=0;
					if(snake.direction!=UP)
						snake.direction=DOWN;
					break;
			case UP:
					KeyBuffer=0;
					if(snake.direction!=DOWN)
						snake.direction=UP;
					break;
			case RIGHT:
					KeyBuffer=0;
					if(snake.direction!=LEFT)
						snake.direction=RIGHT;
					break;
		   	case LEFT:
					KeyBuffer=0;
					if(snake.direction!=RIGHT)
						snake.direction=LEFT;
					break;
			default:
					break;
		}			
		OLEDFB_flush();	
	}//结束while(1)
}

