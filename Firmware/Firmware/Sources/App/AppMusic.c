/*
 * AppMusic.c
 *
 *  Created on: May 31, 2015
 *      Author: leo
 */

#include "Common.h"
#include "App.h"
#include "Core/AppFramework.h"
#include "LMusic.h"

void AppMusic_drawHandler();
void AppMusic_onKeyPressed(int event, int key);

const App AppMusic_app = {
	.initHandler = App_defaultInitHandler,
	.paintHandler = AppMusic_drawHandler,
	.eventHandler = AppMusic_onKeyPressed
};

int musicChoice = 0;


/*
 * UP��,��һ������
 * down��,��һ��
 * ����˳���ǰ�˵�
 * �Ҽ�ֹͣ����
 */

void AppMusic_drawHandler()
{
	char str[20];
	static int counter=0;
	counter++;
	if(counter>=10) {
		//ADC_channel_potentiometer();
		//ADC_potentiometer=ADC0_TR_DATA()-127;
		counter=0;
	}
	OLEDFB_clear();
	OLEDFB_drawText(1,8,"Music:");
	
	if(musicChoice==1) {
		OLEDFB_drawText(6,24,"Marie's lamb");
	} else if(musicChoice==2) {
		OLEDFB_drawText(6,24,"Invisible wings");
	} else if(musicChoice==3) {
		OLEDFB_drawText(6,24,"City of Sky");
	} else if(musicChoice==4) {
		OLEDFB_drawText(6,24,"Song of Joy");
	}
	
	 sprintf(str,"Volume:       ");
			str[12]='0';
			/*str[11]=(ADC_potentiometer/10)%10+'0';
			str[10]=(ADC_potentiometer/100)%10+'0';
			str[9]=(ADC_potentiometer/1000)%10+'0';
			str[8]=(ADC_potentiometer/10000)%10+'0';*/
			OLEDFB_drawText(1,55,str);
			OLEDFB_flush();
	//OLED_W_Numeral(4,10,step);
	
}


void AppMusic_onKeyPressed(int event, int key)
{
	if(event == EVENT_APP_QUIT)
	{
		MusicStop();
	}
	if(event == EVENT_KEY_LONG_PRESSED && key == KEY_LEFT)
	{
		App_switchTo(&AppMenu_theApp);
	}
	if(event != EVENT_KEY_PRESSED) return;
	if(key == KEY_LEFT)
	{
		OLED_Clr();
		//App_switchTo(OLED_MENU_getApp());
		musicChoice=0;
		/*LED_color(BLACK,1);
		LED_color(BLACK,4);
		LED_color(BLACK,2);
		LED_color(BLACK,3);*/
	}
	else if(key==KEY_UP) {
		if(musicChoice<=3) { 
			musicChoice++;
		}
		OLED_Clr();
		MusicPlay(musicChoice);
	}
	else if(key==KEY_DOWN) {
		if(musicChoice>=2) {
			musicChoice--;
		}
		OLED_Clr();
		MusicPlay(musicChoice);
	}
	else if(key==KEY_RIGHT) {
		MusicStop();
		OLED_Clr();
	}
}
