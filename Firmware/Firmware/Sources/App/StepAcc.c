/*
 * AppAccStep.c
 *
 *  Created on: Jun 11, 2015
 *      Author: leo
 */

#include "Common.h"
#include "App.h"
#include "Core/AppFramework.h"
#include "Peripheral/MMA8451Q.h"

//TODO : These Functions needs to be fixed
static void MusicPlay(int a){}
static void MusicStop(){}
static void delayms(int b){}

App AppAccStep_app;

int acc_x_pre=0;
int acc_y_pre=0;
int acc_z_pre=0;
int stepDistance=0;
int accStep=0;
int stepTarget=1000;
float stepEnergy=0;
int stepEidt=0;
int stepEditChoice=0;
int stepEdit[4]={0,1,0,0};  //默认是100步

int powerOff=0;

int _flagToMusic=1;

void AppAccStep_drawHandler();
void AppAccStep_eventHandler(int event, int data);

const App AppAccStep_theApp = {
	.initHandler = App_defaultInitHandler,
	.paintHandler = AppAccStep_drawHandler,
	.eventHandler = AppAccStep_eventHandler
};

void AppAccStep_drawHandler()
{
	char str[20];
	int _sum=0;
	static int counter  = 0;
	OLEDFB_clear();
	counter++;
	if(!powerOff) {
		if(counter>=7) {
			counter=0;
			Demo_accMeasure();
		}
		//OLEDFB_drawText(10,1,"STEP COUNTER");
		if(!stepEidt) {
			sprintf(str,"Step:     ");
			str[9]=accStep%10+'0';
			str[8]=(accStep/10)%10+'0';
			str[7]=(accStep/100)%10+'0';
			str[6]=(accStep/1000)%10+'0';
			OLEDFB_drawText(1,1,str);
			
			sprintf(str,"Distan:     m");
			str[11]=stepDistance%10+'0';
			str[10]=(stepDistance/10)%10+'0';
			str[9]=(stepDistance/100)%10+'0';
			str[8]=(stepDistance/1000)%10+'0';
			OLEDFB_drawText(1,15,str);
			
			sprintf(str,"Target:     ");
			str[11]=stepTarget%10+'0';
			str[10]=(stepTarget/10)%10+'0';
			str[9]=(stepTarget/100)%10+'0';
			str[8]=(stepTarget/1000)%10+'0';
			//OLED_W_Str(2,1,str);
			OLEDFB_drawText(1,43,str);
			
			sprintf(str,"Energy:    Kcal");
			str[10]=((int)stepEnergy)%10+'0';
			str[9]=(((int)stepEnergy)/10)%10+'0';
			str[8]=(((int)stepEnergy)/100)%10+'0';
			//OLED_W_Str(4,1,str);
			OLEDFB_drawText(1,29,str);
			OLEDFB_drawText(90,53,"Zero");
			_sum=(acc_x-acc_x_pre)*(acc_x-acc_x_pre)+(acc_y-acc_y_pre)*(acc_y-acc_y_pre)+(acc_z-acc_z_pre)*(acc_z-acc_z_pre);
			if(_sum>300) {
				accStep++;
			}
			stepDistance=accStep*0.5;
			stepEnergy=accStep*0.04;
			acc_x_pre=acc_x;
			acc_y_pre=acc_y;
			acc_z_pre=acc_z;
			if((stepTarget==accStep)&&(_flagToMusic)) {
				/*LED_color(GREEN,1);
				LED_color(GREEN,4);
				LED_color(GREEN,2);
				LED_color(GREEN,3);*/
				MusicPlay(1);
				_flagToMusic=0;
			}
		} else {
			//OLED_W_Str(1,10,"SetClock");
			OLEDFB_drawText(10,20,"SetTarget");
			sprintf(str,"      ");
			str[2]=stepEdit[2]+'0';
			str[1]=stepEdit[3]+'0';
			str[4]=stepEdit[0]+'0';
			str[3]=stepEdit[1]+'0';
			
			//OLED_W_Str(4,30,str);
			OLEDFB_drawLine(70-8*stepEditChoice,44,70-8*stepEditChoice-8,44);
			OLEDFB_drawText(30,35,str);
			OLEDFB_drawText(1,55,"             OK");
		}
	}
	OLEDFB_flush();
}

void AppAccStep_eventHandler(int event, int key)
{
	switch(event)
	{
	case EVENT_KEY_PRESSED:
		if(key == KEY_RIGHT)
		{
			if(!stepEidt) {
				MusicStop();
				_flagToMusic=1;
				//LED_color(BLACK,1);
				//LED_color(BLACK,4);
				//LED_color(BLACK,2);
				//LED_color(BLACK,3);
			} else {
				if(stepEditChoice>=1) {
					stepEditChoice--;
				}
			}
		}
		else if(key == KEY_LEFT)
		{
			if(!stepEidt) {
				MusicStop();
				_flagToMusic=1;
				//LED_color(BLACK,1);
				//LED_color(BLACK,4);
				//LED_color(BLACK,2);
				//LED_color(BLACK,3);
			} else {
				if(stepEditChoice<=2) {
					stepEditChoice++;
				}
			}
		}
		else if(key == KEY_UP)
		{
			if(!stepEidt) {
				MusicStop();
				_flagToMusic=1;
				//LED_color(BLACK,1);
				//LED_color(BLACK,4);
				//LED_color(BLACK,2);
				//LED_color(BLACK,3);
			} else {
				if(stepEdit[stepEditChoice]<=8) {
					stepEdit[stepEditChoice]++;
				} 
			}
		}
		else if(key == KEY_DOWN)
		{
			if(!stepEidt) {
				App_switchTo(&AppMenu_theApp);
				stepEdit[1]=1; //有一个预设值,防止它直接闹铃,
				OLED_Clr();
				MusicStop();
				_flagToMusic=1;
				//LED_color(BLACK,1);
				//LED_color(BLACK,4);
				//LED_color(BLACK,2);
				//LED_color(BLACK,3);
			} else {
				if(stepEdit[stepEditChoice]>=1) {
					stepEdit[stepEditChoice]--;
				}
			}
		}
		break;
	case EVENT_KEY_LONG_PRESSED:
		if(key == KEY_UP)
		{
			stepEidt=1;
			stepEdit[1]=0;
			OLED_Clr();
		}
		else if(key == KEY_DOWN)
		{
		}
		else if(key == KEY_RIGHT)
		{
			if(!stepEidt) {
				accStep=0;
				OLED_Clr();
				MusicStop();
				OLED_W_Str(3,20,"Waiting...");
				delayms(9000);
				OLED_Clr();
				OLED_W_Str(3,20,"Success Clear");
				delayms(6000);
				OLED_Clr();
			} else {
				stepEidt=0;
				OLED_Clr();
				OLED_W_Str(3,5,"Success Edit!");
				stepTarget=stepEdit[0]+stepEdit[1]*10+stepEdit[2]*100+stepEdit[3]*1000;
				delayms(6000);
				OLED_Clr();
			}
		}
		else if(key == KEY_LEFT)
		{
			App_switchTo(&AppMenu_theApp);
			//powerOff=~powerOff;
		}
		break;
	}
}
