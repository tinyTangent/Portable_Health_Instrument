#include <stdio.h>
#include "DateTime.h"

static DateTime currentTime = {0,0,0,0,0,0};

DateTime* DateTime_getCurrent()
{
	return &currentTime;
}

void DateTime_fromString(DateTime* dateTime, char* str)
{
	sscanf(str, "%d-%d-%d %d:%d:%d",
		&dateTime->year, &dateTime->month, &dateTime->day,
		&dateTime->hour, &dateTime->minute, &dateTime->second);
}

void DateTime_toString(DateTime* dateTime, char* str)
{
	sprintf(str, "%d-%d-%d %d:%d:%d",
		dateTime->year, dateTime->month, dateTime->day,
		dateTime->hour, dateTime->minute, dateTime->second);
}

void DateTime_plusSecond(DateTime* dateTime, int second)
{
	dateTime->second += second;
	DateTime_normalize(dateTime);
}

void DateTime_minusSecond(DateTime* dateTime, int second)
{
	dateTime->second -= second;
	DateTime_normalize(dateTime);
}

void DateTime_normalize(DateTime* dateTime)
{
	dateTime->minute += dateTime->second / 60;
	dateTime->second %= 60;
	if(dateTime->second < 0)
	{
		dateTime->second += 60;
		dateTime->minute--;
	}
	dateTime->hour += dateTime->minute / 60;
	dateTime->minute %= 60;
	if(dateTime->minute < 0)
	{
		dateTime->minute += 60;
		dateTime->hour--;
	}
}

int DateTime_diff(DateTime* time1, DateTime* time2);
