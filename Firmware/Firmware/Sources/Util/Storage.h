/*
 * OneWire.h
 *
 *  Created on: Oct 26, 2015
 *      Author: tansinan
 */

#ifndef UTIL_STORAGE_H_
#define UTIL_STORAGE_H_

#include <stdint.h>

void recordSport();
void recordPollution(int val);
uint16_t getAvailableDataCount();
uint32_t getRecordDuration();
uint16_t* getPollutionData();
uint16_t* getSportData();

#endif
