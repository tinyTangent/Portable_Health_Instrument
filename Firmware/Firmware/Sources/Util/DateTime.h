/*
 * OneWire.h
 *
 *  Created on: Oct 26, 2015
 *      Author: tansinan
 */

#ifndef UTIL_DATETIME_H_
#define UTIL_DATETIME_H_

#include "Common.h"

typedef struct {
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} DateTime;

DateTime* DateTime_getCurrent();
void DateTime_fromString(DateTime* dateTime, char* str);
void DateTime_toString(DateTime* dateTime, char* str);
void DateTime_plusSecond(DateTime* dateTime, int second);
void DateTime_minusSecond(DateTime* dateTime, int second);
void DateTime_normalize(DateTime* dateTime);
int DateTime_diff(DateTime* time1, DateTime* time2);

#endif
