/*
 * LMusic.h
 *
 *  Created on: Sep 19, 2015
 *      Author: tansinan
 */

#ifndef LMUSIC_H_
#define LMUSIC_H_

void MusicPlay(int _musicChoice);
void MusicStop();

#endif /* LMUSIC_H_ */
