/*
 * PinBinding.h
 *
 *  Created on: Sep 18, 2015
 *      Author: tansinan
 */

#ifndef PINBINDING_H_
#define PINBINDING_H_

#include "derivative.h"
#include "Types.h"

/**
 * Define Ports for the MCU
 * Warning: this order must be compliant with the index of GPIO_BASE_PTRS in KL25Z128.h!
 */
typedef enum
{
	PORT_A = 0,
	PORT_B = 1,
	PORT_C = 2,
	PORT_D = 3,
	PORT_E = 4
} Port;

/**
 * Typedef uint16 as Pin number
 */
typedef uint16_t PinNumber;

/**
 * Define a structure to identify a pin of the MCU
 */
typedef struct {
	Port port;
	PinNumber pin;
} Pin;

/**
 * Warning: The count is required to be updated if you have add buttons!
 */
#define BUTTON_PIN_BINDING_COUNT 4

/**
 * Define the pins used by bottons.
 * Warning: this order must be compliant with the key order in Constant.h!
 */
extern const Pin BUTTON_PIN_BINDING[];

/**
 * Define the pin used by the step sensor.
 */
extern const Pin STEP_SENSOR_PIN_BINDING;

/**
 * This is a work around for FreeScale's definition has no type.
 * Do not remove, and be cautious about the 'S' when coding.
 */
extern const PORT_MemMapPtr PORT_BASE_PTR[];
extern const GPIO_MemMapPtr GPIO_BASE_PTR[];

#endif /* PINBINDING_H_ */
