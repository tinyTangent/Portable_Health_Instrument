/*
 * PinBinding.c
 *
 *  Created on: Sep 18, 2015
 *      Author: tansinan
 */

#include "PinBinding.h"

/**
 * This is a work around for FreeScale's definition has no type.
 * Do not remove, and be cautious about the 'S' when coding.
 */
const PORT_MemMapPtr PORT_BASE_PTR[] = PORT_BASE_PTRS;
const GPIO_MemMapPtr GPIO_BASE_PTR[] = GPIO_BASE_PTRS;

/**
 * Warning: this order need to be compliant with the key order in Constant.h!
 * Warning: The BUTTON_PIN_BINDING_COUNT is required to be updated if you have add buttons!
 * @see PinBinding.h
 */
const Pin BUTTON_PIN_BINDING[BUTTON_PIN_BINDING_COUNT] = {
	{PORT_B, 17},
	{PORT_B, 16},
	{PORT_B, 9},
	{PORT_B, 8}
};

/**
 * Define the pin used by the step sensor.
 */
const Pin STEP_SENSOR_PIN_BINDING = {PORT_D, 6};
