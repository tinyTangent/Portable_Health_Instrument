/*
 * Constants.h
 *
 *  Created on: Sep 17, 2015
 *      Author: tansinan
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

//Define all possible bitmasks
#define BIT0 0x01
#define BIT1 (1<<1)
#define BIT2 (1<<2)
#define BIT3 (1<<3)
#define BIT4 (1<<4)
#define BIT5 (1<<5)
#define BIT6 (1<<6)
#define BIT7 (1<<7)
#define BIT8 (1<<8)
#define BIT9 (1<<9)
#define BIT10 (1<<10)
#define BIT11 (1<<11)
#define BIT12 (1<<12)
#define BIT13 (1<<13)
#define BIT14 (1<<14)
#define BIT15 (1<<15)
#define BIT16 (1<<16)
#define BIT17 (1<<17)
#define BIT18 (1<<18)
#define BIT19 (1<<19)
#define BIT20 (1<<20)
#define BIT21 (1<<21)
#define BIT22 (1<<22)
#define BIT23 (1<<23)
#define BIT24 (1<<24)
#define BIT25 (1<<25)
#define BIT26 (1<<26)
#define BIT27 (1<<27)
#define BIT28 (1<<28)
#define BIT29 (1<<29)
#define BIT30 (1<<30)
#define BIT31 (1<<31)

//An enumerator containing all possible directions
typedef enum Direction
{
	DIRECTION_UP = 0,
	DIRECTION_DOWN = 1,
	DIRECTION_LEFT = 2,
	DIRECTION_RIGHT = 3,
} Direction;

//An enumerator containing all key definitions
typedef enum
{
	KEY_UP = 0,
	KEY_DOWN = 1,
	KEY_LEFT = 2,
	KEY_RIGHT = 3,
	KEY_A = 4,
	KEY_B = 5,
	KEY_C = 6,
	KEY_D = 7
} Key;

//An enumerator containing all possible events
typedef enum
{
	EVENT_APP_START,
	EVENT_APP_QUIT,
	EVENT_TIMER,
	EVENT_KEY_DOWN,
	EVENT_KEY_PRESSED,
	EVENT_KEY_LONG_PRESSED, 
	EVENT_UART_READY_READ
} Event;

//Buffer for constant
#define UART_HAVE_STRING_END 1

#endif /* CONSTANTS_H_ */
