/*
 * Types.h
 *
 *  Created on: May 28, 2015
 *      Author: tansinan
 */

#ifndef TYPES_H_
#define TYPES_H_


//Typedef some data type
typedef unsigned char uchar;
typedef unsigned char bool;
typedef unsigned int uint;

//Typedef some float vector/matrix/geometry data types
typedef float Vec2D[2];
typedef float Vec3D[3];
typedef float Mat33[3][3];
typedef float Triangle2D[3][2];
typedef float Triangle3D[3][3];
typedef float Quad2D[4][2];
typedef float Quad3D[4][3];

#define true 1
#define false 0
#define TRUE 1
#define FALSE 0

#ifndef NULL
#define NULL 0
#endif

#define nullptr 0

#endif /* TYPES_H_ */
