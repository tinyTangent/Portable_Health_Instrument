/*
 * App.h
 *
 *  Created on: Sep 19, 2015
 *      Author: tansinan
 */

#ifndef APP_H_
#define APP_H_

#include "Core/AppFramework.h"

extern const App AppAccStep_theApp;
extern const App AppAlarmClock_theApp;
extern const App AppMusicGame_theApp;
extern const App AppReactionTime_theApp;
extern const App AppMenu_theApp;
extern const App APP_SNAKE_3D;
extern const App AppMusic_app;
extern const App AppTimer_app;
extern const App AppStepSensor_theApp;
extern const App APP_PM25;
extern const App APP_SNAKE_2D;

#endif /* APP_H_ */
