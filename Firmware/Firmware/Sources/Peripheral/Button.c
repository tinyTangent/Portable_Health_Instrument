/*
 * Button.c
 *
 *  Created on: Jun 11, 2015
 *      Author: tansinan
 */

#include <stdint.h>
#include "derivative.h"
#include "Button.h"
#include "Common.h"
#include "Core/Timer.h"
#include "Core/AppFramework.h"

static bool Button_init();
static void Button_onTimer();

const Service ServiceButton_service = {
	.initHandler = Button_init,
	.taskHandler = Button_onTimer,
	.taskPerformDuration = 3
};

static uint8_t keyStatus[BUTTON_PIN_BINDING_COUNT];
static uint32_t lastKeyPressed[BUTTON_PIN_BINDING_COUNT];

static bool Button_init()
{
	// Port B��Port Eʹ��
	SIM_SCGC5 |= 0x2400;
	
	for(int i = 0; i < BUTTON_PIN_BINDING_COUNT; i++)
	{
		Port port = BUTTON_PIN_BINDING[i].port;
		PinNumber pinNumber = BUTTON_PIN_BINDING[i].pin;
		
		//Config needed pins as GPIO
		PORT_BASE_PTR[port]->PCR[pinNumber] = 0x102;
		GPIO_BASE_PTR[port]->PDDR &= ~(1<<pinNumber);
	}
	
	Timer_set(3, Button_onTimer);
	
	for(int i=0;i<BUTTON_PIN_BINDING_COUNT;i++)
	{
		keyStatus[i] = 0;
		lastKeyPressed[i] = 0;
	}
	
	return true;
}

static void Button_onTimer()
{
	for(int i=0;i<BUTTON_PIN_BINDING_COUNT;i++)
	{
		Port port = BUTTON_PIN_BINDING[i].port;
		PinNumber pinNumber = BUTTON_PIN_BINDING[i].pin;
		if((GPIO_BASE_PTR[port]->PDIR&(1<<pinNumber)) == 0)
		{
			if(keyStatus[i] == 0)
			{
				srand(rand()^(Timer_getClockFromStartUp()+ i*535));
				lastKeyPressed[i] = Timer_getClockFromStartUp();
				App_sendMessage(EVENT_KEY_DOWN, (Key)i);
			}
			keyStatus[i] = 1;
		}
		else
		{
			if(keyStatus[i] == 1)
			{
				uint32_t duration = Timer_getClockFromStartUp() - lastKeyPressed[i];
				if(duration > 500) App_sendMessage(EVENT_KEY_LONG_PRESSED, (Key)i);
				else App_sendMessage(EVENT_KEY_PRESSED, (Key)i);
			}
			keyStatus[i] = 0;
		}
	}
}
