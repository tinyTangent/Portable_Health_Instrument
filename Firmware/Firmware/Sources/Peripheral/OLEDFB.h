#ifndef OLEDFB_H_
#define OLEDFB_H_

#define OLEDFB_BLACK 0
#define OLEDFB_WHITE 1
#define OLEDFB_INVERTED 2
#define OLEDFB_BORDER 3

#include "Common/Types.h"
#include <stdint.h>
//TODO : functions not implemented.

//These are pixel-level controlling functions

void OLEDFB_init();
void OLEDFB_clear();
void OLEDFB_setPixelColumn(uint8_t x, uint8_t y, uint8_t data);
void OLEDFB_setPixelBlack(uint8_t x, uint8_t y);
void OLEDFB_setPixelWhite(uint8_t x, uint8_t y);
void OLEDFB_setPixelInverted(uint8_t x, uint8_t y);
void OLEDFB_flush();

void OLEDFB_getPixel();

void OLEDFB_drawPixel(uint8_t x, uint8_t y, uint8_t mode);

void OLEDFB_drawBitmap(uint8_t x, uint8_t y, uint8_t* bitmap, uint8_t srcWidth, uint8_t srcHeight);

void OLEDFB_drawBitmapEx(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint8_t* bitmap, uint8_t srcWidth, uint8_t srcHeight);

void OLEDFB_drawChar(uint8_t x, uint8_t y, const uchar c);
void OLEDFB_drawCharEx(uint8_t x, uint8_t y, uint8_t charWidth, uint8_t charHeight, const uchar c);
void OLEDFB_drawText(uint8_t x, uint8_t y, const char* str);
void OLEDFB_drawTextEx(uint8_t x, uint8_t y, uint8_t charWidth, uint8_t charHeight, const char* str);

void OLEDFB_drawRect(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t mode);
void OLEDFB_drawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2);
void OLEDFB_drawCircle(int8_t x, int8_t y, int8_t r, uint8_t mode);

#endif //OLED_H_
