/*
 * Button.h
 *
 *  Created on: Jun 11, 2015
 *      Author: tansinan
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "Common.h"
#include "Core/ServiceFramework.h"

#define EVENT_KEY_UP 100

extern const Service ServiceButton_service;

#endif /* BUTTON_H_ */
