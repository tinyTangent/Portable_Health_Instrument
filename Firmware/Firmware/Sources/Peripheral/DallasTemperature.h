/*
 * DallasTemperature.h
 *
 *  Created on: Jul 23, 2015
 *      Author: tansinan
 */

#ifndef DALLASTEMPERATURE_H_
#define DALLASTEMPERATURE_H_

#include "Common.h"
#include "OneWire.h"

typedef uint8_t ScratchPad[9];
typedef uint8_t DeviceAddress[8];

// Model IDs
#define DS18S20MODEL 0x10
#define DS18B20MODEL 0x28
#define DS1822MODEL  0x22

// OneWire commands
#define STARTCONVO      0x44  // Tells device to take a temperature reading and put it on the scratchpad
#define COPYSCRATCH     0x48  // Copy EEPROM
#define READSCRATCH     0xBE  // Read EEPROM
#define WRITESCRATCH    0x4E  // Write to EEPROM
#define RECALLSCRATCH   0xB8  // Reload from last known
#define READPOWERSUPPLY 0xB4  // Determine if device needs parasite power
#define ALARMSEARCH     0xEC  // Query bus for devices with an alarm condition

// Scratchpad locations
#define TEMP_LSB        0
#define TEMP_MSB        1
#define HIGH_ALARM_TEMP 2
#define LOW_ALARM_TEMP  3
#define CONFIGURATION   4
#define INTERNAL_BYTE   5
#define COUNT_REMAIN    6
#define COUNT_PER_C     7
#define SCRATCHPAD_CRC  8

// Device resolution
#define TEMP_9_BIT  0x1F //  9 bit
#define TEMP_10_BIT 0x3F // 10 bit
#define TEMP_11_BIT 0x5F // 11 bit
#define TEMP_12_BIT 0x7F // 12 bit

// Error Codes
#define DEVICE_DISCONNECTED -127

typedef struct 
{
	// parasite power on or off
	bool parasite;
	
	// used to determine the delay amount needed to allow for the
	// temperature conversion to take place
	int conversionDelay;
	
	// count of devices on the bus
	uint8_t devices;
	
	// Take a pointer to one wire instance
	OneWire* _wire;
} DallasTemperature;

void DallasTemperature_init(DallasTemperature* dallasTemp, OneWire* _oneWire);
void DallasTemperature_begin(DallasTemperature* dallasTemp);
bool DallasTemperature_validAddress(uint8_t* deviceAddress);
void DallasTemperature_readScratchPad(DallasTemperature* dallasTemp, uint8_t* deviceAddress, uint8_t* scratchPad);
bool DallasTemperature_readPowerSupply(DallasTemperature* dallasTemp, uint8_t* deviceAddress);
void DallasTemperature_requestTemperatures(DallasTemperature* dallasTemp);
bool DallasTemperature_getAddress(DallasTemperature* dallasTemp, uint8_t* deviceAddress, uint8_t index);
bool DallasTemperature_isConnected(DallasTemperature* dallasTemp, uint8_t* deviceAddress, uint8_t* scratchPad);
float DallasTemperature_getTempC(DallasTemperature* dallasTemp, uint8_t* deviceAddress);
float DallasTemperature_getTempCByIndex(DallasTemperature* dallasTemp, uint8_t deviceIndex);
float DallasTemperature_calculateTemperature(uint8_t* deviceAddress, uint8_t* scratchPad);

#endif /* DALLASTEMPERATURE_H_ */
