/*
 * OneWire.c
 *
 *  Created on: Jul 23, 2015
 *      Author: tansinan
 */

#include "OneWire.h"
#include "Common.h"
#include "Core/Delay.h"

void OneWire_init(OneWire* oneWire)
{
	oneWire->regMemMap = (GPIO_MemMapPtr)PTC_BASE_PTR;
	oneWire->bitMask = 1 << 1;
}

bool OneWire_reset(OneWire* oneWire)
{
	GPIO_MemMapPtr regMemMap = oneWire->regMemMap;
	uint32_t bitMask = oneWire->bitMask;
	uint8_t r;
	uint8_t retries = 125;

	DIRECT_MODE_INPUT(regMemMap, bitMask);
	do {
		if (--retries == 0) return 0;
		delayMicroseconds(2);
	} while ( !DIRECT_READ(regMemMap, bitMask));

	DIRECT_WRITE_LOW(regMemMap, bitMask);
	DIRECT_MODE_OUTPUT(regMemMap, bitMask);	// drive output low
	delayMicroseconds(500);
	DIRECT_MODE_INPUT(regMemMap, bitMask);	// allow it to float
	delayMicroseconds(80);
	r = !DIRECT_READ(regMemMap, bitMask);
	delayMicroseconds(420);
	return r;
}

void OneWire_writeBit(OneWire* oneWire, uint8_t v)
{
	GPIO_MemMapPtr regMemMap = oneWire->regMemMap;
	uint32_t bitMask = oneWire->bitMask;

	if (v & 1) {
		DIRECT_WRITE_LOW(regMemMap, bitMask);
		DIRECT_MODE_OUTPUT(regMemMap, bitMask);	// drive output low
		delayMicroseconds(10);
		DIRECT_WRITE_HIGH(regMemMap, bitMask);	// drive output high
		delayMicroseconds(55);
	} else {
		DIRECT_WRITE_LOW(regMemMap, bitMask);
		DIRECT_MODE_OUTPUT(regMemMap, bitMask);	// drive output low
		delayMicroseconds(65);
		DIRECT_WRITE_HIGH(regMemMap, bitMask);	// drive output high
		delayMicroseconds(5);
	}
}

uint8_t OneWire_readBit(OneWire* oneWire)
{
	GPIO_MemMapPtr regMemMap = oneWire->regMemMap;
	uint32_t bitMask = oneWire->bitMask;
	uint8_t r;

	DIRECT_MODE_OUTPUT(regMemMap, bitMask);
	DIRECT_WRITE_LOW(regMemMap, bitMask);
	//delayMicroseconds(3);
	delayLoop(0);
	DIRECT_MODE_INPUT(regMemMap, bitMask);	// let pin float, pull up will raise
	delayMicroseconds(10);
	r = DIRECT_READ(regMemMap, bitMask);
	delayMicroseconds(53);
	return r;
}

void OneWire_writeBitDbg(OneWire* oneWire, uint8_t v, int t)
{
	GPIO_MemMapPtr regMemMap = oneWire->regMemMap;
	uint32_t bitMask = oneWire->bitMask;

	if (v & 1) {
		DIRECT_WRITE_LOW(regMemMap, bitMask);
		DIRECT_MODE_OUTPUT(regMemMap, bitMask);	// drive output low
		delayMicroseconds(t / 10);
		DIRECT_WRITE_HIGH(regMemMap, bitMask);	// drive output high
		delayMicroseconds(55 * t /100);
	} else {
		DIRECT_WRITE_LOW(regMemMap, bitMask);
		DIRECT_MODE_OUTPUT(regMemMap, bitMask);	// drive output low
		delayMicroseconds(65* t/100);
		DIRECT_WRITE_HIGH(regMemMap, bitMask);	// drive output high
		delayMicroseconds(t / 20);
	}
}

uint8_t OneWire_readBitDbg(OneWire* oneWire, int t)
{
	GPIO_MemMapPtr regMemMap = oneWire->regMemMap;
	uint32_t bitMask = oneWire->bitMask;
	uint8_t r;

	DIRECT_MODE_OUTPUT(regMemMap, bitMask);
	DIRECT_WRITE_LOW(regMemMap, bitMask);
	//delayMicroseconds((3*t + 50)/100);
	delay(2);
	DIRECT_MODE_INPUT(regMemMap, bitMask);	// let pin float, pull up will raise
	delayMicroseconds(10*t/100);
	r = DIRECT_READ(regMemMap, bitMask);
	delayMicroseconds(53*t / 100);
	//delayMicroseconds(53*t/100);
	return r;
}


void OneWire_write(OneWire* oneWire, uint8_t v, uint8_t power)
{
    uint8_t bitMask;
    for (bitMask = 0x01; bitMask; bitMask <<= 1)
    	OneWire_writeBit(oneWire, (bitMask & v)?1:0);
    if (!power)
    {
    	DIRECT_MODE_INPUT(oneWire->regMemMap, oneWire->bitMask);
    	DIRECT_WRITE_LOW(oneWire->regMemMap, oneWire->bitMask);
    }
}

void OneWire_writeDbg(OneWire* oneWire, uint8_t v, uint8_t power, int t)
{
    uint8_t bitMask;
    for (bitMask = 0x01; bitMask; bitMask <<= 1)
    	OneWire_writeBitDbg(oneWire, (bitMask & v)?1:0, t);
    if (!power)
    {
    	DIRECT_MODE_INPUT(oneWire->regMemMap, oneWire->bitMask);
    	DIRECT_WRITE_LOW(oneWire->regMemMap, oneWire->bitMask);
    }
}


void OneWire_writeBytes(OneWire* oneWire, const uint8_t *buf, uint16_t count, bool power)
{
	for (uint16_t i = 0 ; i < count ; i++)
		OneWire_write(oneWire, buf[i], 0);
	if (!power)
	{
		DIRECT_MODE_INPUT(oneWire->regMemMap, oneWire->bitMask);
		DIRECT_WRITE_LOW(oneWire->regMemMap, oneWire->bitMask);
	}
}

uint8_t OneWire_read(OneWire* oneWire)
{
	uint8_t bitMask;
	uint8_t r = 0;
	for (bitMask = 0x01; bitMask; bitMask <<= 1)
	{
		if ( OneWire_readBit(oneWire)) r |= bitMask;
	}
	return r;
}

void OneWire_readBytes(OneWire* oneWire, uint8_t *buf, uint16_t count)
{
	for (uint16_t i = 0 ; i < count ; i++)
		buf[i] = OneWire_read(oneWire);
}

void OneWire_select(OneWire* oneWire, uint8_t rom[8])
{
    int i;
    OneWire_write(oneWire, 0x55, 0);
    for( i = 0; i < 8; i++) OneWire_write(oneWire, rom[i], 0);
}

void OneWire_skip(OneWire* oneWire)
{
	OneWire_write(oneWire, 0xCC, 0);
}

void OneWire_depower(OneWire* oneWire)
{
	DIRECT_MODE_INPUT(oneWire->regMemMap, oneWire->bitMask);
}

void OneWire_resetSearch(OneWire* oneWire)
{
	// reset the search state
	oneWire->LastDiscrepancy = 0;
	oneWire->LastDeviceFlag = FALSE;
	oneWire->LastFamilyDiscrepancy = 0;
	for(int i = 7; ; i--)
	{
		oneWire->ROM_NO[i] = 0;
		if ( i == 0) break;
	}
}

//
// Perform a search. If this function returns a '1' then it has
// enumerated the next device and you may retrieve the ROM from the
// OneWire::address variable. If there are no devices, no further
// devices, or something horrible happens in the middle of the
// enumeration then a 0 is returned.  If a new device is found then
// its address is copied to newAddr.  Use OneWire::reset_search() to
// start over.
//
// --- Replaced by the one from the Dallas Semiconductor web site ---
//--------------------------------------------------------------------------
// Perform the 1-Wire Search Algorithm on the 1-Wire bus using the existing
// search state.
// Return TRUE  : device found, ROM number in ROM_NO buffer
//        FALSE : device not found, end of search
//
uint8_t OneWire_search(OneWire* oneWire, uint8_t *newAddr)
{
   uint8_t id_bit_number;
   uint8_t last_zero, rom_byte_number, search_result;
   uint8_t id_bit, cmp_id_bit;

   unsigned char rom_byte_mask, search_direction;

   // initialize for search
   id_bit_number = 1;
   last_zero = 0;
   rom_byte_number = 0;
   rom_byte_mask = 1;
   search_result = 0;

   // if the last call was not the last one
   if (!oneWire->LastDeviceFlag)
   {
      // 1-Wire reset
      if (!OneWire_reset(oneWire))
      {
         // reset the search
    	 oneWire->LastDiscrepancy = 0;
    	 oneWire->LastDeviceFlag = FALSE;
    	 oneWire->LastFamilyDiscrepancy = 0;
         return FALSE;
      }

      // issue the search command
      OneWire_write(oneWire, 0xF0, 0);

      // loop to do the search
      do
      {
         // read a bit and its complement
         id_bit = OneWire_readBit(oneWire);
         cmp_id_bit = OneWire_readBit(oneWire);

         // check for no devices on 1-wire
         if ((id_bit == 1) && (cmp_id_bit == 1))
         {
        	 id_bit = 1;
            break;
         }
         else
         {
            // all devices coupled have 0 or 1
            if (id_bit != cmp_id_bit)
               search_direction = id_bit;  // bit write value for search
            else
            {
               // if this discrepancy if before the Last Discrepancy
               // on a previous next then pick the same as last time
               if (id_bit_number < oneWire->LastDiscrepancy)
                  search_direction = ((oneWire->ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
               else
                  // if equal to last pick 1, if not then pick 0
                  search_direction = (id_bit_number == oneWire->LastDiscrepancy);

               // if 0 was picked then record its position in LastZero
               if (search_direction == 0)
               {
                  last_zero = id_bit_number;

                  // check for Last discrepancy in family
                  if (last_zero < 9)
                	  oneWire->LastFamilyDiscrepancy = last_zero;
               }
            }

            // set or clear the bit in the ROM byte rom_byte_number
            // with mask rom_byte_mask
            if (search_direction == 1)
            	oneWire->ROM_NO[rom_byte_number] |= rom_byte_mask;
            else
            	oneWire->ROM_NO[rom_byte_number] &= ~rom_byte_mask;

            // serial number search direction write bit
            OneWire_writeBit(oneWire, search_direction);

            // increment the byte counter id_bit_number
            // and shift the mask rom_byte_mask
            id_bit_number++;
            rom_byte_mask <<= 1;

            // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
            if (rom_byte_mask == 0)
            {
                rom_byte_number++;
                rom_byte_mask = 1;
            }
         }
      }
      while(rom_byte_number < 8);  // loop until through all ROM bytes 0-7

      // if the search was successful then
      if (!(id_bit_number < 65))
      {
         // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
    	  oneWire->LastDiscrepancy = last_zero;

         // check for last device
         if (oneWire->LastDiscrepancy == 0)
        	 oneWire->LastDeviceFlag = TRUE;

         search_result = TRUE;
      }
   }

   // if no device found then reset counters so next 'search' will be like a first
   if (!search_result || !oneWire->ROM_NO[0])
   {
	   oneWire->LastDiscrepancy = 0;
	   oneWire->LastDeviceFlag = FALSE;
	   oneWire->LastFamilyDiscrepancy = 0;
	   search_result = FALSE;
   }
   for (int i = 0; i < 8; i++) newAddr[i] = oneWire->ROM_NO[i];
   return search_result;
}

static const uint8_t dscrc_table[] = {
      0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
    157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
     35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
    190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
     70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
    219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
    101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
    248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
    140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
     17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
    175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
     50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
    202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
     87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
    233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
    116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53};

//
// Compute a Dallas Semiconductor 8 bit CRC. These show up in the ROM
// and the registers.  (note: this might better be done without to
// table, it would probably be smaller and certainly fast enough
// compared to all those delayMicrosecond() calls.  But I got
// confused, so I use this table from the examples.)
//
uint8_t OneWire_crc8( uint8_t *addr, uint8_t len)
{
	uint8_t crc = 0;

	while (len--) {
		crc = *(dscrc_table + (crc ^ *addr++));
	}
	return crc;
}
