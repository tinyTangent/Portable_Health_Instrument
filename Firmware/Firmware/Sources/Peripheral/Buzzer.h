/*
 * Buzzer.h
 *
 *  Created on: Jun 14, 2015
 *      Author: tansinan
 */

#ifndef BUZZER_H_
#define BUZZER_H_

#include <stdint.h>
#include "Common/Types.h"

void Buzzer_init();
void Buzzer_set(uint32_t frequency, uint8_t volume);

#endif /* BUZZER_H_ */
