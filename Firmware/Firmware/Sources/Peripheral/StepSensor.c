/*
 * StepSensor.c
 *
 *  Created on: Sep 19, 2015
 *      Author: tansinan
 */

#include "Common.h"
#include "StepSensor.h"
#include "Util/Storage.h"

static void init();
static void step_query();

static int previousRecord = 0;
static uint32_t step = 0;

const Service ServiceStepSensor_service = {
	.initHandler = init,
	.taskHandler = step_query,
	.taskPerformDuration = 3
};


static void init()
{
	Port port = STEP_SENSOR_PIN_BINDING.port;
	PinNumber pinNumber = STEP_SENSOR_PIN_BINDING.pin;
	SIM_SCGC5 |= (SIM_SCGC5_PORTA_MASK << STEP_SENSOR_PIN_BINDING.port);
	PORT_BASE_PTR[port]->PCR[pinNumber] = PORT_PCR_MUX(0X1) + PORT_PCR_PE_MASK + PORT_PCR_PS_MASK;
	Timer_set(3, step_query);
}

static void step_query()
{
	Port port = STEP_SENSOR_PIN_BINDING.port;
	PinNumber pinNumber = STEP_SENSOR_PIN_BINDING.pin;
	if(GPIO_BASE_PTR[port]->PDIR&(1<<pinNumber))
	{
		previousRecord = 1;
	}
	else
	{
		if(previousRecord == 1)
		{
			recordSport();
			step++;
		}
		previousRecord = 0;
	}
}

uint32_t StepSensor_queryStep()
{
	return step;
}

void StepSensor_resetStep()
{
	step = 0;
}
