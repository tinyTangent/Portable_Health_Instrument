/*
 * UART.h
 *
 *  Created on: Apr 16, 2015
 *      Author: tansinan
 */

#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include "Common.h"

void UART_init();

bool UART_isReadyRead();

bool UART_isReadyWrite();

char UART_directRead();

void UART_directWrite(char data);

void UART_putChar(unsigned char data);

void UART_putString(unsigned char* str);

unsigned char UART_getChar();

#endif /* UART_H_ */
