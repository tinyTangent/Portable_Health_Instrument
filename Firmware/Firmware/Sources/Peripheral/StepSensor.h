/*
 * StepSensor.h
 *
 *  Created on: Sep 19, 2015
 *      Author: tansinan
 */

#ifndef STEPSENSOR_H_
#define STEPSENSOR_H_

#include "Common.h"
#include "Core/ServiceFramework.h"

typedef struct {
	uint16_t walkStepCount;
	uint16_t walkDistance;
	uint16_t runStepCount;
	uint16_t runDistance;
	uint16_t pm25Pollution;
} SportRecordSegment;

SportRecordSegment sportRecord[24];

extern const Service ServiceStepSensor_service;

uint32_t StepSensor_queryStep();
void StepSensor_resetStep();

#endif /* STEPSENSOR_H_ */
