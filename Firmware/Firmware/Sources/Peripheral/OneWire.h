/*
 * OneWire.h
 *
 *  Created on: Jul 23, 2015
 *      Author: tansinan
 */

#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include "Common.h"

#define DIRECT_READ(regMemMap, mask)         (((regMemMap)->PDIR & (mask)) ? 1 : 0)
#define DIRECT_MODE_INPUT(regMemMap, mask)   ((regMemMap)->PDDR &= ~(mask))
#define DIRECT_MODE_OUTPUT(regMemMap, mask)  ((regMemMap)->PDDR |= (mask))
#define DIRECT_WRITE_LOW(regMemMap, mask)    ((regMemMap)->PDOR &= ~(mask))
#define DIRECT_WRITE_HIGH(regMemMap, mask)   ((regMemMap)->PDOR |= (mask))

typedef struct OneWire
{
	GPIO_MemMapPtr regMemMap;
	uint32_t bitMask;
    unsigned char ROM_NO[8];
    uint8_t LastDiscrepancy;
    uint8_t LastFamilyDiscrepancy;
    uint8_t LastDeviceFlag;
} OneWire;

void OneWire_init(OneWire* oneWire);
bool OneWire_reset(OneWire* oneWire);
void OneWire_writeBit(OneWire* oneWire, uint8_t v);
uint8_t OneWire_readBit(OneWire* oneWire);
void OneWire_write(OneWire* oneWire, uint8_t v, uint8_t power);
void OneWire_writeBytes(OneWire* oneWire, const uint8_t *buf, uint16_t count, bool power);
uint8_t OneWire_read(OneWire* oneWire);
void OneWire_readBytes(OneWire* oneWire, uint8_t *buf, uint16_t count);
void OneWire_select(OneWire* oneWire, uint8_t rom[8]);
void OneWire_skip(OneWire* oneWire);
void OneWire_depower(OneWire* oneWire);
void OneWire_resetSearch(OneWire* oneWire);
uint8_t OneWire_search(OneWire* oneWire, uint8_t *newAddr);
uint8_t OneWire_crc8(uint8_t *addr, uint8_t len);

void OneWire_writeBitDbg(OneWire* oneWire, uint8_t v, int t);
uint8_t OneWire_readBitDbg(OneWire* oneWire, int t);
void OneWire_writeDbg(OneWire* oneWire, uint8_t v, uint8_t power, int t);

#endif /* ONEWIRE_H_ */
