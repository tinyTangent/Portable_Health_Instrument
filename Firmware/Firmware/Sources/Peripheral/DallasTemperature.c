/*
 * DallasTemperature.c
 *
 *  Created on: Jul 23, 2015
 *      Author: tansinan
 */

#include "DallasTemperature.h"
#include "Core/Delay.h"

void DallasTemperature_init(DallasTemperature* dallasTemp, OneWire* _oneWire)
{
	dallasTemp->_wire = _oneWire;
	dallasTemp->devices = 0;
	dallasTemp->parasite = false;
	dallasTemp->conversionDelay = TEMP_9_BIT;
}

bool DallasTemperature_validAddress(uint8_t* deviceAddress)
{
	return (OneWire_crc8(deviceAddress, 7) == deviceAddress[7]);
}

void DallasTemperature_begin(DallasTemperature* dallasTemp)
{
	OneWire* _wire = dallasTemp->_wire;
	DeviceAddress deviceAddress;
	
	OneWire_resetSearch(_wire);
	
	while (OneWire_search(_wire, deviceAddress))
	{
		if (DallasTemperature_validAddress(deviceAddress))
		{      
		  if (!dallasTemp->parasite && DallasTemperature_readPowerSupply(dallasTemp, deviceAddress)) dallasTemp->parasite = true;
		
		  ScratchPad scratchPad;
		
		  DallasTemperature_readScratchPad(dallasTemp, deviceAddress, scratchPad);
		
		  if (deviceAddress[0] == DS18S20MODEL) dallasTemp->conversionDelay = TEMP_12_BIT; // 750 ms
		  else if (scratchPad[CONFIGURATION] > dallasTemp->conversionDelay)
			  dallasTemp->conversionDelay = scratchPad[CONFIGURATION];
		
		  dallasTemp->devices++;
		}
	}
}

void DallasTemperature_readScratchPad(DallasTemperature* dallasTemp, uint8_t* deviceAddress, uint8_t* scratchPad)
{
	OneWire* _wire = dallasTemp->_wire;
  // send the command
	OneWire_reset(_wire);
	OneWire_select(_wire, deviceAddress);
	OneWire_write(_wire, READSCRATCH, 0);

  // read the response   

  // byte 0: temperature LSB
  scratchPad[TEMP_LSB] = OneWire_read(_wire);

  // byte 1: temperature MSB
  scratchPad[TEMP_MSB] = OneWire_read(_wire);

  // byte 2: high alarm temp
  scratchPad[HIGH_ALARM_TEMP] = OneWire_read(_wire);

  // byte 3: low alarm temp
  scratchPad[LOW_ALARM_TEMP] = OneWire_read(_wire);

  // byte 4:
  // DS18S20: store for crc
  // DS18B20 & DS1822: configuration register
  scratchPad[CONFIGURATION] = OneWire_read(_wire);

  // byte 5:
  // internal use & crc
  scratchPad[INTERNAL_BYTE] = OneWire_read(_wire);

  // byte 6:
  // DS18S20: COUNT_REMAIN
  // DS18B20 & DS1822: store for crc
  scratchPad[COUNT_REMAIN] = OneWire_read(_wire);
  
  // byte 7:
  // DS18S20: COUNT_PER_C
  // DS18B20 & DS1822: store for crc
  scratchPad[COUNT_PER_C] = OneWire_read(_wire);
  
  // byte 8:
  // SCTRACHPAD_CRC
  scratchPad[SCRATCHPAD_CRC] = OneWire_read(_wire);

  OneWire_reset(_wire);
}

bool DallasTemperature_readPowerSupply(DallasTemperature* dallasTemp, uint8_t* deviceAddress)
{
	OneWire* _wire = dallasTemp->_wire;
	bool ret = false;
	OneWire_reset(_wire);
	OneWire_select(_wire, deviceAddress);
	OneWire_write(_wire, READPOWERSUPPLY, 0);
	if (OneWire_readBit(_wire) == 0) ret = true;
	OneWire_reset(_wire);
	return ret;
}

void DallasTemperature_requestTemperatures(DallasTemperature* dallasTemp)
{
	OneWire* _wire = dallasTemp->_wire;
	OneWire_reset(_wire);
	OneWire_skip(_wire);
	OneWire_write(_wire, STARTCONVO, dallasTemp->parasite);

	switch (dallasTemp->conversionDelay)
	{
	case TEMP_9_BIT:
		delay(94);
		break;
	case TEMP_10_BIT:
		delay(188);
		break;
	case TEMP_11_BIT:
		delay(375);
		break;
	case TEMP_12_BIT:
	default:
		delay(750);
		break;
	}
}

float DallasTemperature_getTempCByIndex(DallasTemperature* dallasTemp, uint8_t deviceIndex)
{
	DeviceAddress deviceAddress;
	DallasTemperature_getAddress(dallasTemp, deviceAddress, deviceIndex);
	return DallasTemperature_getTempC(dallasTemp, (uint8_t*)deviceAddress);
}

bool DallasTemperature_getAddress(DallasTemperature* dallasTemp, uint8_t* deviceAddress, uint8_t index)
{
	OneWire* _wire = dallasTemp->_wire;
	uint8_t depth = 0;
	
	OneWire_resetSearch(_wire);
	
	while (depth <= index && OneWire_search(_wire, deviceAddress))
	{
		if (depth == index && DallasTemperature_validAddress(deviceAddress)) return true;
		depth++;
	}
	
	return false;
}

// attempt to determine if the device at the given address is connected to the bus
// also allows for updating the read scratchpad
bool DallasTemperature_isConnected(DallasTemperature* dallasTemp, uint8_t* deviceAddress, uint8_t* scratchPad)
{
	DallasTemperature_readScratchPad(dallasTemp, deviceAddress, scratchPad);
	return (OneWire_crc8(scratchPad, 8) == scratchPad[SCRATCHPAD_CRC]);
}

float DallasTemperature_getTempC(DallasTemperature* dallasTemp, uint8_t* deviceAddress)
{
  // TODO: Multiple devices (up to 64) on the same bus may take some time to negotiate a response
  // What happens in case of collision?

  ScratchPad scratchPad;
  if (DallasTemperature_isConnected(dallasTemp, deviceAddress, scratchPad))
	  return DallasTemperature_calculateTemperature(deviceAddress, scratchPad);
  return DEVICE_DISCONNECTED;
}

float DallasTemperature_calculateTemperature(uint8_t* deviceAddress, uint8_t* scratchPad)
{
  int16_t rawTemperature = (((int16_t)scratchPad[TEMP_MSB]) << 8) | scratchPad[TEMP_LSB];

  switch (deviceAddress[0])
  {
    case DS18B20MODEL:
    case DS1822MODEL:
      switch (scratchPad[CONFIGURATION])
      {
        case TEMP_12_BIT:
          return (float)rawTemperature * 0.0625;
          break;
        case TEMP_11_BIT:
          return (float)(rawTemperature >> 1) * 0.125;
          break;
        case TEMP_10_BIT:
          return (float)(rawTemperature >> 2) * 0.25;
          break;
        case TEMP_9_BIT:
          return (float)(rawTemperature >> 3) * 0.5;
          break;
      }
      break;
    case DS18S20MODEL:
      /*
      
      Resolutions greater than 9 bits can be calculated using the data from 
      the temperature, COUNT REMAIN and COUNT PER �C registers in the 
      scratchpad. Note that the COUNT PER �C register is hard-wired to 16 
      (10h). After reading the scratchpad, the TEMP_READ value is obtained 
      by truncating the 0.5�C bit (bit 0) from the temperature data. The 
      extended resolution temperature can then be calculated using the 
      following equation:
      
                                       COUNT_PER_C - COUNT_REMAIN
      TEMPERATURE = TEMP_READ - 0.25 + --------------------------
                                               COUNT_PER_C
      */
  
      // Good spot. Thanks Nic Johns for your contribution
		return (float)(rawTemperature >> 1) - 0.25 +((float)(scratchPad[COUNT_PER_C] - scratchPad[COUNT_REMAIN]) / (float)scratchPad[COUNT_PER_C] ); 
		break;
	}
}

