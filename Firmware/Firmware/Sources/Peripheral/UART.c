/*
 * UART.c
 *
 *  Created on: May 7, 2015
 *      Author: leo
 */
 
 #include "UART.h"
 
void set_irq_priority (int irq, int prio)
{   
    /*irq priority pointer*/
    uint8_t *prio_reg;
    uint8_t err = 0;
    uint8_t div = 0;
    
    /* Make sure that the IRQ is an allowable number. Right now up to 32 is 
     * used.
     *
     * NOTE: If you are using the interrupt definitions from the header
     * file, you MUST SUBTRACT 16!!!
     */
    if (irq > 32)
    {
        //printf("\nERR! Invalid IRQ value passed to priority irq function!\n");
        err = 1;
    }

    if (prio > 3)
    {
        //printf("\nERR! Invalid priority value passed to priority irq function!\n");
        err = 1;
    }
    
    if (err != 1)
    {
        /* Determine which of the NVICIPx corresponds to the irq */
        div = irq / 4;
        prio_reg = (uint8_t *)((uint32_t)&NVIC_IP(div));
        /* Assign priority to IRQ */
        *prio_reg = ( (prio&0x3) << (8 - 2) );             
    }
}
void UART_init()
{
	SIM_SOPT2 |= SIM_SOPT2_UART0SRC(1);
	SIM_SOPT2 &= ~SIM_SOPT2_PLLFLLSEL_MASK;
	SIM_SCGC5 |= 0x0200;
	SIM_SCGC4 |= 0x0400;
	PORTA_PCR1 = PORT_PCR_ISF_MASK|PORT_PCR_MUX(0x2); //0x200
	PORTA_PCR2 = PORT_PCR_ISF_MASK|PORT_PCR_MUX(0x2);
	UART0_C2 &= ~ (UART0_C2_TE_MASK| UART0_C2_RE_MASK);
	NVIC_ISER |= 1 << 12;
	set_irq_priority(12,3);
	
	UART0_BDH = 0x00;
	UART0_BDL = 0x8F;
	////////
	UART0_C1 = 0x00;
	UART0_C3 = 0x00;
	UART0_MA1 = 0x00;
	UART0_MA1 = 0x00;
	UART0_S1 |= 0x1F;
	UART0_S2 |= 0xC0;
	//UART0_C2 |= UART0_C2_TE_MASK|UART0_C2_RE_MASK;
	UART0_C2 |= UART0_C2_RIE_MASK|UART0_C2_TE_MASK|UART0_C2_RE_MASK;
}

bool UART_isReadyRead()
{
	return UART0_S1 & 0x20;
}

bool UART_isReadyWrite()
{
	return UART0_S1 & UART_S1_TDRE_MASK;
}

char UART_directRead()
{
	return UART0_D;
}

void UART_directWrite(char data)
{
	UART0_D = data;
}

void UART_putChar(unsigned char data)
{
	while(!UART_isReadyWrite());
	UART0_D = data;
}

void UART_putString(unsigned char* str)
{
	for(int i=0;;i++)
	{
		if(str[i]!='\0')
		{
			UART_putChar(str[i]);
		}
		else return;
	}
}

unsigned char UART_getChar()
{
	while(!UART_isReadyRead());
	return UART0_D;
}
