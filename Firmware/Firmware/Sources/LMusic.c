/*
 * LMusic.c
 *
 *  Created on: Sep 19, 2015
 *      Author: tansinan
 */

/*
 * Music.c
 *
 *  Created on: Jun 16, 2015
 *      Author: tansinan
 */

#include "Peripheral/Buzzer.h"
#include "Core/Timer.h"
#include "Common/Types.h"
#include "LMusic.h"
#include "Resource/LMusicLibrary.h"

static int musicChoice = -1;
static int notePos = 0;
static int durationPos = 0;
static uint8_t timerId = 0xFF;
const unsigned short* currentNote;
const unsigned short* currentDuration;

static void timerHandler();

void MusicPlay(int _musicChoice)
{
	if(musicChoice != -1)
	{
		MusicStop();
	}
	if(_musicChoice < 1 || _musicChoice > 4) return;
	musicChoice = _musicChoice;
	
	int speed = 125;
	
	switch(musicChoice)
	{
	case 1:
		currentNote = note;
		currentDuration = duration;
		break;
	case 2:
		currentNote = note_1;
		currentDuration = duration_1;
		break;
	case 3:
		currentNote = note_2;
		currentDuration = duration_2;
		break;
	case 4:
		currentNote = note_3;
		currentDuration = duration_3;
		break;
	}
	
	notePos = 0;
	durationPos = 0;
	musicChoice = _musicChoice;
	timerHandler();
	timerId = Timer_set(125, timerHandler);
}

void MusicStop()
{
	Buzzer_set(0,0);
	if(timerId != 0xFF)
	{
		musicChoice = -1;
		Timer_unset(timerId);
	}
}

static void timerHandler()
{
	if(musicChoice == -1) return;
	durationPos++;
	if(durationPos == currentDuration[notePos])
	{
		durationPos = 0;
		notePos++;
	}
	if(currentNote[notePos] == 0xFF)
	{
		MusicStop();
		return;
	}
	Buzzer_set(currentNote[notePos] ,0xFF);
}

